/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "artefact/Environment.h"


class StdOs :
    public artefact::Os
{
  public:
    void write(const std::filesystem::path& name, const std::string& content) override;
    bool fileExists(const std::filesystem::path&) const override;
    std::string read(const std::filesystem::path& name) const override;
    void execute(const std::string& command) override;

};
