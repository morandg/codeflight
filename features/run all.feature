# language: en

Feature: run all
  As a user of the tool
  I want easily get some information about my code
  In order have a successful first interaction with the tool


Scenario: generate all usefull files
  Given I have the file "input/test.ast":
    """
    <project>
        <package name="">
            <package name="p1">
                <class name="ClassA">
                    <method name="methodA">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
            <variable name="variableA" id="1"/>
            <function name="functionA">
                <reference target="1"/>
            </variable>
            <variable name="variableC"/>
        </package>
    </project>
    """

  When I process the file "input/test.ast" with the output directory "output"

  Then I expect to have at least these files:
    | filename                                             |
    | input/test.ast                                       |
    | output/index.html                                    |
    | output/-/package-content-dependencies.svg            |
    | output/dependencies-between-packages/-~p1,-.svg      |
    | output/-/p1/package-content-dependencies.svg         |
    | output/-/p1/ClassA/intra-class-dependencies.svg      |
    | output/-/package-dependencies.svg                    |
    | output/-/p1/package-dependencies.svg                 |


Scenario: print the filename that will be generated
  Given I have the file "input/test.ast":
    """
    <project>
        <package name="">
            <package name="p1">
                <class name="ClassA">
                    <method name="methodA">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
            <variable name="variableA" id="1"/>
            <function name="functionA">
                <reference target="1"/>
            </variable>
            <variable name="variableC"/>
        </package>
    </project>
    """

  When I process the file "input/test.ast" with the output directory "output"

  Then I expect the log output:
    """
    INFO: generate: index.html
    INFO: generate: packages-with-cyclic-dependencies.html
    INFO: generate: stable-dependency-principle-violations.html
    INFO: generate: -/index.html
    INFO: generate: -/p1/index.html
    INFO: generate: -/p1/ClassA/index.html
    INFO: generate: -/functionA/index.html
    INFO: generate: -/variableA/index.html
    INFO: generate: -/package-content-dependencies.svg
    INFO: generate: -/package-dependencies.svg
    INFO: generate: -/neighbors.svg
    INFO: generate: -/p1/package-content-dependencies.svg
    INFO: generate: -/p1/package-dependencies.svg
    INFO: generate: -/p1/neighbors.svg
    INFO: generate: -/p1/ClassA/intra-class-dependencies.svg
    INFO: generate: -/p1/ClassA/neighbors.svg
    INFO: generate: -/functionA/neighbors.svg
    INFO: generate: -/variableA/neighbors.svg
    INFO: generate: dependencies-between-packages/-~p1,-.svg

    """
