# language: en

Feature: parse invalid top level package
  As a user of the tool
  I want to get notified about a wrong file format
  In order to fix my input file


Scenario: report a warning when no top level package is specified
  Given I have the input file:
    """
    <project>
    </project>
    """

  When I parse the input

  Then I expect the log output:
    """
    WARNING: missing top level package

    """


Scenario: report a warning when multiple level packages are specified
  Given I have the input file:
    """
    <project>
        <package name="a"/>
        <package name="b"/>
        <package name="c"/>
    </project>
    """

  When I parse the input

  Then I expect the log output:
    """
    WARNING: multiple top level packages

    """
