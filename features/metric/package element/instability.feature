# language: en

Feature: calculate the instability metric for package elements
  As an architect
  I want to know the instability of the elements
  In order to check if it is ok and find stable elements that depend on instable ones


Scenario: a simple stable, instable and in between elements
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="ma">
                    <reference target="1"/>
                </method>
            </class>
            <class name="ClassB">
                <method name="mb" id="1">
                    <reference target="2"/>
                </method>
            </class>
            <class name="ClassC">
                <method name="mc" id="2"/>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package element instability for "ClassA" to be 1.0
  And I expect the package element instability for "ClassB" to be 0.5
  And I expect the package element instability for "ClassC" to be 0.0


Scenario: instability of a class with no neighbors
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA"/>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package element instability for "ClassA" to be 0.0


Scenario: more than one in and out
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="m1">
                    <reference target="1"/>
                </method>
            </class>
            <class name="ClassB">
                <method name="m1">
                    <reference target="1"/>
                </method>
            </class>
            <class name="ClassC">
                <method name="m1">
                    <reference target="1"/>
                </method>
            </class>
            <class name="ClassD">
                <method name="m1" id="1">
                    <reference target="2"/>
                </method>
            </class>
            <class name="ClassE">
                <method name="m1" id="2"/>
            </class>
        </package>
    </project>
    """

  When I parse the input

  And I expect the package element instability for "ClassD" to be 0.25


Scenario: doesn't count multiple connection between the same classes
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="m1">
                    <reference target="1"/>
                </method>
                <method name="m2">
                    <reference target="1"/>
                </method>
                <method name="m3">
                    <reference target="2"/>
                </method>
            </class>
            <class name="ClassB">
                <method name="m1" id="1">
                    <reference target="3"/>
                </method>
                <method name="m2" id="2">
                    <reference target="3"/>
                </method>
            </class>
            <class name="ClassC">
                <method name="m1" id="3"/>
            </class>
        </package>
    </project>
    """

  When I parse the input

  And I expect the package element instability for "ClassB" to be 0.5
