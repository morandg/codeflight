# language: en

Feature: calculate the fan-in metric for package elements
  As an architect
  I want to know which elements have a low/high fan-in
  In order to fix my project


Scenario: simple fan-in calculation
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="ma">
                    <reference target="1"/>
                </method>
            </class>
            <class name="ClassB">
                <method name="mb" id="1"/>
            </class>
            <class name="ClassC">
                <method name="mc">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package element fan-in for "ClassB" to be 2


Scenario: does not count self-reference
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="ma">
                    <reference target="1"/>
                </method>
                <field name="fa" id="1"/>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package element fan-in for "ClassA" to be 0


Scenario: count multiple references from the same source only once
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="m1">
                    <reference target="1"/>
                </method>
                <method name="m2">
                    <reference target="1"/>
                </method>
            </class>
            <class name="ClassB">
                <method name="mb" id="1"/>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package element fan-in for "ClassB" to be 1


Scenario: count reference from different package
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="pa">
                <class name="ClassA">
                    <method name="m1">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
            <package name="pb">
                <class name="ClassB">
                    <method name="mb" id="1"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package element fan-in for "pb::ClassB" to be 1


Scenario: count reference from function
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="mb" id="1"/>
            </class>
            <function name="f">
                <reference target="1"/>
            </function>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package element fan-in for "ClassA" to be 1
