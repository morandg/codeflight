# language: en

Feature: calculate the fan-out metric for package elements
  As an architect
  I want to know which elements have a low/high fan-out
  In order to fix my project


Scenario: simple fan-out calculation
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA" id="1"/>
            <class name="ClassB">
                <method name="m1">
                    <reference target="1"/>
                </method>
                <method name="m2">
                    <reference target="2"/>
                </method>
            </class>
            <class name="ClassC">
                <method name="mc" id="2"/>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package element fan-out for "ClassB" to be 2


Scenario: does not count self-reference
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="ma">
                    <reference target="1"/>
                </method>
                <field name="fa" id="1"/>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package element fan-out for "ClassA" to be 0


Scenario: count multiple references to the same destination only once
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="m1" id="1"/>
                <method name="m2" id="2"/>
            </class>
            <class name="ClassB">
                <method name="m1">
                    <reference target="1"/>
                </method>
                <method name="m2">
                    <reference target="2"/>
                </method>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package element fan-out for "ClassB" to be 1


Scenario: count reference to different package
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="pa">
                <class name="ClassA">
                    <method name="m1" id="1"/>
                </class>
            </package>
            <package name="pb">
                <class name="ClassB">
                    <method name="m1">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package element fan-out for "pb::ClassB" to be 1


Scenario: count reference to function
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="mb">
                    <reference target="1"/>
                </method>
            </class>
            <function name="f" id="1"/>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package element fan-out for "ClassA" to be 1
