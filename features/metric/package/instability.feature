# language: en

Feature: calculate the instability metric for packages
  As an architect
  I want to know the instability of the packages
  In order to check if it is ok and find stable packages that depend on instable ones


Scenario: simple stable, instable and in between packages
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <method name="ma">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
            <package name="b">
                <class name="ClassB">
                    <method name="mb" id="1">
                        <reference target="2"/>
                    </method>
                </class>
            </package>
            <package name="c">
                <class name="ClassC">
                    <method name="mc" id="2"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package instability for "a" to be 1.0
  And I expect the package instability for "b" to be 0.5
  And I expect the package instability for "c" to be 0.0


Scenario: instability of a package with no neighbors
  Given I have the input file:
    """
    <project>
        <package name=""/>
    </project>
    """

  When I parse the input

  Then I expect the package instability for "::" to be 0.0


Scenario: more than one in and out
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <method name="m1">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
            <package name="b">
                <class name="ClassB">
                    <method name="m1">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
            <package name="c">
                <class name="ClassC">
                    <method name="m1">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
            <package name="d">
                <class name="ClassD">
                    <method name="m1" id="1">
                        <reference target="2"/>
                    </method>
                </class>
            </package>
            <package name="e">
                <class name="ClassE">
                    <method name="m1" id="2"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input

  And I expect the package instability for "d" to be 0.25


Scenario: count multiple connection between the same packages
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <method name="m">
                        <reference target="1"/>
                    </method>
                </class>
                <class name="ClassB">
                    <method name="m">
                        <reference target="1"/>
                    </method>
                </class>
                <class name="ClassC">
                    <method name="m">
                        <reference target="2"/>
                    </method>
                </class>
            </package>
            <package name="b">
                <class name="ClassA">
                    <method name="m" id="1">
                        <reference target="3"/>
                    </method>
                </class>
                <class name="ClassB">
                    <method name="m" id="2">
                        <reference target="3"/>
                    </method>
                </class>
            </package>
            <package name="c">
                <class name="ClassA">
                    <method name="m" id="3"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input

  And I expect the package instability for "b" to be 0.4


Scenario: don't count multiple connection between the same classes
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <method name="m1">
                        <reference target="1"/>
                    </method>
                    <method name="m2">
                        <reference target="1"/>
                    </method>
                    <method name="m3">
                        <reference target="2"/>
                    </method>
                </class>
            </package>
            <package name="b">
                <class name="ClassA">
                    <method name="m1" id="1">
                        <reference target="3"/>
                    </method>
                    <method name="m2" id="2">
                        <reference target="3"/>
                    </method>
                </class>
            </package>
            <package name="c">
                <class name="ClassA">
                    <method name="m1" id="3"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input

  And I expect the package instability for "b" to be 0.5
