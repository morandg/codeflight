# language: en

Feature: calculate the fan-in metric for packages
  As an architect
  I want to know which packages have a low/high fan-in
  In order to fix my project


Scenario: simple fan-in calculation
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <method name="ma">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
            <package name="b">
                <class name="ClassB">
                    <method name="mb" id="1"/>
                </class>
            </package>
            <package name="c">
                <class name="ClassC">
                    <method name="mc">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package fan-in for "b" to be 2


Scenario: does not count self-reference
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="ma">
                    <reference target="1"/>
                </method>
                <field name="fa" id="1"/>
            </class>
            <class name="ClassB">
                <method name="mb">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package fan-in for "::" to be 0


Scenario: count multiple references from the same class only once
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <method name="m1">
                        <reference target="1"/>
                    </method>
                    <method name="m2">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
            <package name="b">
                <class name="ClassB">
                    <method name="mb" id="1"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package fan-in for "b" to be 1


Scenario: count all references from the same package
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <method name="m1">
                        <reference target="1"/>
                    </method>
                </class>
                <class name="ClassB">
                    <method name="m1">
                        <reference target="1"/>
                    </method>
                </class>
                <class name="ClassC">
                    <method name="m1">
                        <reference target="2"/>
                    </method>
                </class>
                <class name="ClassD">
                    <method name="m1">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
            <package name="b">
                <class name="ClassA">
                    <method name="mb" id="1"/>
                </class>
                <class name="ClassB">
                    <method name="mb" id="2"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package fan-in for "b" to be 4


Scenario: count reference from function
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <method name="mb" id="1"/>
                </class>
            </package>
            <package name="b">
                <function name="f">
                    <reference target="1"/>
                </function>
            </package>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package fan-in for "a" to be 1
