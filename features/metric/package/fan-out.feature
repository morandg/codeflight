# language: en

Feature: calculate the fan-out metric for packages
  As an architect
  I want to know which packages have a low/high fan-out
  In order to fix my project


Scenario: simple fan-out calculation
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA" id="1"/>
            </package>
            <package name="b">
                <class name="ClassB">
                    <method name="m1">
                        <reference target="1"/>
                    </method>
                    <method name="m2">
                        <reference target="2"/>
                    </method>
                </class>
            </package>
            <package name="c">
                <class name="ClassC">
                    <method name="mc" id="2"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package fan-out for "b" to be 2


Scenario: does not count self-reference
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="ma">
                    <reference target="1"/>
                </method>
            </class>
            <class name="ClassB">
                <field name="fa" id="1"/>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package fan-out for "::" to be 0


Scenario: count multiple references from the same class to the same class only once
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <method name="m1" id="1"/>
                    <method name="m2" id="2"/>
                </class>
            </package>
            <package name="b">
                <class name="ClassB">
                    <method name="m1">
                        <reference target="1"/>
                    </method>
                    <method name="m2">
                        <reference target="2"/>
                    </method>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package fan-out for "b" to be 1


Scenario: count multiple connection between the same packages
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="b">
                <class name="ClassA">
                    <method name="ba" id="1">
                        <reference target="3"/>
                    </method>
                </class>
                <class name="ClassB">
                    <method name="bb" id="2">
                        <reference target="3"/>
                    </method>
                </class>
            </package>
            <package name="c">
                <class name="ClassA">
                    <method name="ca" id="3"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input

  And I expect the package fan-out for "b" to be 2


Scenario: count reference to sub-package
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="pa">
                <class name="ClassA">
                    <method name="m1" id="1"/>
                </class>
            </package>
            <class name="ClassB">
                <method name="m1">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package fan-out for "::" to be 1


Scenario: count reference to function
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <method name="mb">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
            <package name="f">
                <function name="f" id="1"/>
            </package>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package fan-out for "a" to be 1
