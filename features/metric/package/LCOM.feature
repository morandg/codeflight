# language: en

Feature: calculate the LCOM4 metric for packages
  As an architect
  I want to know which packages have a lack of cohesion
  In order to fix my project


Scenario: simple LCOM4 calculation
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA"/>
            <class name="ClassB"/>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package LCOM4 for "::" to be 2


Scenario: LCOM4 calculation between all connected classes
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="methodA">
                    <reference target="methodB"/>
                    <reference target="methodD"/>
                </method>
            </class>
            <class name="ClassB">
                <method name="methodB" id="methodB"/>
            </class>
            <class name="ClassC">
                <method name="methodC">
                    <reference target="methodB"/>
                </method>
            </class>
            <class name="ClassD">
                <method name="methodD" id="methodD"/>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package LCOM4 for "::" to be 1


Scenario: LCOM4 calculation in different packages
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="methodA">
                    <reference target="methodB"/>
                    <reference target="methodD"/>
                </method>
            </class>
            <class name="ClassB">
                <method name="methodB" id="methodB"/>
            </class>
            <package name="package1">
                <class name="ClassC">
                    <method name="methodC">
                        <reference target="methodB"/>
                    </method>
                </class>
                <class name="ClassD">
                    <method name="methodD" id="methodD"/>
                </class>
                <package name="package2"/>
            </package>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package LCOM4 for "::" to be 1
  And I expect the package LCOM4 for "package1" to be 2
  And I expect the package LCOM4 for "package1::package2" to be 0


Scenario: consider only relations withing the same package
  Given I have the input file:
    """
    <project>
        <package name="">
            <function name="functionA">
                <reference target="functionC"/>
            </function>
            <function name="functionB">
                <reference target="functionC"/>
            </function>
            <package name="package1">
                <function name="functionC" id="functionC"/>
            </package>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package LCOM4 for "::" to be 2


Scenario: include functions in calculation
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="methodA">
                    <reference target="functionA"/>
                </method>
            </class>
            <function name="functionA" id="functionA"/>
            <function name="functionB">
                <reference target="functionA"/>
            </function>
            <function name="functionC"/>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package LCOM4 for "::" to be 2


Scenario: include variables in calculation
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="methodA">
                    <reference target="variableA"/>
                </method>
            </class>
            <variable name="variableA" id="variableA"/>
            <variable name="variableB">
                <reference target="variableA"/>
            </variable>
            <variable name="variableC"/>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package LCOM4 for "::" to be 2


Scenario: LCOM4 calculation for class in class
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <class name="ClassB"/>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the package LCOM4 for "::" to be 1
