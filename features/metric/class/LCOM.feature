# language: en

Feature: calculate the LCOM metric classes
  As an architect
  I want to know which classes have a lack of cohesion
  In order to fix my project


Scenario: simple LCOM4 calculation
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <field name="field1" id="f1"/>
                <field name="field2" id="f2"/>
                <field name="field3" id="f3"/>
                <method name="method1">
                    <reference target="f1"/>
                    <reference target="f2"/>
                </method>
                <method name="method2">
                    <reference target="f3"/>
                </method>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the class LCOM4 for "ClassA" to be 2


Scenario: LCOM4 calculation in different classes
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="methodA">
                    <reference target="f1"/>
                    <reference target="f2"/>
                </method>
                <field name="field1" id="f1"/>
            </class>
            <package name="package1">
                <class name="ClassB">
                    <method name="methodB" id="methodB"/>
                    <field name="field2" id="f2"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the class LCOM4 for "ClassA" to be 1
  And I expect the class LCOM4 for "package1::ClassB" to be 2


Scenario: functions are not considered in calculation
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="methodA">
                    <reference target="f1"/>
                </method>
                <method name="methodB" id="mB"/>
            </class>
            <function name="function1" id="f1">
                <reference target="mB"/>
            </function>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the class LCOM4 for "ClassA" to be 2


Scenario: LCOM4 calculation of class in class
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <field name="field1"/>
                <class name="ClassB">
                    <method name="methodB"/>
                </class>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the class LCOM4 for "ClassA" to be 2
  And I expect the class LCOM4 for "ClassA::ClassB" to be 1
