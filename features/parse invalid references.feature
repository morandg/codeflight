# language: en

Feature: parse invalid references
  As a user of the tool
  I want to get notified about invalid references
  In order to fix my input file


Scenario: report an invalid reference
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <reference target="x"/>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the log output:
    """
    WARNING: reference target not found: x

    """


Scenario: print graph without invalid reference
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <reference target="x"/>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/package-content-dependencies.gv"

  Then I expect to get the output file "-/package-content-dependencies.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            label="ClassA"
            URL="ClassA/index.html"
        ]
    }

    """
