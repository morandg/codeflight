# language: en

Feature: parse unknown nodes
  As a user of the tool
  I want to get notified about unknown nodes
  In order to fix my input file


Scenario: report an error for functions in classes
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <function name="Field2"/>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the log output:
    """
    WARNING: unknown element found: function (4:13)

    """


Scenario: report an error for anything but references in fields
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <field name="Field1" id="3">
                    <class name="1"/>
                    <reference target="3"/>
                    <field name="3"/>
                    <method name="4"/>
                </field>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the log output:
    """
    WARNING: unknown element found: class (5:17)
    WARNING: unknown element found: field (7:17)
    WARNING: unknown element found: method (8:17)

    """


Scenario: report an error for anything but references in functions
  Given I have the input file:
    """
    <project>
        <package name="">
            <function name="f1" id="3">
                <class name="1"/>
                <reference target="3"/>
                <field name="3"/>
                <method name="4"/>
            </function>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the log output:
    """
    WARNING: unknown element found: class (4:13)
    WARNING: unknown element found: field (6:13)
    WARNING: unknown element found: method (7:13)

    """


Scenario: report an error for member and fields in packages
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="1">
                <field name="2"/>
                <package name="3">
                    <method name="4"/>
                </package>
            </package>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the log output:
    """
    WARNING: unknown element found: field (4:13)
    WARNING: unknown element found: method (6:17)

    """
