# language: en

Feature: add CSS an JavaScript
  As a user of the tool
  I wahe result presented in a nice way
  In order see the relevant information easily


Scenario: add a provided CSS and JavaScript
  Given I have the file "input/test.ast":
    """
    <project name="The Super Project">
        <package name="">
            <class name="ClassA"/>
            <class name="ClassB"/>
            <package name="package1">
                <class name="ClassC"/>
            </package>
            <package name="package2"/>
        </package>
    </project>
    """
  And I have the file "input/res/style1.css":
    """
    """
  And I have the file "input/res/style2.css":
    """
    """
  And I have the file "input/res/other.txt":
    """
    """
  And I have the file "input/res/script1.js":
    """
    """

  When I process the file "input/test.ast" with the resources directory "input/res" and the output directory "output"

  Then I expect to get the output file "output/index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <script src="script1.js"></script>
            <link rel="stylesheet" href="style1.css" />
            <link rel="stylesheet" href="style2.css" />
            <title>The Super Project</title>
        </head>
        <body>
            <h1>Project</h1>
            <p>
                <ul>
                    <li>
                        <a href="packages-with-cyclic-dependencies.html">Packages with cylic dependencies</a>
                    </li>
                    <li>
                        <a href="stable-dependency-principle-violations.html">Stable dependency principle violations</a>
                    </li>
                </ul>
            </p>
            <h1>Packages</h1>
            <table class="string-numbers">
                <tr>
                    <th>Name</th>
                    <th>Packages</th>
                    <th>Classes</th>
                    <th>Functions</th>
                    <th>Variables</th>
                    <th>LCOM4</th>
                    <th>fan-in</th>
                    <th>fan-out</th>
                    <th>instability</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/index.html">::</a>
                    </td>
                    <td>2</td>
                    <td>2</td>
                    <td>0</td>
                    <td>0</td>
                    <td>2</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/index.html">package1</a>
                    </td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package2/index.html">package2</a>
                    </td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                </tr>
            </table>
            <h1>Classes</h1>
            <table class="string-numbers">
                <tr>
                    <th>Name</th>
                    <th>Methods</th>
                    <th>Fields</th>
                    <th>LCOM4</th>
                    <th>fan-in</th>
                    <th>fan-out</th>
                    <th>instability</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/ClassA/index.html">ClassA</a>
                    </td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/ClassB/index.html">ClassB</a>
                    </td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/ClassC/index.html">package1::ClassC</a>
                    </td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                </tr>
            </table>
        </body>
    </html>

    """
  And I expect to get the output file "output/-/ClassA/index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <script src="../../script1.js"></script>
            <link rel="stylesheet" href="../../style1.css" />
            <link rel="stylesheet" href="../../style2.css" />
            <title>ClassA</title>
        </head>
        <body>
            <h1>ClassA</h1>
            <p>
                <ul>
                    <li>
                        <a href="intra-class-dependencies.svg">Member dependencies</a>
                    </li>
                    <li>
                        <a href="neighbors.svg">Class neighbors</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """
  And I expect to have at least these files:
    | filename                                             |
    | output/index.html                                    |
    | output/style1.css                                    |
    | output/style2.css                                    |
    | output/script1.js                                    |


Scenario: provide information about the table type
  Given I have the file "input/test.ast":
    """
    <project name="The Super Project">
        <package name="">
            <package name="package1">
                <class name="ClassA" id="1">
                    <reference target="2"/>
                </class>
            </package>
            <package name="package2">
                <class name="ClassB" id="2">
                    <reference target="1"/>
                </class>
            </package>
        </package>
    </project>
    """
  And I have the file "input/res/style.css":
    """
    """

  When I process the file "input/test.ast" with the resources directory "input/res" and the output directory "output"

  Then I expect to get the output file "output/index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <link rel="stylesheet" href="style.css" />
            <title>The Super Project</title>
        </head>
        <body>
            <h1>Project</h1>
            <p>
                <ul>
                    <li>
                        <a href="packages-with-cyclic-dependencies.html">Packages with cylic dependencies</a>
                    </li>
                    <li>
                        <a href="stable-dependency-principle-violations.html">Stable dependency principle violations</a>
                    </li>
                </ul>
            </p>
            <h1>Packages</h1>
            <table class="string-numbers">
                <tr>
                    <th>Name</th>
                    <th>Packages</th>
                    <th>Classes</th>
                    <th>Functions</th>
                    <th>Variables</th>
                    <th>LCOM4</th>
                    <th>fan-in</th>
                    <th>fan-out</th>
                    <th>instability</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/index.html">::</a>
                    </td>
                    <td>2</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/index.html">package1</a>
                    </td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0.50</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package2/index.html">package2</a>
                    </td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0.50</td>
                </tr>
            </table>
            <h1>Classes</h1>
            <table class="string-numbers">
                <tr>
                    <th>Name</th>
                    <th>Methods</th>
                    <th>Fields</th>
                    <th>LCOM4</th>
                    <th>fan-in</th>
                    <th>fan-out</th>
                    <th>instability</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/ClassA/index.html">package1::ClassA</a>
                    </td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0.50</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package2/ClassB/index.html">package2::ClassB</a>
                    </td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0.50</td>
                </tr>
            </table>
        </body>
    </html>

    """
  And I expect to get the output file "output/packages-with-cyclic-dependencies.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <link rel="stylesheet" href="style.css" />
            <title>Packages with cyclic dependencies</title>
        </head>
        <body>
            <h1>Packages with cyclic dependencies</h1>
            <table class="string-numbers">
                <tr>
                    <th>metric</th>
                    <th>value</th>
                </tr>
                <tr>
                    <td>Number of cycles</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>Number of involved dependencies</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>Number of involved packages</td>
                    <td>2</td>
                </tr>
            </table>
            <h2>Involved dependencies</h2>
            <table class="string-string-numbers">
                <tr>
                    <th>from</th>
                    <th>to</th>
                    <th>in cycles</th>
                    <th>stability</th>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~package1,-~package2.svg">package1</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~package1,-~package2.svg">package2</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~package2,-~package1.svg">package2</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~package2,-~package1.svg">package1</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
            </table>
            <h2>Involved packages</h2>
            <table class="string-numbers">
                <tr>
                    <th>Package</th>
                    <th>in cycles</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/index.html">package1</a>
                    </td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package2/index.html">package2</a>
                    </td>
                    <td>1</td>
                </tr>
            </table>
        </body>
    </html>

    """
