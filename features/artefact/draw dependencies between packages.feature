# language: en

Feature: draw the dependencies between packages
  As an architect
  I want to know the dependencies between packages
  In order to reduce them

Scenario: generate a graph with the dependencies between 2 packages
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="package1">
                <class name="ClassA">
                    <method name="Method1" id="m1"/>
                </class>
                <package name="package2">
                    <class name="ClassB">
                        <method name="Method2">
                            <reference target="m1"/>
                        </method>
                    </class>
                </package>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "dependencies-between-packages/-~package1~package2,-~package1.gv"

  Then I expect to get the output file "dependencies-between-packages/-~package1~package2,-~package1.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="package1::package2"
            URL="../-/package1/package2/index.html"
            _node0 [
                shape="box"
                label="ClassB"
                URL="../-/package1/package2/ClassB/index.html"
            ]
        }
        subgraph cluster_1 {
            label="package1"
            URL="../-/package1/index.html"
            _node1 [
                shape="box"
                label="ClassA"
                URL="../-/package1/ClassA/index.html"
            ]
        }
        _node0 -> _node1
    }

    """


Scenario: don't draw duplicated edges
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="package1">
                <class name="ClassA">
                    <method name="Method1" id="m1"/>
                    <field name="Field1" id="f1"/>
                </class>
            </package>
            <package name="package2">
                <class name="ClassB">
                    <method name="Method2">
                        <reference target="m1"/>
                    </method>
                    <method name="Field2">
                        <reference target="f1"/>
                    </method>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "dependencies-between-packages/-~package1,-~package2.gv"

  Then I expect to get the output file "dependencies-between-packages/-~package1,-~package2.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="package1"
            URL="../-/package1/index.html"
            _node0 [
                shape="box"
                label="ClassA"
                URL="../-/package1/ClassA/index.html"
            ]
        }
        subgraph cluster_1 {
            label="package2"
            URL="../-/package2/index.html"
            _node1 [
                shape="box"
                label="ClassB"
                URL="../-/package2/ClassB/index.html"
            ]
        }
        _node1 -> _node0
    }

    """


Scenario: don't draw edges to itself
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="package1">
                <class name="ClassA">
                    <method name="Method1">
                        <reference target="f1"/>
                    </method>
                    <field name="Field1" id="f1"/>
                </class>
            </package>
            <package name="package2">
                <class name="ClassB">
                    <method name="Field2">
                        <reference target="f1"/>
                    </method>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "dependencies-between-packages/-~package1,-~package2.gv"

  Then I expect to get the output file "dependencies-between-packages/-~package1,-~package2.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="package1"
            URL="../-/package1/index.html"
            _node0 [
                shape="box"
                label="ClassA"
                URL="../-/package1/ClassA/index.html"
            ]
        }
        subgraph cluster_1 {
            label="package2"
            URL="../-/package2/index.html"
            _node1 [
                shape="box"
                label="ClassB"
                URL="../-/package2/ClassB/index.html"
            ]
        }
        _node1 -> _node0
    }

    """


Scenario: generate a graph with the dependencies between 2 packages with variables in them
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="package1">
                <variable name="v1" id="v1"/>
                <package name="package2">
                    <function name="f1">
                        <reference target="v1"/>
                    </variable>
                </package>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "dependencies-between-packages/-~package1~package2,-~package1.gv"

  Then I expect to get the output file "dependencies-between-packages/-~package1~package2,-~package1.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="package1::package2"
            URL="../-/package1/package2/index.html"
            _node0 [
                shape="box"
                label="f1"
                URL="../-/package1/package2/f1/index.html"
            ]
        }
        subgraph cluster_1 {
            label="package1"
            URL="../-/package1/index.html"
            _node1 [
                shape="box"
                label="v1"
                URL="../-/package1/v1/index.html"
            ]
        }
        _node0 -> _node1
    }

    """


Scenario: generate a graph with the dependencies between root and 2 sub package
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="p1">
                <class name="ClassB">
                    <method name="Method1" id="m1">
                        <reference target="m2"/>
                    </method>
                </class>
            </package>
            <package name="p2">
                <class name="ClassC">
                    <method name="Method3">
                        <reference target="m1"/>
                    </method>
                </class>
            </package>
            <class name="ClassA">
                <method name="Method2" id="m2"/>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "dependencies-between-packages/-~p1,-.gv"
  And I generate the file "dependencies-between-packages/-~p2,-~p1.gv"
  And I process the queue until empty

  Then I expect to get the output file "dependencies-between-packages/-~p1,-.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="p1"
            URL="../-/p1/index.html"
            _node0 [
                shape="box"
                label="ClassB"
                URL="../-/p1/ClassB/index.html"
            ]
        }
        subgraph cluster_1 {
            label="::"
            URL="../-/index.html"
            _node1 [
                shape="box"
                label="ClassA"
                URL="../-/ClassA/index.html"
            ]
        }
        _node0 -> _node1
    }

    """
  And I expect to get the output file "dependencies-between-packages/-~p2,-~p1.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="p2"
            URL="../-/p2/index.html"
            _node0 [
                shape="box"
                label="ClassC"
                URL="../-/p2/ClassC/index.html"
            ]
        }
        subgraph cluster_1 {
            label="p1"
            URL="../-/p1/index.html"
            _node1 [
                shape="box"
                label="ClassB"
                URL="../-/p1/ClassB/index.html"
            ]
        }
        _node0 -> _node1
    }

    """


Scenario: only include nodes that are involved in the dependencies between packages
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="p1">
                <class name="C1" id="1"/>
                <class name="C1A">
                    <reference target="1"/>
                </class>
            </package>
            <package name="p2">
                <class name="C2">
                    <reference target="1"/>
                </class>
            </package>
            <package name="p3">
                <class name="C3"/>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "dependencies-between-packages/-~p1.gv"
  And I generate the file "dependencies-between-packages/-~p1,-~p2.gv"
  And I generate the file "dependencies-between-packages/-~p1,-~p2,-~p3.gv"
  And I process the queue until empty

  Then I expect to get the output file "dependencies-between-packages/-~p1.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="p1"
            URL="../-/p1/index.html"
        }
    }

    """
  And I expect to get the output file "dependencies-between-packages/-~p1,-~p2.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="p1"
            URL="../-/p1/index.html"
            _node0 [
                shape="box"
                label="C1"
                URL="../-/p1/C1/index.html"
            ]
        }
        subgraph cluster_1 {
            label="p2"
            URL="../-/p2/index.html"
            _node1 [
                shape="box"
                label="C2"
                URL="../-/p2/C2/index.html"
            ]
        }
        _node1 -> _node0
    }

    """
  And I expect to get the output file "dependencies-between-packages/-~p1,-~p2,-~p3.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="p1"
            URL="../-/p1/index.html"
            _node0 [
                shape="box"
                label="C1"
                URL="../-/p1/C1/index.html"
            ]
        }
        subgraph cluster_1 {
            label="p2"
            URL="../-/p2/index.html"
            _node1 [
                shape="box"
                label="C2"
                URL="../-/p2/C2/index.html"
            ]
        }
        subgraph cluster_2 {
            label="p3"
            URL="../-/p3/index.html"
        }
        _node1 -> _node0
    }

    """


Scenario: include class inside class in dependency graph
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="A">
                    <class name="AB" id="1"/>
                </class>
            </package>
            <package name="b">
                <class name="B">
                    <reference target="1"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "dependencies-between-packages/-~a,-~b.gv"

  Then I expect to get the output file "dependencies-between-packages/-~a,-~b.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="a"
            URL="../-/a/index.html"
            _node0 [
                shape="box"
                label="A::AB"
                URL="../-/a/A/AB/index.html"
            ]
        }
        subgraph cluster_1 {
            label="b"
            URL="../-/b/index.html"
            _node1 [
                shape="box"
                label="B"
                URL="../-/b/B/index.html"
            ]
        }
        _node1 -> _node0
    }

    """


Scenario: don't run dot when the graph didn't change
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="p1">
                <class name="ClassA" id="1"/>
            </package>
            <package name="p2">
                <class name="ClassB">
                    <reference target="1"/>
                </class>
            </package>
        </package>
    </project>
    """
  And I have the file "dependencies-between-packages/-~p1,-~p2.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="p1"
            URL="../-/p1/index.html"
            _node0 [
                shape="box"
                label="ClassA"
                URL="../-/p1/ClassA/index.html"
            ]
        }
        subgraph cluster_1 {
            label="p2"
            URL="../-/p2/index.html"
            _node1 [
                shape="box"
                label="ClassB"
                URL="../-/p2/ClassB/index.html"
            ]
        }
        _node1 -> _node0
    }

    """
  And I have the file "dependencies-between-packages/-~p1,-~p2.svg":
    """
    old content
    """

  When I parse the input
  And I generate the file "dependencies-between-packages/-~p1,-~p2.svg"

  Then I expect to get the output file "dependencies-between-packages/-~p1,-~p2.svg":
    """
    old content
    """


Scenario: run dot when the graph didn't change but there is no svg
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="p1">
                <class name="ClassA" id="1"/>
            </package>
            <package name="p2">
                <class name="ClassB">
                    <reference target="1"/>
                </class>
            </package>
        </package>
    </project>
    """
  And I have the file "dependencies-between-packages/-~p1,-~p2.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="p1"
            URL="../-/p1/index.html"
            _node0 [
                shape="box"
                label="ClassA"
                URL="../-/p1/ClassA/index.html"
            ]
        }
        subgraph cluster_1 {
            label="p2"
            URL="../-/p2/index.html"
            _node1 [
                shape="box"
                label="ClassB"
                URL="../-/p2/ClassB/index.html"
            ]
        }
        _node1 -> _node0
    }

    """

  When I parse the input
  And I generate the file "dependencies-between-packages/-~p1,-~p2.svg"

  Then I expect to get the output file "dependencies-between-packages/-~p1,-~p2.svg":
    """
    dummy file
    """
