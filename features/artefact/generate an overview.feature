# language: en

Feature: generate an overview
  As a user of the tool
  I want to easily see the most important information about my project
  In order to get used to the tool and already get some insights into my project


Scenario: print all packages and classes
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <class name="ClassA"/>
            <class name="ClassB"/>
            <package name="package1">
                <class name="ClassC"/>
                <package name="package2">
                    <class name="ClassD"/>
                    <class name="ClassE"/>
                </package>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "index.html"
  And I process the queue until empty

  Then I expect to get the output file "index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>The Super Project</title>
        </head>
        <body>
            <h1>Project</h1>
            <p>
                <ul>
                    <li>
                        <a href="packages-with-cyclic-dependencies.html">Packages with cylic dependencies</a>
                    </li>
                    <li>
                        <a href="stable-dependency-principle-violations.html">Stable dependency principle violations</a>
                    </li>
                </ul>
            </p>
            <h1>Packages</h1>
            <table>
                <tr>
                    <th>Name</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/index.html">::</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/index.html">package1</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/package2/index.html">package1::package2</a>
                    </td>
                </tr>
            </table>
            <h1>Classes</h1>
            <table>
                <tr>
                    <th>Name</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/ClassA/index.html">ClassA</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/ClassB/index.html">ClassB</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/ClassC/index.html">package1::ClassC</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/package2/ClassD/index.html">package1::package2::ClassD</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/package2/ClassE/index.html">package1::package2::ClassE</a>
                    </td>
                </tr>
            </table>
        </body>
    </html>

    """
    And I expect to get the output file "-/ClassA/index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>ClassA</title>
        </head>
        <body>
            <h1>ClassA</h1>
            <p>
                <ul>
                    <li>
                        <a href="intra-class-dependencies.svg">Member dependencies</a>
                    </li>
                    <li>
                        <a href="neighbors.svg">Class neighbors</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """
    And I expect to get the output file "-/package1/package2/index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>package1::package2</title>
        </head>
        <body>
            <h1>package1::package2</h1>
            <p>
                <ul>
                    <li>
                        <a href="package-content-dependencies.svg">Package content dependencies</a>
                    </li>
                    <li>
                        <a href="package-dependencies.svg">Package dependencies</a>
                    </li>
                    <li>
                        <a href="neighbors.svg">Package neighbors</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """


Scenario: print the functions
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <function name="funcA"/>
            <function name="funcB"/>
            <package name="package1">
                <function name="funcC"/>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "index.html"

  Then I expect to get the output file "index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>The Super Project</title>
        </head>
        <body>
            <h1>Project</h1>
            <p>
                <ul>
                    <li>
                        <a href="packages-with-cyclic-dependencies.html">Packages with cylic dependencies</a>
                    </li>
                    <li>
                        <a href="stable-dependency-principle-violations.html">Stable dependency principle violations</a>
                    </li>
                </ul>
            </p>
            <h1>Packages</h1>
            <table>
                <tr>
                    <th>Name</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/index.html">::</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/index.html">package1</a>
                    </td>
                </tr>
            </table>
            <h1>Functions</h1>
            <table>
                <tr>
                    <th>Name</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/funcA/index.html">funcA</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/funcB/index.html">funcB</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/funcC/index.html">package1::funcC</a>
                    </td>
                </tr>
            </table>
        </body>
    </html>

    """


Scenario: print the variables
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <variable name="varA"/>
            <package name="p1">
                <variable name="varC"/>
            </package>
            <variable name="varB"/>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "index.html"

  Then I expect to get the output file "index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>The Super Project</title>
        </head>
        <body>
            <h1>Project</h1>
            <p>
                <ul>
                    <li>
                        <a href="packages-with-cyclic-dependencies.html">Packages with cylic dependencies</a>
                    </li>
                    <li>
                        <a href="stable-dependency-principle-violations.html">Stable dependency principle violations</a>
                    </li>
                </ul>
            </p>
            <h1>Packages</h1>
            <table>
                <tr>
                    <th>Name</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/index.html">::</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/p1/index.html">p1</a>
                    </td>
                </tr>
            </table>
            <h1>Variables</h1>
            <table>
                <tr>
                    <th>Name</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/varA/index.html">varA</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/p1/varC/index.html">p1::varC</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/varB/index.html">varB</a>
                    </td>
                </tr>
            </table>
        </body>
    </html>

    """


Scenario: print all package, class, function and variable metrics
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <class name="ClassA">
                <method name="m1">
                    <reference target="f2"/>
                    <reference target="m2"/>
                </method>
                <field name="f1"/>
                <field name="f2" id="f2"/>
                <method name="m2" id="m2">
                    <reference target="f3"/>
                </method>
                <field name="f3" id="f3"/>
            </class>
            <package name="package">
                <class name="ClassA">
                    <method name="m1">
                        <reference target="f3"/>
                        <reference target="func3"/>
                    </method>
                </class>
                <function name="f1">
                    <reference target="func3"/>
                </function>
                <package name="package1"/>
                <variable name="v1" id="v1"/>
                <function name="f2"/>
                <variable name="v2"/>
                <function name="f3" id="func3">
                    <reference target="v1"/>
                </function>
            </package>
        </package>
    </project>
    """
  And I enable all metrics

  When I parse the input
  And I generate the file "index.html"

  Then I expect to get the output file "index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>The Super Project</title>
        </head>
        <body>
            <h1>Project</h1>
            <p>
                <ul>
                    <li>
                        <a href="packages-with-cyclic-dependencies.html">Packages with cylic dependencies</a>
                    </li>
                    <li>
                        <a href="stable-dependency-principle-violations.html">Stable dependency principle violations</a>
                    </li>
                </ul>
            </p>
            <h1>Packages</h1>
            <table>
                <tr>
                    <th>Name</th>
                    <th>Packages</th>
                    <th>Classes</th>
                    <th>Functions</th>
                    <th>Variables</th>
                    <th>LCOM4</th>
                    <th>fan-in</th>
                    <th>fan-out</th>
                    <th>instability</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/index.html">::</a>
                    </td>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0.00</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package/index.html">package</a>
                    </td>
                    <td>1</td>
                    <td>1</td>
                    <td>3</td>
                    <td>2</td>
                    <td>3</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1.00</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package/package1/index.html">package::package1</a>
                    </td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                </tr>
            </table>
            <h1>Classes</h1>
            <table>
                <tr>
                    <th>Name</th>
                    <th>Methods</th>
                    <th>Fields</th>
                    <th>LCOM4</th>
                    <th>fan-in</th>
                    <th>fan-out</th>
                    <th>instability</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/ClassA/index.html">ClassA</a>
                    </td>
                    <td>2</td>
                    <td>3</td>
                    <td>2</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0.00</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package/ClassA/index.html">package::ClassA</a>
                    </td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>2</td>
                    <td>1.00</td>
                </tr>
            </table>
            <h1>Functions</h1>
            <table>
                <tr>
                    <th>Name</th>
                    <th>fan-in</th>
                    <th>fan-out</th>
                    <th>instability</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/package/f1/index.html">package::f1</a>
                    </td>
                    <td>0</td>
                    <td>1</td>
                    <td>1.00</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package/f2/index.html">package::f2</a>
                    </td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package/f3/index.html">package::f3</a>
                    </td>
                    <td>2</td>
                    <td>1</td>
                    <td>0.33</td>
                </tr>
            </table>
            <h1>Variables</h1>
            <table>
                <tr>
                    <th>Name</th>
                    <th>fan-in</th>
                    <th>fan-out</th>
                    <th>instability</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/package/v1/index.html">package::v1</a>
                    </td>
                    <td>1</td>
                    <td>0</td>
                    <td>0.00</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package/v2/index.html">package::v2</a>
                    </td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                </tr>
            </table>
        </body>
    </html>

    """


Scenario: print class in class
  Given I have the input file:
    """
    <project name="test">
        <package name="">
            <class name="ClassA">
                <class name="ClassB"/>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "index.html"
  And I process the queue until empty

  Then I expect to get the output file "index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>test</title>
        </head>
        <body>
            <h1>Project</h1>
            <p>
                <ul>
                    <li>
                        <a href="packages-with-cyclic-dependencies.html">Packages with cylic dependencies</a>
                    </li>
                    <li>
                        <a href="stable-dependency-principle-violations.html">Stable dependency principle violations</a>
                    </li>
                </ul>
            </p>
            <h1>Packages</h1>
            <table>
                <tr>
                    <th>Name</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/index.html">::</a>
                    </td>
                </tr>
            </table>
            <h1>Classes</h1>
            <table>
                <tr>
                    <th>Name</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/ClassA/index.html">ClassA</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/ClassA/ClassB/index.html">ClassA::ClassB</a>
                    </td>
                </tr>
            </table>
        </body>
    </html>

    """
    And I expect to get the output file "-/ClassA/index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>ClassA</title>
        </head>
        <body>
            <h1>ClassA</h1>
            <p>
                <ul>
                    <li>
                        <a href="intra-class-dependencies.svg">Member dependencies</a>
                    </li>
                    <li>
                        <a href="neighbors.svg">Class neighbors</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """
    And I expect to get the output file "-/ClassA/ClassB/index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>ClassA::ClassB</title>
        </head>
        <body>
            <h1>ClassA::ClassB</h1>
            <p>
                <ul>
                    <li>
                        <a href="intra-class-dependencies.svg">Member dependencies</a>
                    </li>
                    <li>
                        <a href="neighbors.svg">Class neighbors</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """
