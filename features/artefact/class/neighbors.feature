# language: en

Feature: draw class neighbors
  As an architect
  I want to see the neighbors of a class
  In order to see all dependencies


Scenario: draw only the neighbors of the class
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="m1">
                    <reference target="b"/>
                </method>
                <method name="m2">
                    <reference target="d"/>
                </method>
            </class>
            <class name="ClassB" id="b"/>
            <class name="ClassC" id="c"/>
            <class name="ClassD" id="d">
                <method name="m3">
                    <reference target="c"/>
                </method>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/ClassA/neighbors.gv"

  Then I expect to get the output file "-/ClassA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="ClassA"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                label="ClassB"
                URL="../ClassB/index.html"
            ]
            _node2 [
                shape="box"
                label="ClassD"
                URL="../ClassD/index.html"
            ]
        }
        _node0 -> _node1
        _node0 -> _node2
    }

    """


Scenario: don't draw dependencies between neighbors of interested class
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="m1">
                    <reference target="b"/>
                </method>
                <method name="m2">
                    <reference target="c"/>
                </method>
            </class>
            <class name="ClassB" id="b"/>
            <class name="ClassC" id="c">
                <method name="m3">
                    <reference target="b"/>
                </method>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/ClassA/neighbors.gv"

  Then I expect to get the output file "-/ClassA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="ClassA"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                label="ClassB"
                URL="../ClassB/index.html"
            ]
            _node2 [
                shape="box"
                label="ClassC"
                URL="../ClassC/index.html"
            ]
        }
        _node0 -> _node1
        _node0 -> _node2
    }

    """


Scenario: draw dependencies to other packages
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA" id="a">
                <method name="m1">
                    <reference target="b"/>
                </method>
                <method name="m2">
                    <reference target="c"/>
                </method>
            </class>
            <package name="p1">
                <class name="ClassB" id="b"/>
                <package name="p2">
                    <class name="ClassC" id="c"/>
                </package>
                <class name="ClassD" id="d">
                    <method name="m3">
                        <reference target="a"/>
                    </method>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/ClassA/neighbors.gv"

  Then I expect to get the output file "-/ClassA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="ClassA"
                URL="index.html"
            ]
        }
        subgraph cluster_1 {
            label="p1"
            URL="../p1/index.html"
            _node1 [
                shape="box"
                label="ClassB"
                URL="../p1/ClassB/index.html"
            ]
            _node2 [
                shape="box"
                label="ClassD"
                URL="../p1/ClassD/index.html"
            ]
        }
        subgraph cluster_2 {
            label="p1::p2"
            URL="../p1/p2/index.html"
            _node3 [
                shape="box"
                label="ClassC"
                URL="../p1/p2/ClassC/index.html"
            ]
        }
        _node0 -> _node1 [
            URL="../../dependencies-between-packages/-,-~p1.svg"
        ]
        _node0 -> _node3 [
            URL="../../dependencies-between-packages/-,-~p1~p2.svg"
        ]
        _node2 -> _node0 [
            URL="../../dependencies-between-packages/-~p1,-.svg"
        ]
    }

    """


Scenario: draw the encapsulated class neighbor of the class
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA" id="1">
                <class name="ClassB">
                    <reference target="1"/>
                    <reference target="2"/>
                </class>
                <class name="ClassC" id="2"/>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/ClassA/ClassB/neighbors.gv"

  Then I expect to get the output file "-/ClassA/ClassB/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="ClassA"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="ClassB"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                label="ClassC"
                URL="../ClassC/index.html"
            ]
        }
        subgraph cluster_1 {
            label="::"
            URL="../../index.html"
            _node2 [
                shape="box"
                label="ClassA"
                URL="../index.html"
            ]
        }
        _node0 -> _node2
        _node0 -> _node1
    }

    """


Scenario: don't draw duplicated connections
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="m1">
                    <reference target="b"/>
                    <reference target="b"/>
                </method>
            </class>
            <class name="ClassB" id="b"/>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/ClassA/neighbors.gv"

  Then I expect to get the output file "-/ClassA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="ClassA"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                label="ClassB"
                URL="../ClassB/index.html"
            ]
        }
        _node0 -> _node1
    }

    """


Scenario: don't draw connection to itself
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="m1">
                    <reference target="2"/>
                </method>
                <method name="m2" id="2"/>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/ClassA/neighbors.gv"

  Then I expect to get the output file "-/ClassA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="ClassA"
                URL="index.html"
            ]
        }
    }

    """


Scenario: draw cycles
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA" id="a">
                <method name="m1">
                    <reference target="b"/>
                </method>
            </class>
            <class name="ClassB" id="b">
                <method name="m2">
                    <reference target="a"/>
                </method>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/ClassA/neighbors.gv"

  Then I expect to get the output file "-/ClassA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="ClassA"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                label="ClassB"
                URL="../ClassB/index.html"
            ]
        }
        _node0 -> _node1
        _node1 -> _node0
    }

    """


Scenario: draw function neighbors
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="m1" id="1">
                    <reference target="2"/>
                </method>
            </class>
            <function name="funcB" id="2"/>
            <function name="funcC">
                <reference target="1"/>
            </function>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/ClassA/neighbors.gv"

  Then I expect to get the output file "-/ClassA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="ClassA"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                label="funcB"
                URL="../funcB/index.html"
            ]
            _node2 [
                shape="box"
                label="funcC"
                URL="../funcC/index.html"
            ]
        }
        _node0 -> _node1
        _node2 -> _node0
    }

    """
