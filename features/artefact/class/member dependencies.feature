# language: en

Feature: draw intra class dependencies
  As an architect
  I want to see which methods access which fields
  In order to see if I can split the class


Scenario: draw a simple class
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <field name="Field1" id="f1"/>
                <method name="Method1">
                    <reference target="f2"/>
                </method>
                <method name="Method2" id="m2">
                    <reference target="f1"/>
                </method>
                <field name="Field2" id="f2"/>
                <method name="Method3">
                    <reference target="m2"/>
                </method>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/ClassA/intra-class-dependencies.gv"

  Then I expect to get the output file "-/ClassA/intra-class-dependencies.gv":
    """
    digraph {
        rankdir=LR
        label="ClassA"
        subgraph cluster_0 {
            label="methods"
            _node0 [
                shape="box"
                label="Method1"
            ]
            _node1 [
                shape="box"
                label="Method2"
            ]
            _node2 [
                shape="box"
                label="Method3"
            ]
        }
        subgraph cluster_1 {
            label="fields"
            _node3 [
                shape="box"
                label="Field1"
            ]
            _node4 [
                shape="box"
                label="Field2"
            ]
        }
        _node0 -> _node4
        _node1 -> _node3
        _node2 -> _node1
    }

    """


Scenario: draw classes in packages
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="Package1">
                <class name="ClassA" id="c1">
                    <field name="Field1"/>
                </class>
                <package name="Package2">
                    <class name="ClassB">
                        <method name="Method1">
                            <reference target="c1"/>
                        </method>
                    </class>
                </package>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/Package1/ClassA/intra-class-dependencies.gv"
  And I generate the file "-/Package1/Package2/ClassB/intra-class-dependencies.gv"

  Then I expect to get the output file "-/Package1/ClassA/intra-class-dependencies.gv":
    """
    digraph {
        rankdir=LR
        label="ClassA"
        subgraph cluster_0 {
            label="fields"
            _node0 [
                shape="box"
                label="Field1"
            ]
        }
    }

    """
  And I expect to get the output file "-/Package1/Package2/ClassB/intra-class-dependencies.gv":
    """
    digraph {
        rankdir=LR
        label="ClassB"
        subgraph cluster_0 {
            label="methods"
            _node0 [
                shape="box"
                label="Method1"
            ]
        }
    }

    """


Scenario: draw a dependency to a class definition inside a class
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <class name="ClassB" id="1"/>
                <field name="Field1">
                    <reference target="1"/>
                </field>
                <method name="Method1">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/ClassA/intra-class-dependencies.gv"

  Then I expect to get the output file "-/ClassA/intra-class-dependencies.gv":
    """
    digraph {
        rankdir=LR
        label="ClassA"
        subgraph cluster_0 {
            label="methods"
            _node0 [
                shape="box"
                label="Method1"
            ]
        }
        subgraph cluster_1 {
            label="fields"
            _node1 [
                shape="box"
                label="Field1"
            ]
        }
        subgraph cluster_2 {
            label="classes"
            _node2 [
                shape="box"
                label="ClassB"
                URL="ClassB/index.html"
            ]
        }
        _node1 -> _node2
        _node0 -> _node2
    }

    """


Scenario: draw dependency only once
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <field name="Field1" id="1"/>
                <method name="Method1">
                    <reference target="1"/>
                    <reference target="1"/>
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/ClassA/intra-class-dependencies.gv"

  Then I expect to get the output file "-/ClassA/intra-class-dependencies.gv":
    """
    digraph {
        rankdir=LR
        label="ClassA"
        subgraph cluster_0 {
            label="methods"
            _node0 [
                shape="box"
                label="Method1"
            ]
        }
        subgraph cluster_1 {
            label="fields"
            _node1 [
                shape="box"
                label="Field1"
            ]
        }
        _node0 -> _node1
    }

    """


Scenario: use correct link for a class definition inside a class in a sub package
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="sub">
                <class name="ClassA">
                    <class name="ClassB"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/sub/ClassA/intra-class-dependencies.gv"

  Then I expect to get the output file "-/sub/ClassA/intra-class-dependencies.gv":
    """
    digraph {
        rankdir=LR
        label="ClassA"
        subgraph cluster_0 {
            label="classes"
            _node0 [
                shape="box"
                label="ClassB"
                URL="ClassB/index.html"
            ]
        }
    }

    """
