# language: en

Feature: generate an class overview
  As a user of the tool
  I want to easily see all output for a class
  In order to get used to the tool and see all possible information


Scenario: print a simple class overview
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <class name="ClassA"/>
            <class name="ClassB"/>
            <package name="package1">
                <class name="ClassC"/>
                <package name="package2">
                    <class name="ClassD"/>
                    <class name="ClassE"/>
                </package>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/ClassA/index.html"

  Then I expect to get the output file "-/ClassA/index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>ClassA</title>
        </head>
        <body>
            <h1>ClassA</h1>
            <p>
                <ul>
                    <li>
                        <a href="intra-class-dependencies.svg">Member dependencies</a>
                    </li>
                    <li>
                        <a href="neighbors.svg">Class neighbors</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """
