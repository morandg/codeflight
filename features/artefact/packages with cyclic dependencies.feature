# language: en

Feature: Packages with cyclic dependencies
  As an architect
  I want easily see if and which packages have cyclic dependencies
  In order to remove the cycles


Scenario: print a list of packages with cyclic dependencies
  Given I enable all metrics
  And I have the input file:
    """
    <project name="">
        <package name="">
            <package name="a">
                <class name="ClassA" id="1">
                    <reference target="2"/>
                </class>
            </package>
            <package name="b">
                <class name="ClassB" id="2">
                    <reference target="1"/>
                </class>
            </package>
            <package name="c">
                <class name="ClassC">
                    <reference target="1"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "packages-with-cyclic-dependencies.html"
  And I process the queue until empty

  Then I expect to get the output file "packages-with-cyclic-dependencies.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Packages with cyclic dependencies</title>
        </head>
        <body>
            <h1>Packages with cyclic dependencies</h1>
            <table>
                <tr>
                    <th>metric</th>
                    <th>value</th>
                </tr>
                <tr>
                    <td>Number of cycles</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>Number of involved dependencies</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>Number of involved packages</td>
                    <td>2</td>
                </tr>
            </table>
            <h2>Involved dependencies</h2>
            <table>
                <tr>
                    <th>from</th>
                    <th>to</th>
                    <th>in cycles</th>
                    <th>stability</th>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~a,-~b.svg">a</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~a,-~b.svg">b</a>
                    </td>
                    <td>1</td>
                    <td>-0.167</td>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~b,-~a.svg">b</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~b,-~a.svg">a</a>
                    </td>
                    <td>1</td>
                    <td>0.167</td>
                </tr>
            </table>
            <h2>Involved packages</h2>
            <table>
                <tr>
                    <th>Package</th>
                    <th>in cycles</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/a/index.html">a</a>
                    </td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/b/index.html">b</a>
                    </td>
                    <td>1</td>
                </tr>
            </table>
        </body>
    </html>

    """


Scenario: reduce output when no cycles are found
  Given I have the input file:
    """
    <project name="">
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <reference target="2"/>
                </class>
            </package>
            <package name="b">
                <class name="ClassB" id="2"/>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "packages-with-cyclic-dependencies.html"
  And I process the queue until empty

  Then I expect to get the output file "packages-with-cyclic-dependencies.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Packages with cyclic dependencies</title>
        </head>
        <body>
            <h1>Packages with cyclic dependencies</h1>
            <table>
                <tr>
                    <th>metric</th>
                    <th>value</th>
                </tr>
                <tr>
                    <td>Number of cycles</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Number of involved dependencies</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Number of involved packages</td>
                    <td>0</td>
                </tr>
            </table>
        </body>
    </html>

    """


Scenario: include global package
  Given I have the input file:
    """
    <project name="">
        <package name="">
            <package name="a">
                <class name="ClassA" id="1">
                    <reference target="2"/>
                </class>
            </package>
            <class name="ClassB" id="2">
                <reference target="1"/>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "packages-with-cyclic-dependencies.html"
  And I process the queue until empty

  Then I expect to get the output file "packages-with-cyclic-dependencies.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Packages with cyclic dependencies</title>
        </head>
        <body>
            <h1>Packages with cyclic dependencies</h1>
            <table>
                <tr>
                    <th>metric</th>
                    <th>value</th>
                </tr>
                <tr>
                    <td>Number of cycles</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>Number of involved dependencies</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>Number of involved packages</td>
                    <td>2</td>
                </tr>
            </table>
            <h2>Involved dependencies</h2>
            <table>
                <tr>
                    <th>from</th>
                    <th>to</th>
                    <th>in cycles</th>
                    <th>stability</th>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-,-~a.svg">::</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-,-~a.svg">a</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~a,-.svg">a</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~a,-.svg">::</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
            </table>
            <h2>Involved packages</h2>
            <table>
                <tr>
                    <th>Package</th>
                    <th>in cycles</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/index.html">::</a>
                    </td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/a/index.html">a</a>
                    </td>
                    <td>1</td>
                </tr>
            </table>
        </body>
    </html>

    """


Scenario: detect cyclic dependency with 3 packages
  Given I have the input file:
    """
    <project name="">
        <package name="">
            <package name="a">
                <class name="ClassA" id="1">
                    <reference target="2"/>
                </class>
            </package>
            <package name="b">
                <class name="ClassB" id="2">
                    <reference target="3"/>
                </class>
            </package>
            <package name="c">
                <class name="ClassC" id="3">
                    <reference target="1"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "packages-with-cyclic-dependencies.html"
  And I process the queue until empty

  Then I expect to get the output file "packages-with-cyclic-dependencies.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Packages with cyclic dependencies</title>
        </head>
        <body>
            <h1>Packages with cyclic dependencies</h1>
            <table>
                <tr>
                    <th>metric</th>
                    <th>value</th>
                </tr>
                <tr>
                    <td>Number of cycles</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>Number of involved dependencies</td>
                    <td>3</td>
                </tr>
                <tr>
                    <td>Number of involved packages</td>
                    <td>3</td>
                </tr>
            </table>
            <h2>Involved dependencies</h2>
            <table>
                <tr>
                    <th>from</th>
                    <th>to</th>
                    <th>in cycles</th>
                    <th>stability</th>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~a,-~b.svg">a</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~a,-~b.svg">b</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~b,-~c.svg">b</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~b,-~c.svg">c</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~c,-~a.svg">c</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~c,-~a.svg">a</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
            </table>
            <h2>Involved packages</h2>
            <table>
                <tr>
                    <th>Package</th>
                    <th>in cycles</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/a/index.html">a</a>
                    </td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/b/index.html">b</a>
                    </td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/c/index.html">c</a>
                    </td>
                    <td>1</td>
                </tr>
            </table>
        </body>
    </html>

    """


Scenario: detect 2 independent cycles
  Given I have the input file:
    """
    <project name="">
        <package name="">
            <package name="a">
                <class name="ClassA" id="1">
                    <reference target="2"/>
                </class>
            </package>
            <package name="b">
                <class name="ClassB" id="2">
                    <reference target="1"/>
                </class>
            </package>
            <package name="c">
                <class name="ClassC" id="3">
                    <reference target="4"/>
                </class>
            </package>
            <package name="d">
                <class name="ClassD" id="4">
                    <reference target="3"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "packages-with-cyclic-dependencies.html"
  And I process the queue until empty

  Then I expect to get the output file "packages-with-cyclic-dependencies.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Packages with cyclic dependencies</title>
        </head>
        <body>
            <h1>Packages with cyclic dependencies</h1>
            <table>
                <tr>
                    <th>metric</th>
                    <th>value</th>
                </tr>
                <tr>
                    <td>Number of cycles</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>Number of involved dependencies</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td>Number of involved packages</td>
                    <td>4</td>
                </tr>
            </table>
            <h2>Involved dependencies</h2>
            <table>
                <tr>
                    <th>from</th>
                    <th>to</th>
                    <th>in cycles</th>
                    <th>stability</th>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~a,-~b.svg">a</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~a,-~b.svg">b</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~b,-~a.svg">b</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~b,-~a.svg">a</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~c,-~d.svg">c</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~c,-~d.svg">d</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~d,-~c.svg">d</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~d,-~c.svg">c</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
            </table>
            <h2>Involved packages</h2>
            <table>
                <tr>
                    <th>Package</th>
                    <th>in cycles</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/a/index.html">a</a>
                    </td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/b/index.html">b</a>
                    </td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/c/index.html">c</a>
                    </td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/d/index.html">d</a>
                    </td>
                    <td>1</td>
                </tr>
            </table>
        </body>
    </html>

    """


Scenario: detect cycle with sub-package
  Given I have the input file:
    """
    <project name="">
        <package name="">
            <package name="a">
                <class name="ClassA" id="1">
                    <reference target="2"/>
                </class>
                <package name="b">
                    <class name="ClassB" id="2">
                        <reference target="3"/>
                    </class>
                    <package name="c">
                        <class name="ClassC" id="3">
                            <reference target="1"/>
                        </class>
                    </package>
                </package>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "packages-with-cyclic-dependencies.html"
  And I process the queue until empty

  Then I expect to get the output file "packages-with-cyclic-dependencies.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Packages with cyclic dependencies</title>
        </head>
        <body>
            <h1>Packages with cyclic dependencies</h1>
            <table>
                <tr>
                    <th>metric</th>
                    <th>value</th>
                </tr>
                <tr>
                    <td>Number of cycles</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>Number of involved dependencies</td>
                    <td>3</td>
                </tr>
                <tr>
                    <td>Number of involved packages</td>
                    <td>3</td>
                </tr>
            </table>
            <h2>Involved dependencies</h2>
            <table>
                <tr>
                    <th>from</th>
                    <th>to</th>
                    <th>in cycles</th>
                    <th>stability</th>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~a,-~a~b.svg">a</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~a,-~a~b.svg">a::b</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~a~b,-~a~b~c.svg">a::b</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~a~b,-~a~b~c.svg">a::b::c</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~a~b~c,-~a.svg">a::b::c</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~a~b~c,-~a.svg">a</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
            </table>
            <h2>Involved packages</h2>
            <table>
                <tr>
                    <th>Package</th>
                    <th>in cycles</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/a/index.html">a</a>
                    </td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/a/b/index.html">a::b</a>
                    </td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/a/b/c/index.html">a::b::c</a>
                    </td>
                    <td>1</td>
                </tr>
            </table>
        </body>
    </html>

    """


Scenario: print info in how many cycles a node and edge is
  Given I have the input file:
    """
    <project name="">
        <package name="">
            <package name="a">
                <class name="ClassA" id="a">
                    <reference target="b"/>
                    <reference target="c"/>
                </class>
            </package>
            <package name="b">
                <class name="ClassB" id="b">
                    <reference target="c"/>
                    <reference target="d"/>
                </class>
            </package>
            <package name="c">
                <class name="ClassC" id="c">
                    <reference target="d"/>
                </class>
            </package>
            <package name="d">
                <class name="ClassD" id="d">
                    <reference target="a"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "packages-with-cyclic-dependencies.html"
  And I process the queue until empty

  Then I expect to get the output file "packages-with-cyclic-dependencies.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Packages with cyclic dependencies</title>
        </head>
        <body>
            <h1>Packages with cyclic dependencies</h1>
            <table>
                <tr>
                    <th>metric</th>
                    <th>value</th>
                </tr>
                <tr>
                    <td>Number of cycles</td>
                    <td>3</td>
                </tr>
                <tr>
                    <td>Number of involved dependencies</td>
                    <td>6</td>
                </tr>
                <tr>
                    <td>Number of involved packages</td>
                    <td>4</td>
                </tr>
            </table>
            <h2>Involved dependencies</h2>
            <table>
                <tr>
                    <th>from</th>
                    <th>to</th>
                    <th>in cycles</th>
                    <th>stability</th>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~a,-~b.svg">a</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~a,-~b.svg">b</a>
                    </td>
                    <td>2</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~a,-~c.svg">a</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~a,-~c.svg">c</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~b,-~c.svg">b</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~b,-~c.svg">c</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~b,-~d.svg">b</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~b,-~d.svg">d</a>
                    </td>
                    <td>1</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~c,-~d.svg">c</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~c,-~d.svg">d</a>
                    </td>
                    <td>2</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~d,-~a.svg">d</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~d,-~a.svg">a</a>
                    </td>
                    <td>3</td>
                    <td>0.000</td>
                </tr>
            </table>
            <h2>Involved packages</h2>
            <table>
                <tr>
                    <th>Package</th>
                    <th>in cycles</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/a/index.html">a</a>
                    </td>
                    <td>3</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/b/index.html">b</a>
                    </td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/c/index.html">c</a>
                    </td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/d/index.html">d</a>
                    </td>
                    <td>3</td>
                </tr>
            </table>
        </body>
    </html>

    """
