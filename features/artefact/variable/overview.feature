# language: en

Feature: generate an variable overview
  As a user of the tool
  I want to easily see all variables of my project
  In order to get used to the tool and see all possible information


Scenario: print a simple variable overview
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <variable name="varA"/>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/varA/index.html"

  Then I expect to get the output file "-/varA/index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>varA</title>
        </head>
        <body>
            <h1>varA</h1>
            <p>
                <ul>
                    <li>
                        <a href="neighbors.svg">Variable neighbors</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """
