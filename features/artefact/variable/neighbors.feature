# language: en

Feature: draw variable neighbors
  As an architect
  I want to see the neighbors of a variable
  In order to understand the role of the variable and the dependencies


Scenario: draw only the neighbors of the variable
  Given I have the input file:
    """
    <project>
        <package name="">
            <variable name="varA">
                <reference target="b"/>
                <reference target="d"/>
            </variable>
            <variable name="varB" id="b"/>
            <variable name="varC" id="c"/>
            <variable name="varD" id="d">
                <reference target="c"/>
            </variable>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/varA/neighbors.gv"

  Then I expect to get the output file "-/varA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="varA"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                label="varB"
                URL="../varB/index.html"
            ]
            _node2 [
                shape="box"
                label="varD"
                URL="../varD/index.html"
            ]
        }
        _node0 -> _node1
        _node0 -> _node2
    }

    """


Scenario: draw class neighbors
  Given I have the input file:
    """
    <project>
        <package name="">
            <variable name="varA" id="1">
                <reference target="2"/>
            </variable>
            <class name="ClassB">
                <method name="m1" id="2"/>
            </class>
            <class name="ClassC">
                <method name="m1">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/varA/neighbors.gv"

  Then I expect to get the output file "-/varA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="varA"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                label="ClassB"
                URL="../ClassB/index.html"
            ]
            _node2 [
                shape="box"
                label="ClassC"
                URL="../ClassC/index.html"
            ]
        }
        _node0 -> _node1
        _node2 -> _node0
    }

    """
