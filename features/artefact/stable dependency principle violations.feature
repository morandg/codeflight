# language: en

Feature: Dependencies which violate the stable dendency principle
  As an architect
  I want easily see if and which dependencies violate the stable dendency principle
  In order to move some classes or fix the problem otherwise


Background:
  Given I enable all metrics


Scenario: print a list of dependencies and packages that violate the SDP
  Given I have the input file:
    """
    <project name="">
        <package name="">
            <package name="instable1">
                <class name="A">
                    <reference target="stable"/>
                </class>
            </package>
            <package name="instable2">
                <class name="A">
                    <reference target="stable"/>
                </class>
            </package>
            <package name="stable">
                <class name="A" id="stable">
                    <reference target="flexible"/>
                </class>
                <class name="B">
                    <reference target="flexible"/>
                </class>
            </package>
            <package name="flexible">
                <class name="A" id="flexible">
                    <reference target="x"/>
                </class>
                <class name="B" id="flexible">
                    <reference target="x"/>
                </class>
                <class name="C" id="flexible">
                    <reference target="x"/>
                </class>
            </package>
            <package name="x">
                <class name="A" id="x"/>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "stable-dependency-principle-violations.html"
  And I process the queue until empty

  Then I expect to get the output file "stable-dependency-principle-violations.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Stable dependency principle violations</title>
        </head>
        <body>
            <h1>Stable dependency principle violations</h1>
            <table>
                <tr>
                    <th>metric</th>
                    <th>value</th>
                </tr>
                <tr>
                    <td>Number of violations</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>Number of dependencies</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td>Violation ratio</td>
                    <td>0.250</td>
                </tr>
                <tr>
                    <td>Number of involved packages</td>
                    <td>2</td>
                </tr>
            </table>
            <h2>Dependencies</h2>
            <table>
                <tr>
                    <th>from</th>
                    <th>to</th>
                    <th>stability</th>
                </tr>
                <tr>
                    <td>
                        <a href="dependencies-between-packages/-~stable,-~flexible.svg">stable</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~stable,-~flexible.svg">flexible</a>
                    </td>
                    <td>-0.100</td>
                </tr>
            </table>
            <h2>Packages</h2>
            <table>
                <tr>
                    <th>Package</th>
                    <th>in violations</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/stable/index.html">stable</a>
                    </td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/flexible/index.html">flexible</a>
                    </td>
                    <td>1</td>
                </tr>
            </table>
        </body>
    </html>

    """


Scenario: reduce output when no cycles are found
  Given I have the input file:
    """
    <project name="">
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <reference target="2"/>
                </class>
            </package>
            <package name="b">
                <class name="ClassB" id="2"/>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "stable-dependency-principle-violations.html"
  And I process the queue until empty

  Then I expect to get the output file "stable-dependency-principle-violations.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Stable dependency principle violations</title>
        </head>
        <body>
            <h1>Stable dependency principle violations</h1>
            <table>
                <tr>
                    <th>metric</th>
                    <th>value</th>
                </tr>
                <tr>
                    <td>Number of violations</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Number of dependencies</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>Violation ratio</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>Number of involved packages</td>
                    <td>0</td>
                </tr>
            </table>
        </body>
    </html>

    """


Scenario: handle empty project
  Given I have the input file:
    """
    <project name=""/>
    """

  When I parse the input
  And I generate the file "stable-dependency-principle-violations.html"
  And I process the queue until empty

  Then I expect to get the output file "stable-dependency-principle-violations.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Stable dependency principle violations</title>
        </head>
        <body>
            <h1>Stable dependency principle violations</h1>
            <table>
                <tr>
                    <th>metric</th>
                    <th>value</th>
                </tr>
                <tr>
                    <td>Number of violations</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Number of dependencies</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Violation ratio</td>
                    <td>0.000</td>
                </tr>
                <tr>
                    <td>Number of involved packages</td>
                    <td>0</td>
                </tr>
            </table>
        </body>
    </html>

    """
