# language: en

Feature: generate an function overview
  As a user of the tool
  I want to easily see all functions of my project
  In order to get used to the tool and see all possible information


Scenario: print a simple functions overview
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <function name="funcA"/>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/funcA/index.html"

  Then I expect to get the output file "-/funcA/index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>funcA</title>
        </head>
        <body>
            <h1>funcA</h1>
            <p>
                <ul>
                    <li>
                        <a href="neighbors.svg">Function neighbors</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """
