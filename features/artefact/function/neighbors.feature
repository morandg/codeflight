# language: en

Feature: draw function neighbors
  As an architect
  I want to see the neighbors of a function
  In order to understand the role of the function and the dependencies


Scenario: draw only the neighbors of the function
  Given I have the input file:
    """
    <project>
        <package name="">
            <function name="funcA">
                <reference target="b"/>
                <reference target="d"/>
            </function>
            <function name="funcB" id="b"/>
            <function name="funcC" id="c"/>
            <function name="funcD" id="d">
                <reference target="c"/>
            </function>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/funcA/neighbors.gv"

  Then I expect to get the output file "-/funcA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="funcA"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                label="funcB"
                URL="../funcB/index.html"
            ]
            _node2 [
                shape="box"
                label="funcD"
                URL="../funcD/index.html"
            ]
        }
        _node0 -> _node1
        _node0 -> _node2
    }

    """


Scenario: draw dependencies to other packages
  Given I have the input file:
    """
    <project>
        <package name="">
            <function name="funcA" id="a">
                <reference target="b"/>
                <reference target="c"/>
            </function>
            <package name="p1">
                <function name="funcB" id="b"/>
                <package name="p2">
                    <function name="funcC" id="c"/>
                </package>
                <function name="funcD" id="d">
                    <reference target="a"/>
                </function>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/funcA/neighbors.gv"

  Then I expect to get the output file "-/funcA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="funcA"
                URL="index.html"
            ]
        }
        subgraph cluster_1 {
            label="p1"
            URL="../p1/index.html"
            _node1 [
                shape="box"
                label="funcB"
                URL="../p1/funcB/index.html"
            ]
            _node2 [
                shape="box"
                label="funcD"
                URL="../p1/funcD/index.html"
            ]
        }
        subgraph cluster_2 {
            label="p1::p2"
            URL="../p1/p2/index.html"
            _node3 [
                shape="box"
                label="funcC"
                URL="../p1/p2/funcC/index.html"
            ]
        }
        _node0 -> _node1 [
            URL="../../dependencies-between-packages/-,-~p1.svg"
        ]
        _node0 -> _node3 [
            URL="../../dependencies-between-packages/-,-~p1~p2.svg"
        ]
        _node2 -> _node0 [
            URL="../../dependencies-between-packages/-~p1,-.svg"
        ]
    }

    """


Scenario: don't draw duplicated connections
  Given I have the input file:
    """
    <project>
        <package name="">
            <function name="funcA">
                <reference target="b"/>
                <reference target="b"/>
            </function>
            <function name="funcB" id="b"/>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/funcA/neighbors.gv"

  Then I expect to get the output file "-/funcA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="funcA"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                label="funcB"
                URL="../funcB/index.html"
            ]
        }
        _node0 -> _node1
    }

    """


Scenario: don't draw connection to itself
  Given I have the input file:
    """
    <project>
        <package name="">
            <function name="funcA" id="2">
                <reference target="2"/>
            </function>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/funcA/neighbors.gv"

  Then I expect to get the output file "-/funcA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="funcA"
                URL="index.html"
            ]
        }
    }

    """


Scenario: draw cycles
  Given I have the input file:
    """
    <project>
        <package name="">
            <function name="funcA" id="a">
                <reference target="b"/>
            </function>
            <function name="funcB" id="b">
                <reference target="a"/>
            </function>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/funcA/neighbors.gv"

  Then I expect to get the output file "-/funcA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="funcA"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                label="funcB"
                URL="../funcB/index.html"
            ]
        }
        _node0 -> _node1
        _node1 -> _node0
    }

    """


Scenario: draw class neighbors
  Given I have the input file:
    """
    <project>
        <package name="">
            <function name="funcA" id="1">
                <reference target="2"/>
            </function>
            <class name="ClassB">
                <method name="m1" id="2"/>
            </class>
            <class name="ClassC">
                <method name="m1">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/funcA/neighbors.gv"

  Then I expect to get the output file "-/funcA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="funcA"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                label="ClassB"
                URL="../ClassB/index.html"
            ]
            _node2 [
                shape="box"
                label="ClassC"
                URL="../ClassC/index.html"
            ]
        }
        _node0 -> _node1
        _node2 -> _node0
    }

    """


Scenario: draw variable neighbors
  Given I have the input file:
    """
    <project>
        <package name="">
            <function name="funcA">
                <reference target="1"/>
            </function>
            <variable name="varB" id="1"/>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/funcA/neighbors.gv"

  Then I expect to get the output file "-/funcA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="funcA"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                label="varB"
                URL="../varB/index.html"
            ]
        }
        _node0 -> _node1
    }

    """
