# language: en

Feature: draw the dependencies of one package
  As an architect
  I want to know the dependencies within one package
  In order to see missing or to strong connections


Scenario: reduce method and field dependencies to class dependencies
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="Method1">
                    <reference target="123"/>
                </method>
                <method name="Method2">
                    <reference target="2"/>
                </method>
            </class>
            <class name="ClassB">
                <method name="Method1" id="123"/>
                <field name="Field1" id="2"/>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/package-content-dependencies.gv"

  Then I expect to get the output file "-/package-content-dependencies.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            label="ClassA"
            URL="ClassA/index.html"
        ]
        _node1 [
            shape="box"
            label="ClassB"
            URL="ClassB/index.html"
        ]
        _node0 -> _node1
    }

    """


Scenario: reduce duplicated dependencies
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <method name="AM1" id="2"/>
            </class>
            <class name="B">
                <method name="BM1">
                    <reference target="1"/>
                </method>
                <method name="BM2">
                    <reference target="4"/>
                    <reference target="2"/>
                </method>
                <method name="BM3">
                    <reference target="4"/>
                </method>
                <field name="BF1" id="3">
                    <reference target="1"/>
                </field>
                <field name="BF2" id="4"/>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/package-content-dependencies.gv"

  Then I expect to get the output file "-/package-content-dependencies.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            label="A"
            URL="A/index.html"
        ]
        _node1 [
            shape="box"
            label="B"
            URL="B/index.html"
        ]
        _node1 -> _node0
    }

    """


Scenario: remove dependencies to itself
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="Method1">
                    <reference target="m2"/>
                </method>
                <method name="Method2" id="m2"/>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/package-content-dependencies.gv"

  Then I expect to get the output file "-/package-content-dependencies.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            label="ClassA"
            URL="ClassA/index.html"
        ]
    }

    """


Scenario: reduce encapsulated class dependencies
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <class name="ClassA_A" id="1"/>
            </class>
            <class name="ClassB">
                <class name="ClassB_B">
                    <reference target="1"/>
                </class>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/package-content-dependencies.gv"

  Then I expect to get the output file "-/package-content-dependencies.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            label="ClassA"
            URL="ClassA/index.html"
        ]
        _node1 [
            shape="box"
            label="ClassB"
            URL="ClassB/index.html"
        ]
        _node1 -> _node0
    }

    """


Scenario: use correct links in sub package
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="p1">
                <package name="p2">
                    <class name="ClassA"/>
                </package>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/p1/p2/package-content-dependencies.gv"

  Then I expect to get the output file "-/p1/p2/package-content-dependencies.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            label="ClassA"
            URL="ClassA/index.html"
        ]
    }

    """


Scenario: add link to class
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA"/>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/package-content-dependencies.gv"

  Then I expect to get the output file "-/package-content-dependencies.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            label="ClassA"
            URL="ClassA/index.html"
        ]
    }

    """


Scenario: add link to function
  Given I have the input file:
    """
    <project>
        <package name="">
            <function name="funcA"/>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/package-content-dependencies.gv"

  Then I expect to get the output file "-/package-content-dependencies.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            label="funcA"
            URL="funcA/index.html"
        ]
    }

    """


Scenario: add link to variable
  Given I have the input file:
    """
    <project>
        <package name="">
            <variable name="varA"/>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/package-content-dependencies.gv"

  Then I expect to get the output file "-/package-content-dependencies.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            label="varA"
            URL="varA/index.html"
        ]
    }

    """
