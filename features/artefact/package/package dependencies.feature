# language: en

Feature: draw dependencies between packages
  As an architect
  I want to see the dependencies between the packages
  In order see the structure of the software


Scenario: draw a simple dependency
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="p1">
                <class name="ClassA">
                    <method name="Method1">
                        <reference target="123"/>
                    </method>
                </class>
            </package>
            <package name="p2">
                <package name="p3">
                    <class name="ClassB">
                        <method name="Method2" id="123"/>
                    </class>
                </package>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/package-dependencies.gv"

  Then I expect to get the output file "-/package-dependencies.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            label="::"
            URL="index.html"
        ]
        _node1 [
            shape="box"
            label="p1"
            URL="p1/index.html"
        ]
        _node2 [
            shape="box"
            label="p2"
            URL="p2/index.html"
        ]
        _node3 [
            shape="box"
            label="p2::p3"
            URL="p2/p3/index.html"
        ]
        _node1 -> _node3 [
            URL="../dependencies-between-packages/-~p1,-~p2~p3.svg"
        ]
    }

    """


Scenario: does not print dependency to itself
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="p1">
                <class name="ClassA">
                    <method name="Method1">
                        <reference target="123"/>
                    </method>
                    <method name="Method2" id="123"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/package-dependencies.gv"

  Then I expect to get the output file "-/package-dependencies.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            label="::"
            URL="index.html"
        ]
        _node1 [
            shape="box"
            label="p1"
            URL="p1/index.html"
        ]
    }

    """


Scenario: draw a simple dependency from main to a function in a different namespace
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="foo">
                <function name="bar" id="11"/>
            </package>
            <function name="main">
                <reference target="11"/>
            </function>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/package-dependencies.gv"

  Then I expect to get the output file "-/package-dependencies.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            label="::"
            URL="index.html"
        ]
        _node1 [
            shape="box"
            label="foo"
            URL="foo/index.html"
        ]
        _node0 -> _node1 [
            URL="../dependencies-between-packages/-,-~foo.svg"
        ]
    }

    """


Scenario: include variables in dependency calculation
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="foo">
                <variable name="bar" id="11"/>
            </package>
            <variable name="doo">
                <reference target="11"/>
            </variable>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/package-dependencies.gv"

  Then I expect to get the output file "-/package-dependencies.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            label="::"
            URL="index.html"
        ]
        _node1 [
            shape="box"
            label="foo"
            URL="foo/index.html"
        ]
        _node0 -> _node1 [
            URL="../dependencies-between-packages/-,-~foo.svg"
        ]
    }

    """


Scenario: draw package dependencies for child packages
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="p1">
                <package name="p1_1">
                    <class name="A" id="1"/>
                </package>
                <package name="p1_2">
                    <package name="p1_2_1">
                        <class name="B">
                            <reference target="2"/>
                            <reference target="4"/>
                        </class>
                    </package>
                </package>
                <package name="p1_3">
                    <class name="C" id="2">
                        <reference target="1"/>
                        <reference target="3"/>
                    </class>
                </package>
                <class name="D" id="3"/>
            </package>
            <package name="p2">
                <class name="E" id="4"/>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/p1/package-dependencies.gv"

  Then I expect to get the output file "-/p1/package-dependencies.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            label="p1"
            URL="index.html"
        ]
        _node1 [
            shape="box"
            label="p1::p1_1"
            URL="p1_1/index.html"
        ]
        _node2 [
            shape="box"
            label="p1::p1_2"
            URL="p1_2/index.html"
        ]
        _node3 [
            shape="box"
            label="p1::p1_2::p1_2_1"
            URL="p1_2/p1_2_1/index.html"
        ]
        _node4 [
            shape="box"
            label="p1::p1_3"
            URL="p1_3/index.html"
        ]
        _node3 -> _node4 [
            URL="../../dependencies-between-packages/-~p1~p1_2~p1_2_1,-~p1~p1_3.svg"
        ]
        _node4 -> _node1 [
            URL="../../dependencies-between-packages/-~p1~p1_3,-~p1~p1_1.svg"
        ]
        _node4 -> _node0 [
            URL="../../dependencies-between-packages/-~p1~p1_3,-~p1.svg"
        ]
    }

    """
