# language: en

Feature: draw package neighbors
  As an architect
  I want to see the neighbors of a package
  In order to see all dependencies


Scenario: draw the neighbors of the package
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="Class">
                    <method name="m">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
            <package name="b">
                <class name="Class">
                    <method name="m" id="1">
                        <reference target="2"/>
                    </method>
                </class>
            </package>
            <package name="c">
                <class name="Class" id="2"/>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/b/neighbors.gv"

  Then I expect to get the output file "-/b/neighbors.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            style="bold"
            label="b"
            URL="index.html"
        ]
        _node1 [
            shape="box"
            label="c"
            URL="../c/index.html"
        ]
        _node2 [
            shape="box"
            label="a"
            URL="../a/index.html"
        ]
        _node0 -> _node1 [
            label="1"
            URL="../../dependencies-between-packages/-~b,-~c.svg"
        ]
        _node2 -> _node0 [
            label="1"
            URL="../../dependencies-between-packages/-~a,-~b.svg"
        ]
    }

    """


Scenario: draw only the neighbors of the package
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <method name="m1">
                        <reference target="b"/>
                    </method>
                    <method name="m2">
                        <reference target="d"/>
                    </method>
                </class>
            </package>
            <package name="b">
                <class name="ClassB" id="b"/>
            </package>
            <package name="c">
                <class name="ClassC" id="c"/>
            </package>
            <package name="d">
                <class name="ClassD" id="d">
                    <method name="m3">
                        <reference target="c"/>
                    </method>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/a/neighbors.gv"

  Then I expect to get the output file "-/a/neighbors.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            style="bold"
            label="a"
            URL="index.html"
        ]
        _node1 [
            shape="box"
            label="b"
            URL="../b/index.html"
        ]
        _node2 [
            shape="box"
            label="d"
            URL="../d/index.html"
        ]
        _node0 -> _node1 [
            label="1"
            URL="../../dependencies-between-packages/-~a,-~b.svg"
        ]
        _node0 -> _node2 [
            label="1"
            URL="../../dependencies-between-packages/-~a,-~d.svg"
        ]
    }

    """


Scenario: don't draw dependencies between neighbors of interested package
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <method name="m1">
                        <reference target="b"/>
                    </method>
                    <method name="m2">
                        <reference target="c"/>
                    </method>
                </class>
            </package>
            <package name="b">
                <class name="ClassB" id="b"/>
            </package>
            <package name="c">
                <class name="ClassC" id="c">
                    <method name="m3">
                        <reference target="b"/>
                    </method>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/a/neighbors.gv"

  Then I expect to get the output file "-/a/neighbors.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            style="bold"
            label="a"
            URL="index.html"
        ]
        _node1 [
            shape="box"
            label="b"
            URL="../b/index.html"
        ]
        _node2 [
            shape="box"
            label="c"
            URL="../c/index.html"
        ]
        _node0 -> _node1 [
            label="1"
            URL="../../dependencies-between-packages/-~a,-~b.svg"
        ]
        _node0 -> _node2 [
            label="1"
            URL="../../dependencies-between-packages/-~a,-~c.svg"
        ]
    }

    """


Scenario: draw the encapsulated package neighbor of the package
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA" id="1">
                <class name="ClassB">
                    <reference target="1"/>
                    <reference target="2"/>
                </class>
            </class>
            <package name="c">
                <class name="ClassC" id="2"/>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/neighbors.gv"

  Then I expect to get the output file "-/neighbors.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            style="bold"
            label="::"
            URL="index.html"
        ]
        _node1 [
            shape="box"
            label="c"
            URL="c/index.html"
        ]
        _node0 -> _node1 [
            label="1"
            URL="../dependencies-between-packages/-,-~c.svg"
        ]
    }

    """


Scenario: label duplicated connections
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <method name="m1">
                        <reference target="1"/>
                    </method>
                </class>
                <class name="ClassB">
                    <method name="m1">
                        <reference target="2"/>
                    </method>
                </class>
            </package>
            <package name="b">
                <class name="ClassA" id="1"/>
                <class name="ClassB" id="2"/>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/a/neighbors.gv"

  Then I expect to get the output file "-/a/neighbors.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            style="bold"
            label="a"
            URL="index.html"
        ]
        _node1 [
            shape="box"
            label="b"
            URL="../b/index.html"
        ]
        _node0 -> _node1 [
            label="2"
            URL="../../dependencies-between-packages/-~a,-~b.svg"
        ]
    }

    """


Scenario: don't draw connection to itself
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <method name="m1">
                        <reference target="2"/>
                    </method>
                </class>
                <class name="ClassB">
                    <method name="m2" id="2"/>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/a/neighbors.gv"

  Then I expect to get the output file "-/a/neighbors.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            style="bold"
            label="a"
            URL="index.html"
        ]
    }

    """


Scenario: draw cycle
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="ClassA">
                    <method name="m1" id="1">
                        <reference target="2"/>
                    </method>
                </class>
            </package>
            <package name="b">
                <class name="ClassB">
                    <method name="m2" id="2">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/a/neighbors.gv"

  Then I expect to get the output file "-/a/neighbors.gv":
    """
    digraph {
        rankdir=LR
        _node0 [
            shape="box"
            style="bold"
            label="a"
            URL="index.html"
        ]
        _node1 [
            shape="box"
            label="b"
            URL="../b/index.html"
        ]
        _node0 -> _node1 [
            label="1"
            URL="../../dependencies-between-packages/-~a,-~b.svg"
        ]
        _node1 -> _node0 [
            label="1"
            URL="../../dependencies-between-packages/-~b,-~a.svg"
        ]
    }

    """
