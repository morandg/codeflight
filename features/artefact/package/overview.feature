# language: en

Feature: generate an package overview
  As a user of the tool
  I want to easily see the most important information about a package
  In order to get used to the tool and see all possible information


Scenario: print a simple package overview
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <class name="ClassA"/>
            <class name="ClassB"/>
            <package name="package1">
                <class name="ClassC"/>
                <package name="package2">
                    <class name="ClassD"/>
                    <class name="ClassE"/>
                </package>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/package1/package2/index.html"

  Then I expect to get the output file "-/package1/package2/index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>package1::package2</title>
        </head>
        <body>
            <h1>package1::package2</h1>
            <p>
                <ul>
                    <li>
                        <a href="package-content-dependencies.svg">Package content dependencies</a>
                    </li>
                    <li>
                        <a href="package-dependencies.svg">Package dependencies</a>
                    </li>
                    <li>
                        <a href="neighbors.svg">Package neighbors</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """
