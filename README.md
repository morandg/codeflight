# CodeFlight

CodeFlight is a free and open source tool to gain insights about the high level structure of your software.
It supports various metrics, graphs and statistics to find problematic areas in your code base and keep track of them.

See the [examples](http://ursfassler.gitlab.io/codeflight-examples/index.html) to get an impression.

## Use Cases

CodeFlight supports the software architect and developer in various ways.
For once, it provides metrics and graphs to identify problematic areas, thus guiding the development team in the cleanup / refactoring work.
Integrated in the continuous integration pipeline it is a early warning system for code rot.
If executed with the testing suite it can alarm about a policy violation, e.g. introduction of cyclic dependencies between packages.


## Output

CodeFlight generates the metrics, graphs and more as html.
This allows you to browse it right away or include it in your CI or any other system.

## Input 

CodeFlight takes a simplified AST of the project as input.
The AST consist of just a few different nodes an references between the nodes.

    <project>
        <package name="">
            <class name="ClassA" id="1">
                <method name="method1" id="2">
                    <reference target="3"/>
                </method>
                <field name="field1">
            </class>
            <function name="foo">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

For C++, the simplified AST can be generated with [cpp-ast](https://gitlab.com/ursfassler/cpp-ast).


## Usage

CodeFlight is an easy to use command line tool.
You provide the simplified AST and the output is generated.

