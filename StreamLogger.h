/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "logger/Logger.h"
#include <iostream>


class StreamLogger :
    public logger::Logger
{
  public:
    StreamLogger(std::ostream& standard_, std::ostream& error_);

    void info(const std::string& message) const override;
    void warning(const std::string& message) const override;

  private:
    std::ostream& standard;
    std::ostream& error;

};
