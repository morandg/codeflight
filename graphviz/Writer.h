/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "NumberIterator.h"
#include <string>
#include <map>


namespace graphviz
{

class StreamWriter;


enum class Shape
{
  Box,
  Circle,
};

enum class Style
{
  Dashed,
  Bold,
};


class Writer
{
public:
  Writer(StreamWriter& writer_);

  void tableRow(const std::string& name);
  void tableRow(const std::string& name, const void*);
  void beginNode(Shape, const void*);
  void endNode();
  void node(const std::string& caption, Shape, const void*);
  void beginLabelTable();
  void endLabelTable();
  void label(const std::string&);
  void url(const std::string&);
  void beginSubgraph();
  void endSubgraph();
  void beginGraph();
  void endGraph();
  void edge(const void* source, const void* destination);
  void beginEdge(const void* source, const void* destination);
  void endEdge();
  void weight(unsigned);
  void style(Style);


private:
  StreamWriter& writer;

  NumberIterator subgraphNr{};
  NumberIterator nodeNr{};
  NumberIterator portNr{};
  std::map<const void*, std::string> nodeName{};
  std::map<const void*, std::string> portName{};

  std::string toString(const void*) const;
  std::string currentNodeName() const;
  std::string currentPortName() const;
  std::string currentSubraphName() const;
};


}
