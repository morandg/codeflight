/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Writer.h"
#include "StreamWriter.h"
#include <map>
#include <functional>


namespace graphviz
{
namespace
{


const std::map<char, std::string> EscapeMap
{
  {'"', "&quot;"},
  {'\'', "&apos;"},
  {'<', "&lt;"},
  {'>', "&gt;"},
  {'&', "&amp;"},
};

std::string escape(const std::string& value)
{
  std::string result;

  for (const auto& sym : value) {
    const auto idx = EscapeMap.find(sym);
    if (idx == EscapeMap.end()) {
      result += sym;
    } else {
      result += idx->second;
    }
  }

  return result;
}

const std::map<Shape, std::string> ShapeStrings
{
  {Shape::Box, "box"},
  {Shape::Circle, "circle"},
};

const std::map<Style, std::string> StyleStrings
{
  {Style::Dashed, "dashed"},
  {Style::Bold, "bold"},
};

template<typename T>
std::string to_string(T value, const std::map<T, std::string>& map)
{
  const auto idx = map.find(value);
  if (idx == map.end()) {
    throw std::runtime_error("string for value not found: " + std::to_string(static_cast<int>(value)));
  }
  return idx->second;
}

std::string to_string(Shape value)
{
  return to_string(value, ShapeStrings);
}

std::string to_string(Style value)
{
  return to_string(value, StyleStrings);
}


}


Writer::Writer(StreamWriter &writer_) :
  writer{writer_}
{
}

void Writer::beginGraph()
{
  nodeName.clear();
  portName.clear();
  subgraphNr.reset();
  nodeNr.reset();

  writer.write("digraph {");
  writer.inc();
  writer.write("rankdir=LR");
}

void Writer::endGraph()
{
  writer.dec();
  writer.write("}");
}

void Writer::beginNode(Shape shape, const void* id)
{
  nodeNr.next();
  portNr.reset();

  const auto name = currentNodeName();
  nodeName[id] = name;

  writer.write(name + " [");
  writer.inc();
  writer.write("shape=\"" + to_string(shape) + "\"");
}

void Writer::endNode()
{
  writer.dec();
  writer.write("]");
}

void Writer::node(const std::string& caption, Shape shape, const void* id)
{
  beginNode(shape, id);
  label(caption);
  endNode();
}

void Writer::beginLabelTable()
{
  writer.write(R"(label=<<table border="0" cellborder="1" cellspacing="0" cellpadding="2">)");
  writer.inc();
}

void Writer::endLabelTable()
{
  writer.dec();
  writer.write("</table>>");
}

void Writer::tableRow(const std::string &name)
{
  writer.write("<tr><td>" + escape(name) + "</td></tr>");
}

void Writer::tableRow(const std::string& name, const void* id)
{
  portNr.next();
  const auto portId = currentPortName();
  nodeName[id] = currentNodeName();
  portName[id] = portId;

  writer.write("<tr><td port=\"" + portId + "\">" + escape(name) + "</td></tr>");
}

void Writer::label(const std::string& caption)
{
  writer.write("label=\"" +  escape(caption) + "\"");
}

void Writer::url(const std::string& value)
{
  writer.write("URL=\"" +  escape(value) + "\"");
}

void Writer::beginSubgraph()
{
  subgraphNr.next();
  writer.write("subgraph " + currentSubraphName() + " {");
  writer.inc();
}

void Writer::endSubgraph()
{
  writer.dec();
  writer.write("}");
}

void Writer::edge(const void* source, const void* destination)
{
  writer.write(toString(source) + " -> " + toString(destination));
}

void Writer::beginEdge(const void *source, const void *destination)
{
  writer.write(toString(source) + " -> " + toString(destination) + " [");
  writer.inc();
}

void Writer::endEdge()
{
  writer.dec();
  writer.write("]");
}

void Writer::weight(unsigned value)
{
  writer.write("weight=\"" + std::to_string(value) + "\"");
}

void Writer::style(Style value)
{
  writer.write("style=\"" + to_string(value) + "\"");
}

std::string Writer::toString(const void* id) const
{
  std::string result{};

  const auto nodeIdx = nodeName.find(id);
  if (nodeIdx != nodeName.end()) {
    result = nodeIdx->second;
  } else {
    throw std::invalid_argument("edge to/from undefined node");
  }

  const auto portIdx = portName.find(id);
  if (portIdx != portName.end()) {
    result += ":" + portIdx->second;
  }

  return result;
}

std::string Writer::currentNodeName() const
{
  return "_node" + std::to_string(nodeNr.current());
}

std::string Writer::currentPortName() const
{
  return "_port" + std::to_string(portNr.current());
}

std::string Writer::currentSubraphName() const
{
  return "cluster_" + std::to_string(subgraphNr.current());
}


}
