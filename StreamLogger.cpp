/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "StreamLogger.h"


StreamLogger::StreamLogger(std::ostream &standard_, std::ostream &error_) :
    standard{standard_},
    error{error_}
{
}

void StreamLogger::info(const std::string &message) const
{
    standard << message << std::endl;
}

void StreamLogger::warning(const std::string &message) const
{
    error << message << std::endl;
}
