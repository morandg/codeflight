/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "ArtefactFactory.h"
#include "ast/Function.h"
#include "ast/Class.h"
#include "ast/Package.h"
#include "artefact/generateGraphvizSvg.h"
#include "graphviz/StreamWriter.h"
#include "graphviz/Writer.h"
#include "artefact/Path.h"
#include "library/OrderedSet.h"
#include "ast/repository/Parent.h"
#include "ast/repository/Neighbor.h"
#include "ast/DefaultVisitor.h"
#include <optional>
#include <cassert>


namespace artefact::overviewfactory
{
namespace
{


class OverviewArtefactVisitor :
    public ast::DefaultVisitor
{
  public:
    OverviewArtefactVisitor(
        const ArtefactFactory& artefactFactory_
        ) :
      artefactFactory{artefactFactory_}
    {
    }

    void visit(const ast::Package& node) override
    {
      artefact = artefactFactory.package(&node);
    }

    void visit(const ast::Class& node) override
    {
      artefact = artefactFactory.clazz(&node);
    }

    void visit(const ast::Function& node) override
    {
      artefact = artefactFactory.function(&node);
    }

    void visit(const ast::Variable& node) override
    {
      artefact = artefactFactory.variable(&node);
    }

    std::optional<artefact::Artefact*> getArtefact() const
    {
      return artefact;
    }

  private:
    const ArtefactFactory& artefactFactory;
    std::optional<artefact::Artefact*> artefact{};
};


}


Factory::Factory(const ArtefactFactory& artefactFactory_) :
  artefactFactory{artefactFactory_}
{
}

std::optional<Artefact*> Factory::produce(const ast::Node* node) const
{
  OverviewArtefactVisitor visitor{artefactFactory};
  node->accept(visitor);
  return visitor.getArtefact();
}


}
