/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/Artefact.h"
#include <string>


namespace artefact::overviewfactory
{

class ArtefactFactory;

class Factory
{
  public:
    explicit Factory(const ArtefactFactory&);

    std::optional<artefact::Artefact*> produce(const ast::Node*) const;

  private:
    const ArtefactFactory& artefactFactory;
};


}
