/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace ast
{

class Node;
class Class;
class Function;
class Variable;

}

namespace artefact
{

class Artefact;

}

namespace artefact::overviewfactory
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual Artefact* package(const ast::Node*) const = 0;  //TODO use ast::Package
  virtual Artefact* clazz(const ast::Class*) const = 0;
  virtual Artefact* function(const ast::Function*) const = 0;
  virtual Artefact* variable(const ast::Variable*) const = 0;

};


}
