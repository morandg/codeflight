/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace ast
{

class Node;

}

namespace artefact
{

class Artefact;

}

namespace artefact::package_overview
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual Artefact* intraPackageDependencies(const ast::Node*) const = 0;
  virtual Artefact* childPackageDependencies(const ast::Node*) const = 0;
  virtual Artefact* packageNeighbors(const ast::Node*) const = 0;

};


}
