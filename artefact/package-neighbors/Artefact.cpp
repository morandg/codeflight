/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "ast/Package.h"
#include "artefact/generateGraphvizSvg.h"
#include "graphviz/StreamWriter.h"
#include "graphviz/Writer.h"
#include "ArtefactFactory.h"
#include "artefact/Path.h"
#include "ast/repository/Neighbor.h"
#include <cassert>


namespace artefact::package_neighbors
{


Artefact::Artefact(
    const ast::Node* package_,
    const ArtefactFactory& artefactFactory_,
    const ast::repository::Neighbor& neighbors_
    ) :
  package{package_},
  artefactFactory{artefactFactory_},
  neighbors{neighbors_}
{
}

void Artefact::generate(const Environment& env) const
{
  const auto output = print(env);
  generateGraphvizSvg(filename(env.pathFor), output, env);
}


std::string Artefact::print(const Environment& env) const
{
  std::stringstream output{};
  graphviz::StreamWriter writer{output};
  graphviz::Writer gvw{writer};

  gvw.beginGraph();
  drawNodes(gvw, env);
  drawEdges(gvw, env);
  gvw.endGraph();

  return output.str();
}

void Artefact::drawNodes(graphviz::Writer& gvw, const Environment& env) const
{
  library::Set<const ast::Node*> drawn{};
  const auto drawer = [&drawn, &gvw, env, this](const ast::Node* node){
    drawNode(node, drawn, gvw, env);
  };

  drawer(package);
  neighbors.foreachOutgoingPackage(package, drawer);
  neighbors.foreachIncomingPackage(package, drawer);
}

void Artefact::drawEdges(graphviz::Writer& gvw, const Environment& env) const
{
  EdgeCount count{};
  const auto edgeCounter = [&count](const ast::Node* source, const ast::Node* destination) {
    count[source][destination]++;
  };

  neighbors.foreachOutgoingPackage(package, std::bind(edgeCounter, package, std::placeholders::_1));
  neighbors.foreachIncomingPackage(package, std::bind(edgeCounter, std::placeholders::_1, package));

  Edges drawn{};
  const auto drawer = [&drawn, &gvw, &count, &env, this](const ast::Node* source, const ast::Node* destination){
    drawEdge(source, destination, drawn, gvw, count, env);
  };

  neighbors.foreachOutgoingPackage(package, std::bind(drawer, package, std::placeholders::_1));
  neighbors.foreachIncomingPackage(package, std::bind(drawer, std::placeholders::_1, package));
}

void Artefact::drawNode(const ast::Node* node, library::Set<const ast::Node*>& drawn, graphviz::Writer& gvw, const Environment& env) const
{
  if (drawn.contains(node)) {
    return;
  }
  drawn.add(node);

  gvw.beginNode(graphviz::Shape::Box, node);
  if (node == package) {
    gvw.style(graphviz::Style::Bold);
  }
  gvw.label(env.pathFor(*node).display());

  const auto package = dynamic_cast<const ast::Package*>(node); //TODO remove cast
  assert(package);

  const auto artefact = artefactFactory.package(package);
  env.queue(artefact);
  const auto rootUrl = filename(env.pathFor);
  const auto url = artefact->filename(env.pathFor).relativeTo(rootUrl);
  gvw.url(url.escaped());

  gvw.endNode();
}

void Artefact::drawEdge(const ast::Node* source, const ast::Node* destination, Artefact::Edges& drawn, graphviz::Writer& gvw, EdgeCount& count, const Environment& env) const
{
  if (drawn[source].contains(destination)) {
    return;
  }
  drawn[source].add(destination);

  if (source != destination) {
    const auto artefact = artefactFactory.packageDependencies({source, destination});
    env.queue(artefact);

    const auto rootUrl = filename(env.pathFor);
    const auto url = artefact->filename(env.pathFor).relativeTo(rootUrl);

    gvw.beginEdge(source, destination);
    gvw.label(std::to_string(count[source][destination]));
    gvw.url(url.escaped());
    gvw.endEdge();
  }
}

html::Url Artefact::filename(const PathForNode& pathFor) const
{
  return html::Url{pathFor(*package).raw(), ".svg"}.append("neighbors");
}


}
