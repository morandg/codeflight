/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/Artefact.h"
#include "library/Set.h"
#include <string>
#include <map>


namespace ast
{

class Project;
class Class;

}

namespace ast::repository
{

class Neighbor;

}

namespace graphviz
{

class Writer;

}

namespace artefact::package_neighbors
{

class ArtefactFactory;

class Artefact :
    public artefact::Artefact
{
  public:
    Artefact(
        const ast::Node*,
        const ArtefactFactory&,
        const ast::repository::Neighbor&
        );

    void generate(const Environment&) const override;
    html::Url filename(const PathForNode&) const override;

  private:
    const ast::Node* package;
    const ArtefactFactory& artefactFactory;
    const ast::repository::Neighbor& neighbors;

    std::string print(const Environment& env) const;
    void drawNodes(graphviz::Writer&, const Environment&) const;
    void drawNode(const ast::Node*, library::Set<const ast::Node*>& drawn, graphviz::Writer&, const Environment&) const;
    typedef std::map<const ast::Node*, std::map<const ast::Node*, std::size_t>> EdgeCount;
    typedef std::map<const ast::Node*, library::Set<const ast::Node*>> Edges;
    void drawEdges(graphviz::Writer&, const Environment&) const;
    void drawEdge(const ast::Node*, const ast::Node*, Edges& drawn, graphviz::Writer&, EdgeCount&, const Environment&) const;

};


}
