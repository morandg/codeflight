/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "ast/Function.h"
#include "ast/Class.h"
#include "artefact/generateGraphvizSvg.h"
#include "graphviz/StreamWriter.h"
#include "graphviz/Writer.h"
#include "artefact/overviewfactory/Factory.h"
#include "artefact/Path.h"
#include "library/OrderedSet.h"
#include "ast/repository/Parent.h"
#include "ast/repository/Neighbor.h"
#include "ast/DefaultVisitor.h"
#include "ArtefactFactory.h"
#include "ast/specification/PackageNode.h"
#include <optional>
#include <cassert>


namespace artefact::pakageelement_neighbors
{


Artefact::Artefact(
    const ast::Node* element_,
    const ArtefactFactory& artefactFactory_,
    const overviewfactory::Factory& overviewFactory_,
    const ast::repository::Parent& parents_,
    const ast::repository::Neighbor& neighbors_
    ) :
  element{element_},
  artefactFactory{artefactFactory_},
  overviewFactory{overviewFactory_},
  parents{parents_},
  neighbors{neighbors_}
{
}

void Artefact::generate(const Environment& env) const
{
  const auto orderedNodes = create();
  const auto output = print(orderedNodes, env);
  generateGraphvizSvg(filename(env.pathFor), output, env);
}


Artefact::NodesInPackages Artefact::create() const
{
  library::OrderedSet<const ast::Node*> orderedPackages{};
  std::map<const ast::Node*, library::OrderedSet<const ast::Node*>> orderedNodes{};

  const auto addNode = [&orderedPackages, &orderedNodes, this](const ast::Node* node){
    const auto parent = parents.findParent(node);
    assert(parent.has_value());
    const auto package = parent.value();
    orderedPackages.add(package);
    orderedNodes[package].add(node);
  };

  addNode(element);
  neighbors.foreachOutgoingPackageElement(element, addNode);
  neighbors.foreachIncomingPackageElement(element, addNode);

  assert(orderedNodes.size() == orderedPackages.data().size());

  Artefact::NodesInPackages result{};
  for (const auto package : orderedPackages.data()) {
    result.emplace_back(package, orderedNodes[package].data());
  }

  return result;
}

std::string Artefact::print(const NodesInPackages& value, const Environment& env) const
{
  std::stringstream output{};
  graphviz::StreamWriter writer{output};
  graphviz::Writer gvw{writer};

  gvw.beginGraph();

  for (const auto &package : value) {
    gvw.beginSubgraph();
    gvw.label(env.pathFor(*package.first).display());
    printUrlWhenPossible(package.first, gvw, env);

    for (const auto node : package.second) {
      drawNode(node, gvw, env);
    }
    gvw.endSubgraph();
  }

  const auto drawEdge = [&gvw, &env, this](const ast::Node* source, const ast::Node* destination){
    if (source != destination) {

      const auto spr = parents.findAnchestor(source, ast::specification::PackageNode);
      assert(spr.has_value());
      const auto sp = spr.value();

      const auto dpr = parents.findAnchestor(destination, ast::specification::PackageNode);
      assert(dpr.has_value());
      const auto dp = dpr.value();

      if (sp == dp) {
        gvw.edge(source, destination);
      } else {
        const auto artefact = artefactFactory.packageDependencies({sp, dp});
        env.queue(artefact);
        const auto url = artefact->filename(env.pathFor).relativeTo(filename(env.pathFor)).escaped();

        gvw.beginEdge(source, destination);
        gvw.url(url);
        gvw.endEdge();
      }
    }
  };
  neighbors.foreachOutgoingPackageElement(element, std::bind(drawEdge, element, std::placeholders::_1));
  neighbors.foreachIncomingPackageElement(element, std::bind(drawEdge, std::placeholders::_1, element));

  gvw.endGraph();

  return output.str();
}

void Artefact::drawNode(const ast::Node* node, graphviz::Writer& gvw, const Environment& env) const
{
  gvw.beginNode(graphviz::Shape::Box, node);
  if (node == element) {
    gvw.style(graphviz::Style::Bold);
  }
  gvw.label(node->name);
  printUrlWhenPossible(node, gvw, env);
  gvw.endNode();
}

void Artefact::printUrlWhenPossible(const ast::Node* node, graphviz::Writer& gvw, const Environment& env) const
{
  const auto overview = overviewFactory.produce(node);
  if (overview.has_value()) {
    const auto artefact = overview.value();
    env.queue(artefact);
    const auto rootUrl = filename(env.pathFor);
    const auto url = artefact->filename(env.pathFor).relativeTo(rootUrl);
    gvw.url(url.escaped());
  }
}

html::Url Artefact::filename(const PathForNode& pathFor) const
{
  return html::Url{pathFor(*element).raw(), ".svg"}.append("neighbors");
}


}
