/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/Artefact.h"
#include <string>


namespace ast::repository
{

class Parent;
class Neighbor;

}

namespace graphviz
{

class Writer;

}

namespace artefact::overviewfactory
{

class Factory;

}

namespace artefact::pakageelement_neighbors
{

class ArtefactFactory;

class Artefact :
    public artefact::Artefact
{
  public:
    Artefact(
        const ast::Node*,
        const ArtefactFactory&,
        const overviewfactory::Factory&,
        const ast::repository::Parent&,
        const ast::repository::Neighbor&
        );

    void generate(const Environment&) const override;
    html::Url filename(const PathForNode&) const override;

  private:
    typedef std::vector<std::pair<const ast::Node*, std::vector<const ast::Node*>>> NodesInPackages;

    const ast::Node* element;
    const ArtefactFactory& artefactFactory;
    const overviewfactory::Factory& overviewFactory;
    const ast::repository::Parent& parents;
    const ast::repository::Neighbor& neighbors;

    std::string print(const NodesInPackages &value, const Environment& env) const;
    void drawNode(const ast::Node*, graphviz::Writer&, const Environment&) const;
    Artefact::NodesInPackages create() const;
    void printUrlWhenPossible(const ast::Node*, graphviz::Writer&, const Environment&) const;

};


}
