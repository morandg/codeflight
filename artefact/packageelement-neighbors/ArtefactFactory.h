/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <vector>

namespace ast
{

class Node;

}

namespace artefact
{

class Artefact;

}

namespace artefact::pakageelement_neighbors
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual Artefact* packageDependencies(const std::vector<const ast::Node*>&) const = 0;

};


}
