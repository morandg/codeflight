/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "generateGraphvizSvg.h"
#include "Environment.h"
#include "html/Url.h"
#include <cassert>


namespace artefact
{
namespace
{


bool shouldGenerate(const std::string& svgName, const std::string& graphvizName, const std::string& graph, const Environment& env)
{
  const auto svgExists = env.os.fileExists(svgName);
  if (!svgExists) {
    return true;
  }

  const auto graphvizExists = env.os.fileExists(graphvizName);
  if (!graphvizExists) {
    return true;
  }

  const auto existing = env.os.read(graphvizName);
  if (existing != graph) {
    return true;
  }

  return false;
}


}


void generateGraphvizSvg(const html::Url& name, const std::string &graph, const Environment &env)
{
  assert(name.getRepresentation() == ".svg");

  const auto graphvizName = env.rootDirectory / name.asRepresentation(".gv").filename();
  const auto svgName = env.rootDirectory / name.filename();

  if (shouldGenerate(svgName, graphvizName, graph, env)) {
    env.os.write(graphvizName, graph);
    env.os.execute("dot -T svg -o" + svgName.string() + " " + graphvizName.string());
  }
}


}
