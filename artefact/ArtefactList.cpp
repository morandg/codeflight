/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ArtefactList.h"
#include "html/Url.h"
#include "Environment.h"
#include "html/Writer.h"
#include "Artefact.h"

namespace artefact
{


void printArtefactList(const ArtefactList& artefacts, const Environment& env, html::Writer& xw)
{
  xw.beginUnnumberedList();

  for (const auto& itr : artefacts) {
    const auto artefact = itr.second;
    env.queue(artefact);

    xw.beginListItem();

    const auto url = artefact->filename(env.pathFor);
    xw.link(itr.first, url);

    xw.endListItem();
  }

  xw.endUnnumberedList();
}


}
