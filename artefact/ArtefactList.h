/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <vector>
#include <map>
#include <string>


namespace html
{

class Writer;

}

namespace artefact
{

class Artefact;
struct Environment;


typedef std::vector<std::pair<std::string, Artefact*>> ArtefactList;

void printArtefactList(const ArtefactList&, const Environment&, html::Writer&);


}
