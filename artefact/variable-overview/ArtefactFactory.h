/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace ast
{

class Variable;

}

namespace artefact
{

class Artefact;

}

namespace artefact::variable_overview
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual Artefact* variableNeighbors(const ast::Variable*) const = 0;

};


}
