/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/Artefact.h"

namespace ast
{

class Function;

}

namespace html
{

class Writer;
class WriterFactory;

}

namespace artefact::function_overview
{

class ArtefactFactory;


class Artefact :
    public artefact::Artefact
{
  public:
    Artefact(
        const ast::Function*,
        const ArtefactFactory&,
        const html::WriterFactory&
        );

    void generate(const Environment&) const override;
    html::Url filename(const PathForNode&) const override;

  private:
    const ast::Function* node;
    const ArtefactFactory& artefactFactory;
    const html::WriterFactory& htmlFactory;
    void printBody(const Environment &env, html::Writer &xw) const;
};


}
