/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace ast
{

class Function;

}

namespace artefact
{

class Artefact;

}

namespace artefact::function_overview
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual Artefact* functionNeighbors(const ast::Function*) const = 0;

};


}
