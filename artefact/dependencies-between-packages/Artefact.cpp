/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "Builder.h"
#include "artefact/Environment.h"
#include "artefact/generateGraphvizSvg.h"
#include "ast/Class.h"
#include "graphviz/StreamWriter.h"
#include "graphviz/Writer.h"
#include "ast/Project.h"
#include "html/Url.h"
#include "artefact/Path.h"
#include "artefact/overviewfactory/Factory.h"


namespace artefact::dependencies_between_packages
{


Artefact::Artefact(
    const std::vector<const ast::Node*>& packages_,
    const overviewfactory::Factory& overviewFactory_,
    const ast::repository::Parent& parents_
    ) :
  packages{packages_},
  overviewFactory{overviewFactory_},
  parents{parents_}
{
}

void Artefact::generate(const Environment& env) const
{
  std::stringstream output;
  writePackagesGraph(output, packages, env);
  generateGraphvizSvg(filename(env.pathFor), output.str(), env);
}

void Artefact::writePackagesGraph(std::ostream& output, const std::vector<const ast::Node*>& included, const Environment& env) const
{
  Builder builder{included, parents};
  builder.build();

  graphviz::StreamWriter writer{output};
  graphviz::Writer gvw{writer};

  const auto rootUrl = filename(env.pathFor);

  gvw.beginGraph();

  builder.foreach([&builder, &gvw, env, rootUrl, this](const ast::Node* package){
    const auto context = env.pathFor(*package);

    gvw.beginSubgraph();
    gvw.label(context.display());
    printUrlWhenPossible(package, gvw, env);

    builder.foreach(package, [&gvw, env, context, rootUrl, this](const ast::Node* node){
      const auto fullPath = env.pathFor(*node);
      const auto localPath = fullPath.subPathIn(context);

      gvw.beginNode(graphviz::Shape::Box, node);
      gvw.label(localPath.display());
      printUrlWhenPossible(node, gvw, env);
      gvw.endNode();
    });

    gvw.endSubgraph();
  });

  builder.foreach([&gvw](const ast::Node* source, const ast::Node* destination){
    gvw.edge(source, destination);
  });

  gvw.endGraph();
}

void Artefact::printUrlWhenPossible(const ast::Node* node, graphviz::Writer& gvw, const Environment& env) const
{
  const auto overview = overviewFactory.produce(node);
  if (overview.has_value()) {
    const auto artefact = overview.value();
    env.queue(artefact);
    const auto rootUrl = filename(env.pathFor);
    const auto url = artefact->filename(env.pathFor).relativeTo(rootUrl);
    gvw.url(url.escaped());
  }
}

html::Url Artefact::filename(const PathForNode& pathFor) const
{
  Paths paths{};
  std::transform(packages.begin(), packages.end(), std::back_inserter(paths), [&pathFor](const ast::Node* node){
    return pathFor(*node);
  });

  const auto name = Path::encodeList(paths);
  return {{{"dependencies-between-packages", name}}, ".svg"};
}


}
