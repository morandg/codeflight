/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/Artefact.h"
#include "ast/NodeSpecification.h"
#include <string>


namespace ast
{

class Project;
class Node;

}

namespace ast::graph
{

class Graph;

}

namespace ast::repository
{

class Parent;

}

namespace graphviz
{

class Writer;

}

namespace artefact::overviewfactory
{

class Factory;

}

namespace artefact::dependencies_between_packages
{


class Artefact :
    public artefact::Artefact
{
  public:
    Artefact(
        const std::vector<const ast::Node*>&,
        const overviewfactory::Factory&,
        const ast::repository::Parent&
        );

    void generate(const Environment&) const override;
    html::Url filename(const PathForNode&) const override;

  private:
    const std::vector<const ast::Node*> packages;
    const overviewfactory::Factory& overviewFactory;
    const ast::repository::Parent& parents;

    void writePackagesGraph(std::ostream &output, const std::vector<const ast::Node *> &included, const Environment&) const;
    void printUrlWhenPossible(const ast::Node*, graphviz::Writer&, const Environment&) const;
};


}
