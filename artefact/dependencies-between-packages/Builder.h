/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "library/OrderedSet.h"
#include "ast/NodeSpecification.h"
#include <vector>
#include <functional>
#include <map>

namespace ast
{

class Node;

}

namespace ast::repository
{

class Parent;

}

namespace artefact::dependencies_between_packages
{


class Builder
{
  public:
    typedef ast::Node Package;
    typedef ast::Node Drawable;
    typedef std::function<void(const Package*)> PackageVisitor;
    typedef std::function<void(const Drawable*)> DrawableVisitor;
    typedef std::function<void(const Drawable*, const Drawable*)> EdgeVisitor;

    Builder(const std::vector<const Package*>&, const ast::repository::Parent&);
    void build();

    void foreach(const PackageVisitor&) const;
    void foreach(const Package*, const DrawableVisitor&) const;
    void foreach(const EdgeVisitor&) const;

  private:
    const ast::repository::Parent& parents;
    typedef std::vector<const Drawable*> DrawableList;

    const std::vector<const Package*> included;
    std::map<const Package*, DrawableList> drawableList{};
    const Package* currentPackage{};
    std::map<const Drawable*, library::OrderedSet<const Drawable*>> referencesFromDrawableAndChildren{};
    std::vector<const Drawable*> drawableStack{};

    std::map<const Package*, library::OrderedSet<const Drawable*>> finalDrawableList{};
    std::map<const Drawable*, DrawableList> finalEdges{};

    void check(const ast::Node* root);
    void handle(const ast::Node* node);

    void buildFinal();


};


}
