/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Builder.h"
#include "ast/Package.h"
#include "ast/query/isOfType.h"
#include "ast/specification/PackageElement.h"
#include "ast/repository/Parent.h"
#include <cassert>


namespace artefact::dependencies_between_packages
{
namespace
{


const ast::NodeSpecification& NodeIsDrawable = ast::specification::PackageElement;
const ast::NodeSpecification PackageNode = [](const ast::Node* node){
  return ast::query::isOfType<ast::Package>(node);
};


}


Builder::Builder(const std::vector<const Package*> &included_, const ast::repository::Parent& parents_) :
  parents{parents_},
  included{included_}
{
}

void Builder::build()
{
  for (const auto& package : included) {
    currentPackage = package;
    check(package);
  }

  buildFinal();
}

void Builder::check(const ast::Node *root)
{
  for (const auto& node : root->children) {
    if (PackageNode(node)) {
      continue;
    }

    if (NodeIsDrawable(node)) {
      assert(currentPackage);
      drawableList[currentPackage].push_back(node);
      drawableStack.push_back(node);
      handle(node);
      drawableStack.pop_back();
    } else {
      handle(node);
    }
  }
}

void Builder::handle(const ast::Node *node)
{
  assert(!drawableStack.empty());
  auto& list = referencesFromDrawableAndChildren[drawableStack.back()];

  for (const auto ref : node->references) {
    const auto d = parents.findAnchestor(ref, NodeIsDrawable);
    if (d.has_value()) {
      list.add(d.value());
    }
  }

  check(node);
}

void Builder::buildFinal()
{
  for (const Package* package : included) {
    auto& sourceNodes = finalDrawableList[package];

    // only drawable sources
    const auto listIdx = drawableList.find(package);
    assert(listIdx != drawableList.end());
    for (const auto& source : listIdx->second) {
      auto& sourceEdges = finalEdges[source];

      // references from this and all child nodes
      const auto referencesIdx = referencesFromDrawableAndChildren.find(source);
      assert(referencesIdx != referencesFromDrawableAndChildren.end());
      for (const auto& dst : referencesIdx->second.data()) {
        const auto destination = dst;

        const std::optional<const Package*> destPackageIdx = parents.findAnchestor(destination, PackageNode);
        if (destPackageIdx.has_value()) {
          const Package* dstPackage = destPackageIdx.value();
          if ((dstPackage != package)) {
            const bool valid = std::find(included.begin(), included.end(), dstPackage) != included.end();
            if (valid) {
              sourceNodes.add(source);
              finalDrawableList[dstPackage].add(destination);
              sourceEdges.push_back(destination);
            }
          }
        }
      }
    }
  }
}

void Builder::foreach(const PackageVisitor& visitor) const
{
  for (const auto package : included) {
    visitor(package);
  }
}

void Builder::foreach(const Package* package, const DrawableVisitor& visitor) const
{
  const auto idx = finalDrawableList.find(package);
  assert(idx != finalDrawableList.end());

  for (const ast::Node* node : idx->second.data()) {
      visitor(node);
  }
}

void Builder::foreach(const EdgeVisitor& visitor) const
{
  for (const auto& edges : finalEdges) {
    for (const auto dst : edges.second) {
      visitor(edges.first, dst);
    }
  }
}


}
