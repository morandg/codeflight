/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/Artefact.h"
#include "artefact/Path.h"
#include "ast/graph/cycle/types.h"
#include <string>
#include <set>


namespace ast
{

class Project;
class Node;

}

namespace ast::graph
{

class Graph;

}

namespace html
{

class Writer;
class WriterFactory;

}

namespace artefact::packages_with_cyclic_dependencies
{

class ArtefactFactory;


class Artefact :
    public artefact::Artefact
{
  public:
    typedef std::function<double(const ast::Node*, const ast::Node*)> Stability;

    Artefact(
        const ast::Project*,
        const ArtefactFactory&,
        const html::WriterFactory&,
        const Stability&
        );

    void generate(const Environment&) const override;
    html::Url filename(const PathForNode&) const override;

  private:
    const ast::Project* root;
    const ArtefactFactory& artefactFactory;
    const html::WriterFactory& htmlFactory;
    Stability stabilityFunc;
    const html::Url name{{{"packages-with-cyclic-dependencies"}}, ".html"};

    struct PackageInfo
    {
        const ast::Node *package;
        std::size_t inCycles;
    };
    struct DependencyInfo
    {
        const ast::Node *from;
        const ast::Node *to;
        std::size_t inCycles;
    };
    ast::graph::cycle::Cycles getPackagesWithCyclicDependencies() const;
    std::vector<PackageInfo> convertToPackages(const ast::graph::cycle::Cycles&, const Environment&) const;
    std::vector<DependencyInfo> convertToDependencies(const ast::graph::cycle::Cycles&, const Environment&) const;
    void printMetric(std::size_t cycles, std::size_t dependencies, std::size_t packages, html::Writer&) const;
    void printPackageList(const std::vector<Artefact::PackageInfo>&, html::Writer&, const Environment&) const;
    void printDependencyList(const std::vector<Artefact::DependencyInfo>&, html::Writer&, const Environment&) const;
};


}
