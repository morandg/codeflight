/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "ast/Project.h"
#include "html/Writer.h"
#include "html/WriterFactory.h"
#include "ast/graph/graph.h"
#include "ast/graph/cycle/johnson.h"
#include "ast/specification/PackageNode.h"
#include "ArtefactFactory.h"
#include "library/util.h"
#include <sstream>


namespace artefact::packages_with_cyclic_dependencies
{


Artefact::Artefact(
    const ast::Project* root_,
    const ArtefactFactory& artefactFactory_,
    const html::WriterFactory& htmlFactory_,
    const Stability& stabilityFunc_
    ) :
  root{root_},
  artefactFactory{artefactFactory_},
  htmlFactory{htmlFactory_},
  stabilityFunc{stabilityFunc_}
{
}

ast::graph::cycle::Cycles Artefact::getPackagesWithCyclicDependencies() const
{
  ast::graph::Graph graph = ast::graph::buildGraph(*root, ast::specification::PackageNode);
  graph.removeDuplicatedEdges();
  graph.removeEdgeToItself();

  return ast::graph::cycle::johnson(graph);
}

void Artefact::generate(const Environment& env) const
{
  const auto nodesWithCyclicDependencies = getPackagesWithCyclicDependencies();
  const auto packages = convertToPackages(nodesWithCyclicDependencies, env);
  const auto dependencies = convertToDependencies(nodesWithCyclicDependencies, env);

  const std::string title = "Packages with cyclic dependencies";

  std::stringstream output;

  auto xw = htmlFactory.produce(name);

  xw->beginDocument(title);

  xw->beginSection(title);
  printMetric(nodesWithCyclicDependencies.size(), dependencies.size(), packages.size(), *xw);
  if (!nodesWithCyclicDependencies.empty()) {
    printDependencyList(dependencies, *xw, env);
    printPackageList(packages, *xw, env);
  }
  xw->endSection();

  xw->endDocument();

  xw->serialize(output);

  const auto fullname = env.rootDirectory / name.filename();

  env.os.write(fullname, output.str());
}

void Artefact::printMetric(std::size_t cycles, std::size_t dependencies, std::size_t packages, html::Writer& xw) const
{
  xw.beginTable({"metric", "value"}, html::Writer::TableType::StringNumbers);
  xw.tableCells({"Number of cycles", std::to_string(cycles)});
  xw.tableCells({"Number of involved dependencies", std::to_string(dependencies)});
  xw.tableCells({"Number of involved packages", std::to_string(packages)});
  xw.endTable();
}

void Artefact::printPackageList(const std::vector<Artefact::PackageInfo>& packages, html::Writer& xw, const Environment& env) const
{
  xw.beginSection("Involved packages");

  xw.beginTable({"Package", "in cycles"}, html::Writer::TableType::StringNumbers);

  for (const auto& package : packages) {
    const auto artefact = artefactFactory.package(package.package);
    env.queue(artefact);
    const auto name = env.pathFor(*package.package).display();
    const auto url = artefact->filename(env.pathFor);
    const auto inCycles = std::to_string(package.inCycles);

    xw.beginTableRow();
    xw.tableCellLink(name, url);
    xw.tableCell(inCycles);
    xw.endTableRow();
  }

  xw.endTable();
  xw.endSection();
}

void Artefact::printDependencyList(const std::vector<Artefact::DependencyInfo>& dependencies, html::Writer& xw, const Environment& env) const
{
  xw.beginSection("Involved dependencies");

  xw.beginTable({"from", "to", "in cycles", "stability"}, html::Writer::TableType::StringStringNumbers);

  for (const auto& dependency: dependencies) {
    const auto artefact = artefactFactory.packageDependencies({dependency.from, dependency.to});
    env.queue(artefact);
    const auto url = artefact->filename(env.pathFor);
    const auto from = env.pathFor(*dependency.from).display();
    const auto to = env.pathFor(*dependency.to).display();
    const auto inCycles = std::to_string(dependency.inCycles);
    const auto stability = library::toString(stabilityFunc(dependency.from, dependency.to), 3);

    xw.beginTableRow();
    xw.tableCellLink(from, url);
    xw.tableCellLink(to, url);
    xw.tableCell(inCycles);
    xw.tableCell(stability);
    xw.endTableRow();
  }

  xw.endTable();
  xw.endSection();
}

std::vector<Artefact::PackageInfo> Artefact::convertToPackages(const ast::graph::cycle::Cycles& cycles, const Environment& env) const
{
  std::map<const ast::Node*, std::size_t> packagesInCycles{};
  for (const auto& nodes : cycles) {
    for (const auto& node : nodes) {
      packagesInCycles[node]++;
    }
  }

  std::vector<PackageInfo> result{};
  for (const auto& package : packagesInCycles) {
    result.push_back({package.first, package.second});
  }

  std::sort(result.begin(), result.end(), [&env](const PackageInfo& left, const PackageInfo& right){
    const auto leftPath = env.pathFor(*left.package).display();
    const auto rightPath = env.pathFor(*right.package).display();
    return leftPath <  rightPath;
  });

  return result;
}

std::vector<Artefact::DependencyInfo> Artefact::convertToDependencies(const ast::graph::cycle::Cycles& cycles, const Environment& env) const
{
  std::map<std::pair<const ast::Node*, const ast::Node*>, std::size_t> dependencyInCycles{};
  for (const auto& cycle : cycles) {
    for (std::size_t i = 0; i < cycle.size(); i++) {
      const auto from = cycle.at(i);
      const auto to = cycle.at((i+1) % cycle.size());
      dependencyInCycles[{from, to}]++;
    }
  }

  std::vector<DependencyInfo> result{};
  for (const auto& dependency : dependencyInCycles) {
    const auto from = dependency.first.first;
    const auto to = dependency.first.second;
    result.push_back({from, to, dependency.second});
  }

  std::sort(result.begin(), result.end(), [&env](const DependencyInfo& left, const DependencyInfo& right){
    const auto leftFrom = env.pathFor(*left.from).display();
    const auto rightFrom = env.pathFor(*right.from).display();
    if (leftFrom == rightFrom) {
      const auto leftTo = env.pathFor(*left.to).display();
      const auto rightTo = env.pathFor(*right.to).display();
      return leftTo < rightTo;
    } else {
      return leftFrom < rightFrom;
    }
  });

  return result;
}

html::Url Artefact::filename(const PathForNode&) const
{
  return name;
}


}
