/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <vector>

namespace ast
{

class Node;

}

namespace artefact
{

class Artefact;

}

namespace artefact::packages_with_cyclic_dependencies
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual Artefact* packageDependencies(const std::vector<const ast::Node*>&) const = 0;
  virtual Artefact* package(const ast::Node*) const = 0;

};


}
