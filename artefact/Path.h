/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>


namespace artefact
{


class Path;
typedef std::vector<Path> Paths;


class Path
{
  public:
    Path() = default;
    Path(const std::vector<std::string>&);
    Path(const std::string&);

    std::string display() const;
    std::string encoded() const;

    Path add(const std::string&) const;
    Path remove() const;

    Path subPathIn(const Path&) const;

    std::vector<std::string> raw() const; //TODO remove


    static std::string encodeList(const Paths&);
    static Paths decodeList(const std::string&);

  private:
    std::vector<std::string> path{};

};

bool operator==(const Path&, const Path&);

}
