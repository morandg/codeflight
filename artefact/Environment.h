/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <functional>
#include <string>
#include <filesystem>


namespace ast
{

class Node;

}

namespace artefact
{

class Artefact;
class Path;


typedef std::function<Path(const ast::Node&)> PathForNode;
typedef std::function<void(const Artefact*)> QueueAdder;

class Os
{
  public:
    virtual ~Os() = default;

    virtual void write(const std::filesystem::path& name, const std::string& content) = 0;
    virtual bool fileExists(const std::filesystem::path&) const = 0;
    virtual std::string read(const std::filesystem::path& name) const = 0;
    virtual void execute(const std::string& command) = 0;

};

struct Environment
{
    const PathForNode pathFor;
    const QueueAdder queue;

    const std::filesystem::path rootDirectory;
    Os& os;
};


}
