/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Path.h"
//#include "ast/Project.h"
//#include "feature-test/filesystem/InMemory.h"
//#include "feature-test/logger/MemoryLogger.h"
//#include "artefact/unit-test/SimpleArtefact.h"
#include <gmock/gmock.h>


namespace artefact::unit_test
{
namespace
{

using namespace testing;


TEST(Path_Test, returns_true_when_comparing_identical_paths)
{
  const Path path{{std::string{"a"}, "b"}};

  EXPECT_TRUE(path == path);
}

TEST(Path_Test, returns_true_when_comparing_equal_paths)
{
  const Path lhs{{std::string{"a"}, "b"}};
  const Path rhs{{std::string{"a"}, "b"}};

  EXPECT_TRUE(lhs == rhs);
}

TEST(Path_Test, returns_false_when_comparing_different_paths_with_equal_length)
{
  const Path lhs{{std::string{"a"}, "x"}};
  const Path rhs{{std::string{"a"}, "y"}};

  EXPECT_FALSE(lhs == rhs);
}

TEST(Path_Test, returns_false_when_comparing_different_paths_with_different_length)
{
  const Path lhs{{std::string{"a"}, "x"}};
  const Path rhs{{std::string{"a"}}};

  EXPECT_FALSE(lhs == rhs);
}

TEST(Path_Test, returns_only_suffix_when_requesting_subPath)
{
  const Path context{{std::string{"a"}, "b"}};
  const Path full{{std::string{"a"}, "b", "c", "d"}};
  const Path expected{{std::string{"c"}, "d"}};

  const auto suffix = full.subPathIn(context);

  EXPECT_EQ(expected, suffix);
}

TEST(Path_Test, suffix_is_empty_when_full_path_matches_the_context)
{
  const Path context{{std::string{"a"}, "b"}};
  const Path full{{std::string{"a"}, "b"}};
  const Path expected{};

  const auto suffix = full.subPathIn(context);

  EXPECT_EQ(expected, suffix);
}

TEST(Path_Test, returns_error_when_path_is_less_deep_than_context)
{
  const Path context{{std::string{"a"}, "b"}};
  const Path full{{std::string{"a"}}};

  EXPECT_THROW(full.subPathIn(context), std::invalid_argument);
}

TEST(Path_Test, returns_error_when_path_is_not_in_context)
{
  const Path context{{std::string{"a"}, "x"}};
  const Path full{{std::string{"a"}, "y", "c", "d"}};

  EXPECT_THROW(full.subPathIn(context), std::invalid_argument);
}


}
}
