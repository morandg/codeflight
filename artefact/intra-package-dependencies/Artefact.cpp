/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "artefact/Environment.h"
#include "artefact/generateGraphvizSvg.h"
#include "ast/Node.h"
#include "ast/Class.h"
#include "ast/graph/graph.h"
#include "ast/specification/PackageElement.h"
#include "graphviz/StreamWriter.h"
#include "graphviz/Writer.h"
#include "artefact/overviewfactory/Factory.h"
#include "html/Url.h"
#include "artefact/Path.h"
#include <sstream>


namespace artefact::intra_package_dependencies
{


Artefact::Artefact(const ast::Node* node_, const overviewfactory::Factory& overviewFactory_) :
  node{node_},
  overviewFactory{overviewFactory_}
{
}

void Artefact::generate(const Environment& env) const
{
  std::stringstream output;
  writeGraph(output, env);
  generateGraphvizSvg(filename(env.pathFor), output.str(), env);
}

void Artefact::writeGraph(std::ostream& output, const Environment& env) const
{
  ast::graph::Graph graph = ast::graph::buildDirectChildrenGraph(*node, ast::specification::PackageElement);
  graph.removeDuplicatedEdges();
  graph.removeEdgeToItself();

  graphviz::StreamWriter writer{output};
  graphviz::Writer gvw{writer};

  gvw.beginGraph();

  graph.foreach([&gvw, &env, this](const ast::Node* node){
    gvw.beginNode(graphviz::Shape::Box, node);
    gvw.label(node->name);

    const auto overview = overviewFactory.produce(node);
    if (overview.has_value()) {
      const auto artefact = overview.value();
      env.queue(artefact);
      const auto rootUrl = filename(env.pathFor);
      const auto url = artefact->filename(env.pathFor).relativeTo(rootUrl);
      gvw.url(url.escaped());
    }

    gvw.endNode();
  });
  graph.foreach([&gvw](const ast::Node* source, const ast::Node* destination){
    gvw.edge(source, destination);
  });

  gvw.endGraph();
}

html::Url Artefact::filename(const PathForNode& pathFor) const
{
  return html::Url{pathFor(*node).raw(), ".svg"}.append("package-content-dependencies");
}


}
