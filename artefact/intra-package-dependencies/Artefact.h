/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/Artefact.h"
#include <string>


namespace ast
{

class Node;

}

namespace artefact::overviewfactory
{

class Factory;

}

namespace artefact::intra_package_dependencies
{


class Artefact :
    public artefact::Artefact
{
  public:
    Artefact(const ast::Node*, const overviewfactory::Factory&);

    void generate(const Environment&) const override;
    html::Url filename(const PathForNode&) const override;

  private:
    const ast::Node* node;
    const overviewfactory::Factory& overviewFactory;

    void writeGraph(std::ostream &output, const Environment&) const;
};


}
