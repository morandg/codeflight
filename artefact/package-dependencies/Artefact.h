/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/Artefact.h"
#include <string>


namespace ast
{

class Project;

}

namespace artefact::package_dependencies
{

class ArtefactFactory;


class Artefact :
    public artefact::Artefact
{
  public:
    Artefact(const ast::Node*, const ArtefactFactory&);

    void generate(const Environment&) const override;
    html::Url filename(const PathForNode&) const override;

  private:
    const ast::Node* node;
    const ArtefactFactory& artefactFactory;
    void printPackageDependencies(const artefact::PathForNode &pathFor, std::ostream &output, const artefact::QueueAdder &enqueue) const;
};


}
