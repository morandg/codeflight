/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "artefact/Environment.h"
#include "artefact/generateGraphvizSvg.h"
#include "html/Url.h"
#include "artefact/Path.h"
#include "ast/graph/graph.h"
#include "ast/specification/PackageNode.h"
#include "graphviz/StreamWriter.h"
#include "graphviz/Writer.h"
#include "ArtefactFactory.h"
#include <sstream>


namespace artefact::package_dependencies
{


Artefact::Artefact(
    const ast::Node* node_,
    const ArtefactFactory& artefactFactory_
    ) :
  node{node_},
  artefactFactory{artefactFactory_}
{
}

void Artefact::generate(const Environment& env) const
{
  std::stringstream output;
  printPackageDependencies(env.pathFor, output, [env](const artefact::Artefact* item){
    env.queue(item);
  });

  generateGraphvizSvg(filename(env.pathFor), output.str(), env);
}

void Artefact::printPackageDependencies(const artefact::PathForNode& pathFor, std::ostream& output, const artefact::QueueAdder& enqueue) const
{
  ast::graph::Graph graph = ast::graph::buildGraph(*node, ast::specification::PackageNode);

  graph.removeDuplicatedEdges();
  graph.removeEdgeToItself();

  const auto rootUrl = filename(pathFor);

  graphviz::StreamWriter writer{output};
  graphviz::Writer gvw{writer};

  gvw.beginGraph();

  graph.foreach([this, &gvw, &pathFor, &enqueue, &rootUrl](const ast::Node* node){
    const auto artefact = artefactFactory.package(node);
    enqueue(artefact);

    const auto name = pathFor(*node);
    const auto url = artefact->filename(pathFor).relativeTo(rootUrl);

    gvw.beginNode(graphviz::Shape::Box, node);
    gvw.label(name.display());
    gvw.url(url.escaped());
    gvw.endNode();
  });
  graph.foreach([this, &gvw, &pathFor, &enqueue, &rootUrl](const ast::Node* source, const ast::Node* destination){
    const auto artefact = artefactFactory.packageDependencies({source, destination});
    enqueue(artefact);

    const auto url = artefact->filename(pathFor).relativeTo(rootUrl);

    gvw.beginEdge(source, destination);
    gvw.url(url.escaped());
    gvw.endEdge();
  });

  gvw.endGraph();
}

html::Url Artefact::filename(const PathForNode& pathFor) const
{
  return html::Url{pathFor(*node).raw(), ".svg"}.append("package-dependencies");
}


}
