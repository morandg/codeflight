/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/package-dependencies/ArtefactFactory.h"
#include "artefact/overview/ArtefactFactory.h"
#include "artefact/class-overview/ArtefactFactory.h"
#include "artefact/package-overview/ArtefactFactory.h"
#include "artefact/overviewfactory/ArtefactFactory.h"
#include "artefact/intra-class-dependencies/ArtefactFactory.h"
#include "artefact/packages-with-cyclic-dependencies/ArtefactFactory.h"
#include "artefact/package-neighbors/ArtefactFactory.h"
#include "artefact/stable-dependency-principle-violations/ArtefactFactory.h"
#include "artefact/function-overview/ArtefactFactory.h"
#include "artefact/variable-overview/ArtefactFactory.h"
#include "artefact/overviewfactory/Factory.h"
#include "artefact/packageelement-neighbors/ArtefactFactory.h"
#include "metric/InstabilityService.h"
#include "html/WriterFactory.h"
#include "html/Writer.h"

namespace ast
{

class Class;
class Function;
class Variable;

}

namespace ast::repository
{

class Parent;
class Neighbor;

}

namespace metric
{

class Repository;

}

namespace artefact::factory
{


class Factory :
    public overview::ArtefactFactory,
    public package_dependencies::ArtefactFactory,
    public class_overview::ArtefactFactory,
    public package_overview::ArtefactFactory,
    public overviewfactory::ArtefactFactory,
    public intra_class_dependencies::ArtefactFactory,
    public packages_with_cyclic_dependencies::ArtefactFactory,
    public package_neighbors::ArtefactFactory,
    public stable_dependency_principle_violations::ArtefactFactory,
    public function_overview::ArtefactFactory,
    public variable_overview::ArtefactFactory,
    public pakageelement_neighbors::ArtefactFactory
{
public:
  Factory(
      const ast::repository::Parent&,
      const ast::repository::Neighbor&,
      const metric::Repository&,
      const html::Resources&
      );

  Artefact* overview(const ast::Project*) const;
  Artefact* packagesWithCyclicDependencies(const ast::Project*) const override;
  Artefact* stableDependencyPrincipleViolations(const ast::Project*) const override;
  Artefact* package(const ast::Node* node) const override;
  Artefact* childPackageDependencies(const ast::Node*) const override;
  Artefact* packageNeighbors(const ast::Node*) const override;
  Artefact* intraPackageDependencies(const ast::Node* node) const override;
  Artefact* clazz(const ast::Class* clazz) const override;
  Artefact* intraClassDependencies(const ast::Class* clazz) const override;
  Artefact* packageDependencies(const std::vector<const ast::Node*>&) const override;
  Artefact* classNeighbors(const ast::Class* clazz) const override;
  Artefact* functionNeighbors(const ast::Function*) const override;
  Artefact* function(const ast::Function*) const override;
  Artefact* variableNeighbors(const ast::Variable*) const override;
  Artefact* variable(const ast::Variable*) const override;

private:
  const ast::repository::Parent& parents;
  const ast::repository::Neighbor& neighbors;
  const metric::Repository& metrics;
  const metric::InstabilityService instability;
  const overviewfactory::Factory overviewFactory;
  const html::WriterFactory htmlFactory;
};


}
