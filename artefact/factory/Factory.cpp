/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "ast/Class.h"
#include "ast/Function.h"
#include "ast/Variable.h"
#include "artefact/overview/Artefact.h"
#include "artefact/package-dependencies/Artefact.h"
#include "artefact/intra-class-dependencies/Artefact.h"
#include "artefact/intra-package-dependencies/Artefact.h"
#include "artefact/dependencies-between-packages/Artefact.h"
#include "artefact/class-overview/Artefact.h"
#include "artefact/package-overview/Artefact.h"
#include "artefact/packageelement-neighbors/Artefact.h"
#include "artefact/packages-with-cyclic-dependencies/Artefact.h"
#include "artefact/package-neighbors/Artefact.h"
#include "artefact/stable-dependency-principle-violations/Artefact.h"
#include "artefact/function-overview/Artefact.h"
#include "artefact/variable-overview/Artefact.h"
#include "metric/Repository.h"
#include <cassert>


namespace artefact::factory
{


Factory::Factory(
    const ast::repository::Parent& parents_,
    const ast::repository::Neighbor& neihbors_,
    const metric::Repository& metrics_,
    const html::Resources& resources_
    ) :
  parents{parents_},
  neighbors{neihbors_},
  metrics{metrics_},
  instability{metrics.packageInstabilityFunction()},
  overviewFactory{*this},
  htmlFactory{resources_}
{
}

Artefact *Factory::overview(const ast::Project* node) const
{
  return new overview::Artefact(node, metrics, *this, htmlFactory, overviewFactory);
}

Artefact *Factory::packagesWithCyclicDependencies(const ast::Project* node) const
{
  const auto stability = std::bind(&metric::InstabilityService::stability, &instability, std::placeholders::_1, std::placeholders::_2);
  return new packages_with_cyclic_dependencies::Artefact(node, *this, htmlFactory, stability);
}

Artefact *Factory::stableDependencyPrincipleViolations(const ast::Project* node) const
{
  const auto stability = std::bind(&metric::InstabilityService::stability, &instability, std::placeholders::_1, std::placeholders::_2);
  return new stable_dependency_principle_violations::Artefact(node, *this, htmlFactory, stability, neighbors);
}

Artefact *Factory::package(const ast::Node *node) const
{
  assert(node);
  return new package_overview::Artefact(node, *this, htmlFactory);
}

Artefact *Factory::childPackageDependencies(const ast::Node* node) const
{
  return new package_dependencies::Artefact(node, *this);
}

Artefact *Factory::packageNeighbors(const ast::Node* package) const
{
  assert(package);
  return new package_neighbors::Artefact(package, *this, neighbors);
}

Artefact *Factory::intraPackageDependencies(const ast::Node *node) const
{
  assert(node);
  return new intra_package_dependencies::Artefact(node, overviewFactory);
}

Artefact *Factory::clazz(const ast::Class *clazz) const
{
  assert(clazz);
  return new class_overview::Artefact(clazz, *this, htmlFactory);
}

Artefact *Factory::intraClassDependencies(const ast::Class *clazz) const
{
  assert(clazz);
  return new intra_class_dependencies::Artefact(clazz, *this);
}

Artefact *Factory::packageDependencies(const std::vector<const ast::Node*>& packages) const
{
  return new dependencies_between_packages::Artefact(packages, overviewFactory, parents);
}

Artefact *Factory::classNeighbors(const ast::Class* clazz) const
{
  assert(clazz);
  return new pakageelement_neighbors::Artefact(clazz, *this, overviewFactory, parents, neighbors);
}

Artefact *Factory::functionNeighbors(const ast::Function* function) const
{
  assert(function);
  return new pakageelement_neighbors::Artefact(function, *this, overviewFactory, parents, neighbors);
}

Artefact *Factory::function(const ast::Function* function) const
{
  assert(function);
  return new function_overview::Artefact(function, *this, htmlFactory);
}

Artefact *Factory::variableNeighbors(const ast::Variable* variable) const
{
  assert(variable);
  return new pakageelement_neighbors::Artefact(variable, *this, overviewFactory, parents, neighbors);
}

Artefact *Factory::variable(const ast::Variable* variable) const
{
  assert(variable);
  return new variable_overview::Artefact(variable, *this, htmlFactory);
}


}
