/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "ast/traverse/type.h"
#include "ast/Project.h"
#include "ast/Class.h"
#include "ast/Function.h"
#include "ast/Variable.h"
#include "ast/Package.h"
#include "html/Writer.h"
#include "html/WriterFactory.h"
#include "html/Url.h"
#include "artefact/Path.h"
#include "graph/Queue.h"
#include "artefact/Environment.h"
#include "ArtefactFactory.h"
#include "artefact/ArtefactList.h"
#include "metric/Repository.h"
#include "artefact/overviewfactory/Factory.h"


namespace artefact::overview
{
namespace
{


template<typename T>
bool hasChildOf(const ast::Node& node)
{
  bool found = false;

  ast::traverse::recursive<T>(node, [&found](const T&){
    found = true;
  });

  return found;
}


template<typename T>
void printOverviewFor(const std::string& title, const metric::Metrics& metrics, const ast::Node* node, html::Writer& xw, const std::function<Artefact::Link(const ast::Node*)>& nodeOverview)
{
  if (!hasChildOf<T>(*node)) {
    return;
  }

  xw.beginSection(title);

  std::vector<std::string> head{"Name"};
  for (const auto& metric : metrics) {
    head.push_back(metric.name);
  }
  xw.beginTable(head, html::Writer::TableType::StringNumbers);

  ast::traverse::recursive<T>(*node, [&metrics, &nodeOverview, &xw](const ast::Node& variable){
    const auto overview = nodeOverview(&variable);

    xw.beginTableRow();

    xw.tableCellLink(overview.first, overview.second);

    for (const auto& metric : metrics) {
      const std::string value = metric.value(variable);
      xw.tableCell(value);
    }

    xw.endTableRow();
  });

  xw.endTable();

  xw.endSection();
}


}


Artefact::Artefact(
    const ast::Project *node_,
    const metric::Repository& metrics_,
    const ArtefactFactory& artefactFactory_,
    const html::WriterFactory& htmlFactory_,
    const overviewfactory::Factory& overviewFactory_
    ) :
  node{node_},
  metrics{metrics_},
  artefactFactory{artefactFactory_},
  htmlFactory{htmlFactory_},
  overviewFactory{overviewFactory_}
{
}

void Artefact::generate(const Environment &env) const
{
  std::stringstream output;
  printOverview(env, output);

  const auto fullname = env.rootDirectory / name.filename();

  env.os.write(fullname, output.str());
}

void Artefact::printOverview(const artefact::Environment& env, std::ostream& output) const
{
  const auto thisDoc = filename(env.pathFor);
  auto xw = htmlFactory.produce(thisDoc);

  xw->beginDocument(node->name);
  printBody(env, *xw);
  xw->endDocument();

  xw->serialize(output);
}

void Artefact::printBody(const artefact::Environment& env, html::Writer& xw) const
{
  xw.beginSection("Project");
  printPackagesIntro(env, xw);
  xw.endSection();

  printOverviews(env, xw);
}

void Artefact::printPackagesIntro(const Environment& env, html::Writer& xw) const
{
  const ArtefactList artefacts{
    {"Packages with cylic dependencies", artefactFactory.packagesWithCyclicDependencies(node)},
    {"Stable dependency principle violations", artefactFactory.stableDependencyPrincipleViolations(node)},
  };

  xw.beginParagraph();
  printArtefactList(artefacts, env, xw);
  xw.endParagraph();
}

void Artefact::printOverviews(const artefact::Environment& env, html::Writer& xw) const
{
  const auto no = std::bind(&Artefact::nodeOverview, this, std::placeholders::_1, env);

  printOverviewFor<ast::Package>("Packages", metrics.package(), node, xw, no);
  printOverviewFor<ast::Class>("Classes", metrics.clazz(), node, xw, no);
  printOverviewFor<ast::Function>("Functions", metrics.function(), node, xw, no);
  printOverviewFor<ast::Variable>("Variables", metrics.variable(), node, xw, no);
}

Artefact::Link Artefact::nodeOverview(const ast::Node* node, const Environment& env) const
{
  const auto overview = overviewFactory.produce(node);
  assert(overview.has_value());

  const auto artefact = overview.value();
  env.queue(artefact);

  const auto classPath = env.pathFor(*node);
  const auto name = classPath.display();
  const auto url = artefact->filename(env.pathFor);

  return {name, url};
}

html::Url Artefact::filename(const PathForNode&) const
{
  return name;
}


}
