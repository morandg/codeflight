/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace ast
{

class Project;

}

namespace artefact
{

class Artefact;

}

namespace artefact::overview
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual Artefact* packagesWithCyclicDependencies(const ast::Project*) const = 0;
  virtual Artefact* stableDependencyPrincipleViolations(const ast::Project*) const = 0;

};


}
