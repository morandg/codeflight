/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/Artefact.h"

namespace ast
{

class Project;
class Class;

}

namespace html
{

class Writer;
class WriterFactory;

}

namespace metric
{

class Repository;

}

namespace artefact::overviewfactory
{

class Factory;

}

namespace artefact::overview
{

class ArtefactFactory;


class Artefact :
    public artefact::Artefact
{
  public:
    typedef std::pair<std::string, html::Url> Link;

    Artefact(
        const ast::Project*,
        const metric::Repository&,
        const ArtefactFactory&,
        const html::WriterFactory&,
        const overviewfactory::Factory&
        );

    void generate(const Environment&) const override;
    html::Url filename(const PathForNode&) const override;

  private:
    const ast::Project* node;
    const metric::Repository& metrics;
    const html::Url name{{{"index"}}, ".html"};
    const ArtefactFactory& artefactFactory;
    const html::WriterFactory& htmlFactory;
    const overviewfactory::Factory& overviewFactory;
    void printOverview(const artefact::Environment &env, std::ostream &output) const;
    void printBody(const artefact::Environment &env, html::Writer &xw) const;
    void printOverviews(const artefact::Environment &env, html::Writer &xw) const;
    void printPackagesIntro(const Environment &env, html::Writer &xw) const;
    Link nodeOverview(const ast::Node*, const Environment&) const;

};


}
