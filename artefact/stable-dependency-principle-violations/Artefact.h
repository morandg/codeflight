/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/Artefact.h"
#include "artefact/Path.h"
#include "ast/graph/cycle/types.h"
#include <string>
#include <set>


namespace ast
{

class Project;
class Node;

}

namespace ast::graph
{

class Graph;

}

namespace ast::repository
{

class Neighbor;

}

namespace html
{

class Writer;
class WriterFactory;

}

namespace artefact::stable_dependency_principle_violations
{

class ArtefactFactory;


class Artefact :
    public artefact::Artefact
{
  public:
    typedef std::function<double(const ast::Node*, const ast::Node*)> StabilityCalculator;

    Artefact(
        const ast::Project*,
        const ArtefactFactory&,
        const html::WriterFactory&,
        const StabilityCalculator&,
        const ast::repository::Neighbor&
        );

    void generate(const Environment&) const override;
    html::Url filename(const PathForNode&) const override;

  private:
    const ast::Project* root;
    const ArtefactFactory& artefactFactory;
    const html::WriterFactory& htmlFactory;
    const StabilityCalculator stability;
    const ast::repository::Neighbor& neighbors;
    const html::Url name{{{"stable-dependency-principle-violations"}}, ".html"};

    struct DependencyInfo
    {
        const ast::Node *from;
        const ast::Node *to;
        double stability;
    };
    typedef std::vector<std::pair<const ast::Node*, std::size_t>> Packages;

    void printMetric(std::size_t violations, std::size_t dependencies, std::size_t packages, html::Writer&) const;
    void printDependencyList(const std::vector<Artefact::DependencyInfo>&, html::Writer&, const Environment&) const;
    void printPackages(const Packages&, html::Writer&, const Environment&) const;

};


}
