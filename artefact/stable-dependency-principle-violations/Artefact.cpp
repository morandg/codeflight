/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "ast/Project.h"
#include "html/Writer.h"
#include "html/WriterFactory.h"
#include "ast/graph/graph.h"
#include "ast/graph/cycle/johnson.h"
#include "ast/specification/PackageNode.h"
#include "ArtefactFactory.h"
#include "ast/repository/Neighbor.h"
#include "ast/traverse/type.h"
#include "ast/Package.h"
#include "library/util.h"
#include "library/Set.h"
#include "library/OrderedSet.h"


namespace artefact::stable_dependency_principle_violations
{


Artefact::Artefact(
    const ast::Project* root_,
    const ArtefactFactory& artefactFactory_,
    const html::WriterFactory& htmlFactory_,
    const StabilityCalculator& stability_,
    const ast::repository::Neighbor& neighbors_
    ) :
  root{root_},
  artefactFactory{artefactFactory_},
  htmlFactory{htmlFactory_},
  stability{stability_},
  neighbors{neighbors_}
{
}

void Artefact::generate(const Environment& env) const
{
  std::vector<Artefact::DependencyInfo> violations{};
  std::map<const ast::Node*, library::Set<const ast::Node*>> visited{};

  ast::traverse::recursive<ast::Package>(*root, [&visited, &violations, this](const ast::Package& source){
    neighbors.foreachOutgoingPackage(&source, [&visited, &violations, &source, this](const ast::Node* destination){
      if (visited[&source].contains(destination)) {
        return;
      }
      visited[&source].add(destination);

      const auto metric = stability(&source, destination);
      if (metric < 0) {
        violations.push_back(DependencyInfo{&source, destination, metric});
      }
    });
  });

  std::map<const ast::Node*, std::size_t> inViolations{};
  library::OrderedSet<const ast::Node*> packageSet{};

  for (const auto& dependency: violations) {
    packageSet.add(dependency.from);
    inViolations[dependency.from]++;
    packageSet.add(dependency.to);
    inViolations[dependency.to]++;
  }

  Packages packages{};
  for (const auto& package: packageSet.data()) {
    packages.push_back({package, inViolations[package]});
  }

  std::size_t numberOfDependencies = 0;
  std::for_each(visited.begin(), visited.end(), [&numberOfDependencies](const auto& set){
    numberOfDependencies += set.second.size();
  });

  const std::string title = "Stable dependency principle violations";

  std::stringstream output;

  auto xw = htmlFactory.produce(name);

  xw->beginDocument(title);
  xw->beginSection(title);

  printMetric(violations.size(), numberOfDependencies, packages.size(), *xw);
  if (!violations.empty()) {
    printDependencyList(violations, *xw, env);
    printPackages(packages, *xw, env);
  }

  xw->endSection();
  xw->endDocument();

  xw->serialize(output);

  const auto fullname = env.rootDirectory / name.filename();

  env.os.write(fullname, output.str());
}

void Artefact::printMetric(std::size_t violations, std::size_t dependencies, std::size_t packages, html::Writer& xw) const
{
  const double ratio = (dependencies == 0) ? 0.0 : double(violations) / dependencies;

  xw.beginTable({"metric", "value"}, html::Writer::TableType::StringNumbers);
  xw.tableCells({"Number of violations", std::to_string(violations)});
  xw.tableCells({"Number of dependencies", std::to_string(dependencies)});
  xw.tableCells({"Violation ratio", library::toString(ratio, 3)});
  xw.tableCells({"Number of involved packages", std::to_string(packages)});
  xw.endTable();
}

void Artefact::printDependencyList(const std::vector<Artefact::DependencyInfo>& violations, html::Writer& xw, const Environment& env) const
{
  xw.beginSection("Dependencies");

  xw.beginTable({"from", "to", "stability"}, html::Writer::TableType::StringStringNumbers);

  for (const auto& dependency: violations) {
    const auto artefact = artefactFactory.packageDependencies({dependency.from, dependency.to});
    env.queue(artefact);
    const auto url = artefact->filename(env.pathFor);
    const auto from = env.pathFor(*dependency.from).display();
    const auto to = env.pathFor(*dependency.to).display();
    const auto stability = library::toString(dependency.stability, 3);

    xw.beginTableRow();
    xw.tableCellLink(from, url);
    xw.tableCellLink(to, url);
    xw.tableCell(stability);
    xw.endTableRow();
  }

  xw.endTable();
  xw.endSection();
}

void Artefact::printPackages(const Packages& packages, html::Writer& xw, const Environment& env) const
{
  xw.beginSection("Packages");

  xw.beginTable({"Package", "in violations"}, html::Writer::TableType::StringNumbers);

  for (const auto& package: packages) {
    const auto artefact = artefactFactory.package(package.first);
    env.queue(artefact);
    const auto url = artefact->filename(env.pathFor);
    const auto name = env.pathFor(*package.first).display();
    const auto number = std::to_string(package.second);

    xw.beginTableRow();
    xw.tableCellLink(name, url);
    xw.tableCell(number);
    xw.endTableRow();
  }

  xw.endTable();
  xw.endSection();
}

html::Url Artefact::filename(const PathForNode&) const
{
  return name;
}


}
