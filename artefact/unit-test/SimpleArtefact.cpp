/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "SimpleArtefact.h"


namespace artefact::unit_test
{


SimpleArtefact::SimpleArtefact(const std::string &name_) :
  name{{name_}, ""}
{
}

void SimpleArtefact::generate(const Environment &) const
{
  generated++;
}

html::Url SimpleArtefact::filename(const PathForNode&) const
{
  return name;
}

unsigned SimpleArtefact::generatedTimes() const
{
  return generated;
}


}
