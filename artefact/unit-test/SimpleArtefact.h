/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/Artefact.h"


namespace artefact::unit_test
{


class SimpleArtefact :
    public Artefact
{
  public:
    SimpleArtefact(const std::string& name_);

    html::Url filename(const artefact::PathForNode&) const override;

    void generate(const artefact::Environment&) const override;
    unsigned generatedTimes() const;

  private:
    const html::Url name{};
    mutable unsigned generated{0};
};


}
