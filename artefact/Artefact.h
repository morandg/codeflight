/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Environment.h"
#include "html/Url.h"
#include <string>


namespace artefact
{


class Artefact
{
  public:
    virtual ~Artefact() = default;

    virtual void generate(const Environment&) const = 0;
    virtual html::Url filename(const PathForNode&) const = 0;

};


}
