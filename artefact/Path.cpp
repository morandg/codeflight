/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Path.h"
#include "library/util.h"
#include <cassert>
#include <stdexcept>


namespace artefact
{
namespace
{


const std::string DisplaySeparator = "::";
const auto EncodeSeperator = "~";
const auto ListSeparator = ",";


std::vector<std::string> decode(const std::string& encoded)
{
  auto result = library::split(encoded, EncodeSeperator);
  if ((result.size() >= 1) && (result.front() == "-")) {
    result.front() = "";
  }
  return result;
}


}


Path::Path(const std::vector<std::string>& path_) :
  path{path_}
{
}

Path::Path(const std::string& encoded) :
  path{decode(encoded)}
{
}

std::string Path::display() const
{
  const auto size = path.size();
  if ((size >= 1) && (path.front() == "")) {
    if (size == 1) {
      return DisplaySeparator;
    } else {
      return library::join({path.begin()+1, path.end()}, DisplaySeparator);
    }
  }
  return library::join(path, DisplaySeparator);
}

std::string Path::encoded() const
{
  const auto result = library::join(raw(), EncodeSeperator);
  return result;
}

Path Path::add(const std::string& value) const
{
  std::vector<std::string> np = path;
  np.push_back(value);
  return {np};
}

Path Path::remove() const
{
  std::vector<std::string> np = path;
  np.pop_back();
  return {np};
}

Path Path::subPathIn(const Path& context) const
{
  const auto contextSize = context.path.size();
  const auto thisSize = path.size();

  if (thisSize < contextSize) {
    throw std::invalid_argument(display() + " is not within context " + context.display());
  }

  const std::vector<std::string> prefix{path.cbegin(), path.cbegin()+contextSize};
  if (prefix != context.path) {
    throw std::invalid_argument(display() + " is not within context " + context.display());
  }

  const std::vector<std::string> suffix{path.cbegin()+contextSize, path.cend()};
  return {suffix};
}

std::vector<std::string> Path::raw() const
{
  auto result = path;
  if (!result.empty() && (result.front() == "")) {
    result.front() = "-";
  }
  return result;
}

std::string Path::encodeList(const Paths& value)
{
  std::vector<std::string> encoded{};

  for (const auto& path : value) {
    const auto enc = path.encoded();
    encoded.push_back(enc);
  }

  const auto result = library::join(encoded, ListSeparator);
  return result;
}

Paths Path::decodeList(const std::string& value)
{
  const auto parts = library::split(value, ListSeparator);

  Paths result{};
  for (const auto& part : parts) {
    const Path path{part};
    result.push_back(path);
  }

  return result;
}

bool operator==(const Path& lhs, const Path& rhs)
{
  return lhs.raw() == rhs.raw();
}


}
