/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "artefact/Environment.h"
#include "ArtefactFactory.h"
#include "artefact/generateGraphvizSvg.h"
#include "Artefact.h"
#include "html/Url.h"
#include "artefact/Path.h"
#include "ast/Class.h"
#include "ast/Field.h"
#include "ast/Method.h"
#include "ast/query/isOfType.h"
#include "ast/traverse/traverse.h"
#include "ast/traverse/type.h"
#include "graphviz/StreamWriter.h"
#include "graphviz/Writer.h"
#include "library/Set.h"
#include <functional>
#include <sstream>



namespace artefact::intra_class_dependencies
{

namespace
{


template<typename T>
bool hasDirectChildren(const ast::Node& node)
{
  for (const auto child : node.children) {
    if (ast::query::isOfType<T>(child)) {
      return true;
    }
  }
  return false;
}

typedef std::function<bool(const ast::Node*)> Predicate;

typedef std::function<void(const ast::Node* source, const ast::Node* destination)> EdgeVisitor;

void traverseDirectChildrenEdgesOnce(const ast::Node& node, const Predicate& shouldInclude, const EdgeVisitor& visitor)
{
  typedef std::pair<const ast::Node*, const ast::Node*> Edge;
  library::Set<Edge> visited{};

  for (const auto child : node.children) {
    if (shouldInclude(child)) {
      for (const auto dest : child->references) {
        if (shouldInclude(dest)) {
          Edge edge{child, dest};

          if (!visited.contains(edge)) {
            visitor(child, dest);
            visited.add(edge);
          }
        }
      }
    }
  }
}

template<typename T>
using Visitor = std::function<void(const T&)>;

template<typename T>
void printChildrenIfAny(const ast::Node& node, const std::string& label, graphviz::Writer& writer, const Visitor<T>& visitor)
{
  if (hasDirectChildren<T>(node)) {
    writer.beginSubgraph();
    writer.label(label);
    ast::traverse::children<T>(node, visitor);
    writer.endSubgraph();
  }
}


}


Artefact::Artefact(const ast::Class* clazz_, const ArtefactFactory& artefactFactory_) :
  clazz{clazz_},
  artefactFactory{artefactFactory_}
{
}

void Artefact::generate(const Environment& env) const
{
  std::stringstream output{};
  graphviz::StreamWriter writer2{output};
  graphviz::Writer gvw{writer2};
  writeGraph(gvw, env);
  generateGraphvizSvg(filename(env.pathFor), output.str(), env);
}

void Artefact::writeGraph(graphviz::Writer& writer, const Environment& env) const
{
  library::Set<const ast::Node*> printed{};

  const auto printChild = [&printed, &writer](const ast::Node& node){
    writer.beginNode(graphviz::Shape::Box, &node);
    writer.label(node.name);
    writer.endNode();

    printed.add(&node);
  };

  const auto rootUrl = filename(env.pathFor);

  const auto printClassUrl = [&writer, &printed, &env, &rootUrl, this](const ast::Class& node){
    const auto artefact = artefactFactory.clazz(&node);
    env.queue(artefact);

    const auto url = artefact->filename(env.pathFor).relativeTo(rootUrl);

    writer.beginNode(graphviz::Shape::Box, &node);
    writer.label(node.name);
    writer.url(url.escaped());
    writer.endNode();

    printed.add(&node);
  };

  writer.beginGraph();
  writer.label(clazz->name);

  printChildrenIfAny<ast::Method>(*clazz, "methods", writer, printChild);
  printChildrenIfAny<ast::Field>(*clazz, "fields", writer, printChild);
  printChildrenIfAny<ast::Class>(*clazz, "classes", writer, printClassUrl);

  traverseDirectChildrenEdgesOnce(*clazz, std::bind(&library::Set<const ast::Node*>::contains, &printed, std::placeholders::_1), std::bind(&graphviz::Writer::edge, &writer, std::placeholders::_1, std::placeholders::_2));

  writer.endGraph();
}

html::Url Artefact::filename(const PathForNode& pathFor) const
{
  return html::Url{pathFor(*clazz).raw(), ".svg"}.append("intra-class-dependencies");
}


}
