/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace ast
{

class Class;

}

namespace artefact
{

class Artefact;

}

namespace artefact::intra_class_dependencies
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual Artefact* clazz(const ast::Class*) const = 0;

};


}
