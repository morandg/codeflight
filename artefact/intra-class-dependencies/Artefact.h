/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/Artefact.h"
#include <string>


namespace ast
{

class Class;

}

namespace graphviz
{

class Writer;

}

namespace artefact::intra_class_dependencies
{

class ArtefactFactory;


class Artefact :
    public artefact::Artefact
{
  public:
    Artefact(const ast::Class*, const ArtefactFactory&);

    void generate(const Environment&) const override;
    html::Url filename(const PathForNode&) const override;

  private:
    const ast::Class* clazz;
    const ArtefactFactory& artefactFactory;

    void writeGraph(graphviz::Writer &writer, const Environment &env) const;
};


}
