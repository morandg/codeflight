/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace html
{

class Url;

}

namespace artefact
{

struct Environment;


void generateGraphvizSvg(const html::Url&, const std::string& graph, const Environment& env);


}
