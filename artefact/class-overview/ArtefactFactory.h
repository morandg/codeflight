/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace ast
{

class Class;

}

namespace artefact
{

class Artefact;

}

namespace artefact::class_overview
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual Artefact* intraClassDependencies(const ast::Class*) const = 0;
  virtual Artefact* classNeighbors(const ast::Class*) const = 0;

};


}
