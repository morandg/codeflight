/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "ast/Class.h"
#include "html/Writer.h"
#include "html/WriterFactory.h"
#include "html/Url.h"
#include "artefact/Path.h"
#include "artefact/Environment.h"
#include "ArtefactFactory.h"
#include "artefact/ArtefactList.h"


namespace artefact::class_overview
{


Artefact::Artefact(
    const ast::Class* node_,
    const ArtefactFactory& artefactFactory_,
    const html::WriterFactory& htmlFactory_
    ) :
  node{node_},
  artefactFactory{artefactFactory_},
  htmlFactory{htmlFactory_}
{
}

void Artefact::generate(const Environment& env) const
{
  std::stringstream output;

  const auto thisDoc = filename(env.pathFor);
  auto xw = htmlFactory.produce(thisDoc);
  xw->beginDocument(env.pathFor(*node).display());

  printBody(env, *xw);

  xw->endDocument();
  xw->serialize(output);

  const auto fullname = env.rootDirectory / thisDoc.filename();
  env.os.write(fullname, output.str());
}

void Artefact::printBody(const Environment& env, html::Writer& xw) const
{
  const ArtefactList artefacts{
    {"Member dependencies", artefactFactory.intraClassDependencies(node)},
    {"Class neighbors", artefactFactory.classNeighbors(node)},
  };

  xw.beginSection(env.pathFor(*node).display());

  xw.beginParagraph();
  printArtefactList(artefacts, env, xw);
  xw.endParagraph();
  xw.endSection();
}

html::Url Artefact::filename(const PathForNode& pathFor) const
{
  return html::Url{pathFor(*node).raw(), ".html"}.append("index");
}


}
