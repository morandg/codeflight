/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ast/Project.h"
#include "graph/Queue.h"
#include "Generator.h"
#include "ast/repository/Parent.h"
#include "ast/repository/Neighbor.h"
#include "metric/AllRepository.h"
#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>
#include <sstream>


namespace feature_test
{


class Context
{
  public:
    ast::Project* project{};
    ast::repository::Parent parents{};
    ast::repository::Neighbor neighbors{parents};
    graph::Queue* queue{};
    Generator generator{};

    Generator::MetricRepoFactory allMetricsFactory = [this](){
      return new ::metric::AllRepository(neighbors);
    };
};


}
