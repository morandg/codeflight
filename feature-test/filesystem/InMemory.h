/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "driver/Filesystem.h"
#include "artefact/Environment.h"
#include <string>
#include <map>
#include <set>


namespace feature_test::filesystem
{


class InMemory :
    public driver::Filesystem,
    public artefact::Os
{
public:

  std::string pread(const std::string& name) const;  //TODO remove
  std::set<std::filesystem::path> listOfFiles() const;

  std::set<std::filesystem::path> files(const std::filesystem::path&) const override;
  void read(const std::filesystem::path& name, const std::function<void(std::istream&)>& reader) const override;
  void copy(const std::filesystem::path& source, const std::filesystem::path& destination) override;
  bool fileExists(const std::filesystem::path&) const override;
  std::string read(const std::filesystem::path& name) const override;
  void write(const std::filesystem::path& name, const std::string& content) override;
  void execute(const std::string& command) override;

private:
  std::map<std::filesystem::path, std::string> fs{};

};


}
