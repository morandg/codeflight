/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "InMemory.h"
#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


namespace feature_test::filesystem
{
namespace
{


GIVEN("^I have the file \"([^\"]*)\":$")
{
  REGEX_PARAM(std::string, filename);
  REGEX_PARAM(std::string, content);

  cucumber::ScenarioScope<InMemory> filesystem;
  filesystem->write(filename, content);
}

THEN("^I expect to get the output file \"([^\"]*)\":$")
{
  REGEX_PARAM(std::string, filename);
  REGEX_PARAM(std::string, content);

  cucumber::ScenarioScope<InMemory> filesystem;
  const auto actual = filesystem->pread(filename);
  ASSERT_EQ(content, actual);
}


}
}
