/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "InMemory.h"
#include <stdexcept>
#include <algorithm>
#include <sstream>


namespace feature_test::filesystem
{


std::string InMemory::pread(const std::string &name) const
{
  const auto idx = fs.find(name);
  if (idx == fs.end()) {
    throw std::invalid_argument("file not found: " + name);
  }
  return idx->second;
}

std::set<std::filesystem::path> InMemory::listOfFiles() const
{
  std::set<std::filesystem::path> result{};
  for (const auto& itr : fs) {
    const auto filename = itr.first;
    result.insert(filename);
  }
  return result;
}

std::set<std::filesystem::path> InMemory::files(const std::filesystem::path& root) const
{
  std::set<std::filesystem::path> result{};

  const auto files = listOfFiles();
  for (const auto& file : files) {
    const auto relative = std::filesystem::relative(file, root);
    const auto parent_path = relative.parent_path();
    if (parent_path.empty()) {
      result.insert(relative);
    }
  }

  return result;
}

void InMemory::read(const std::filesystem::path& name, const std::function<void (std::istream &)>& reader) const
{
  std::stringstream stream{pread(name)};
  reader(stream);
}

void InMemory::copy(const std::filesystem::path& source, const std::filesystem::path& destination)
{
  fs[destination] = fs[source];
}

bool InMemory::fileExists(const std::filesystem::path& name) const
{
  const auto idx = fs.find(name);
  return idx != fs.end();
}

std::string InMemory::read(const std::filesystem::path& name) const
{
  const auto idx = fs.find(name);
  return (idx == fs.end()) ? "" : idx->second;
}

void InMemory::write(const std::filesystem::path& name, const std::string& content)
{
  fs[name] = content;
}

void InMemory::execute(const std::string &command)
{
  const std::string call{"dot -T svg -o"};
  if (command.substr(0, call.size()) == call) {
    const auto startOfSvgName = call.size();
    const auto endOfSvgName = command.find(' ', startOfSvgName);
    const auto svgFile = command.substr(startOfSvgName, endOfSvgName - startOfSvgName);
    write(svgFile, "dummy file");
  }
}


}
