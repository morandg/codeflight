/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "metric/Repository.h"

namespace feature_test
{


class MinimalRepository :
    public metric::Repository
{
  public:
    const metric::Metrics& package() const;
    const metric::Metrics& clazz() const;
    const metric::Metrics& function() const;
    const metric::Metrics& variable() const;

    RealCalculation packageInstabilityFunction() const;

  private:
    metric::Metrics empty{};
};


}
