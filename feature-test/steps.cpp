/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Context.h"
#include "graph/parse.h"
#include "driver/process.h"
#include "logger/MemoryLogger.h"
#include "filesystem/InMemory.h"
#include "artefact/factory/Factory.h"
#include "graph/Queue.h"
#include "graph/Namer.h"
#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>
#include <sstream>


namespace feature_test
{
namespace
{

const std::string DefaultInputFile{"input.ast"};


GIVEN("^I have the input file:$")
{
  REGEX_PARAM(std::string, input);

  cucumber::ScenarioScope<filesystem::InMemory> filesystem;
  filesystem->write(DefaultInputFile, input);
}

GIVEN("^I enable all metrics$")
{
  cucumber::ScenarioScope<Context> context;
  context->generator.metricRepoFactory = context->allMetricsFactory;
}

WHEN("^I parse the input$")
{
  cucumber::ScenarioScope<Context> context;
  cucumber::ScenarioScope<logger::MemoryLogger> logger;
  cucumber::ScenarioScope<filesystem::InMemory> filesystem;
  std::stringstream input{filesystem->pread(DefaultInputFile)};
  context->project = graph::parse(input, *logger.get());
  context->parents.addFrom(context->project);
  context->neighbors.build(context->project);
  context->generator.metricRepo = context->generator.metricRepoFactory();
  context->generator.factory = new artefact::factory::Factory(context->parents, context->neighbors, *context->generator.metricRepo, {});
  graph::Namer namer{};
  namer.name(context->project);
  context->queue = new graph::Queue(context->project, {}, *filesystem.get(), *logger.get());

  context->generator.project = context->project;
}

WHEN("^I process the file \"([^\"]*)\" with the output directory \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, inputFile);
  REGEX_PARAM(std::string, outputDirectory);

  cucumber::ScenarioScope<Context> context;
  cucumber::ScenarioScope<filesystem::InMemory> filesystem;
  cucumber::ScenarioScope<logger::MemoryLogger> logger;
  driver::process(inputFile, {}, outputDirectory, *filesystem.get(), *filesystem.get(), *logger.get());
}

WHEN("^I process the file \"([^\"]*)\" with the resources directory \"([^\"]*)\" and the output directory \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, inputFile);
  REGEX_PARAM(std::string, resourcesDirectory);
  REGEX_PARAM(std::string, outputDirectory);

  cucumber::ScenarioScope<Context> context;
  cucumber::ScenarioScope<filesystem::InMemory> filesystem;
  cucumber::ScenarioScope<logger::MemoryLogger> logger;
  driver::process(inputFile, resourcesDirectory, outputDirectory, *filesystem.get(), *filesystem.get(), *logger.get());
}

WHEN("^I process the queue until empty$")
{
  cucumber::ScenarioScope<Context> context;
  context->queue->processAll();
}

WHEN("^I generate the file \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, url);

  cucumber::ScenarioScope<Context> context;
  const auto artefact = context->generator.generate(url);
  if (artefact) {
    context->queue->push(artefact.value());
    context->queue->processNext();
  } else {
    throw std::runtime_error("artefact generator not found for: " + url);
  }

}

std::set<std::filesystem::path> parseFilenames(const std::vector<std::map<std::string,std::string>>& hashes)
{
  std::set<std::filesystem::path> result{};
  for (const auto& row : hashes) {
    const auto& filename = row.at("filename");
    result.insert(filename);
  }
  return result;
}

THEN("^I expect to have exactly these files:$")
{
  TABLE_PARAM(filesTable);
  const auto expected = parseFilenames(filesTable.hashes());

  cucumber::ScenarioScope<filesystem::InMemory> filesystem;
  const auto actual = filesystem->listOfFiles();

  ASSERT_EQ(expected, actual);
}

THEN("^I expect to have at least these files:$")
{
  TABLE_PARAM(filesTable);
  const auto expected = parseFilenames(filesTable.hashes());

  cucumber::ScenarioScope<filesystem::InMemory> filesystem;
  const auto all = filesystem->listOfFiles();

  std::set<std::filesystem::path> actual{};
  set_intersection(expected.begin(),expected.end(),all.begin(),all.end(), std::inserter(actual, actual.begin()));

  ASSERT_EQ(expected, actual);
}


}
}
