/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "metric/generic/Lcom4.h"
#include "metric/packageelement/Fan.h"
#include "metric/generic/Instability.h"
#include "metric/package/Fan.h"
#include "ast/query/find.h"
#include "library/util.h"
#include "feature-test/Context.h"
#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>
#include <sstream>


namespace feature_test::metric
{
namespace
{


std::vector<std::string> parseAstPath(const std::string& value)
{
  const auto path = (value == "::") ? std::vector<std::string>{""} : library::split("::" + value, "::");
  return path;
}

const ast::Node* getNode(const std::string& stringPath)
{
  const auto path = parseAstPath(stringPath);
  cucumber::ScenarioScope<Context> context;
  const auto node = ast::query::find(path, context->project);

  if (!node) {
    throw std::runtime_error("node not found: " + stringPath);
  }

  return node;
}


THEN("^I expect the package LCOM4 for \"([^\"]*)\" to be (\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(std::size_t, expectedValue);

  const auto node = getNode(path);

  const auto actualValue = ::metric::generic::packageLcom4(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

THEN("^I expect the class LCOM4 for \"([^\"]*)\" to be (\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(std::size_t, expectedValue);

  const auto node = getNode(path);

  const auto actualValue = ::metric::generic::classLcom4(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

THEN("^I expect the package element fan-in for \"([^\"]*)\" to be (\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(std::size_t, expectedValue);

  const auto node = getNode(path);

  cucumber::ScenarioScope<Context> context;
  ::metric::packageelement::FanIn fan{context->neighbors};
  const auto actualValue = fan.count(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

THEN("^I expect the package element fan-out for \"([^\"]*)\" to be (\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(std::size_t, expectedValue);

  const auto node = getNode(path);

  cucumber::ScenarioScope<Context> context;
  ::metric::packageelement::FanOut fan{context->neighbors};
  const auto actualValue = fan.count(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

THEN("^I expect the package element instability for \"([^\"]*)\" to be (\\d+.\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(double, expectedValue);

  const auto node = getNode(path);

  cucumber::ScenarioScope<Context> context;
  ::metric::packageelement::FanIn fanIn{context->neighbors};
  ::metric::packageelement::FanOut fanOut{context->neighbors};
  ::metric::generic::Instability m{
    std::bind(&::metric::generic::Fan::count, &fanIn, std::placeholders::_1),
    std::bind(&::metric::generic::Fan::count, &fanOut, std::placeholders::_1)
  };
  const auto actualValue = m.value(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

THEN("^I expect the package fan-in for \"([^\"]*)\" to be (\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(std::size_t, expectedValue);

  const auto node = getNode(path);

  cucumber::ScenarioScope<Context> context;
  ::metric::package::FanIn fan{context->neighbors};
  const auto actualValue = fan.count(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

THEN("^I expect the package fan-out for \"([^\"]*)\" to be (\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(std::size_t, expectedValue);

  const auto node = getNode(path);

  cucumber::ScenarioScope<Context> context;
  ::metric::package::FanOut fan{context->neighbors};
  const auto actualValue = fan.count(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

THEN("^I expect the package instability for \"([^\"]*)\" to be (\\d+.\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(double, expectedValue);

  const auto node = getNode(path);

  cucumber::ScenarioScope<Context> context;
  ::metric::package::FanIn fanIn{context->neighbors};
  ::metric::package::FanOut fanOut{context->neighbors};
  ::metric::generic::Instability m{
    std::bind(&::metric::generic::Fan::count, &fanIn, std::placeholders::_1),
    std::bind(&::metric::generic::Fan::count, &fanOut, std::placeholders::_1)
  };
  const auto actualValue = m.value(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

}
}
