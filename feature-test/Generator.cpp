/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Generator.h"

#include "Artefactory.h"


namespace feature_test
{
namespace
{


std::optional<ast::Node*> find(const std::string& name, const std::vector<ast::Node*>& items)
{
  for (const auto item : items) {
    if (item->name == name) {
      return {item};
    }
  }

  return {};
}


}


std::optional<artefact::Artefact*> Generator::generate(const html::Url &url) const
{
  const auto opath = url.getPath();
  std::list<std::string> path{opath.begin(), opath.end()};
  if (path.empty()) {
    return {};
  }
  if (path.front() == "-") {
    path.front() = "";
  }

  return findAndGenerate(path, project);
}

std::optional<artefact::Artefact*> Generator::findAndGenerate(std::list<std::string> path, ast::Node *node) const
{
  if (path.empty()) {
    return {};
  }

  const auto child = find(path.front(), node->children);
  if (child) {
    path.pop_front();
    return findAndGenerate(path, child.value());
  } else {
    return generate(path, node);
  }
}

std::optional<artefact::Artefact*> Generator::generate(std::list<std::string> path, ast::Node *node) const
{
  if (path.empty()) {
    return {};
  }

  const auto artefactName = path.front();
  path.pop_front();

  return generate(artefactName, path, node);
}

std::optional<artefact::Artefact*> Generator::generate(const std::string &artefactName, const std::list<std::string> &path, ast::Node *node) const
{
  Artefactory factory{artefactName, path, this->factory};
  node->accept(factory);
  const auto artefact = factory.getArtefact();
  return artefact;
}


}
