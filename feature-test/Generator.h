/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ast/Node.h"
#include "ast/Project.h"
#include "metric/Repository.h"
#include "artefact/factory/Factory.h"
#include "html/Url.h"
#include "MinimalRepository.h"
#include <list>
#include <string>
#include <optional>
#include <functional>

namespace feature_test
{


class Generator
{
  public:
    std::optional<artefact::Artefact*> generate(const html::Url& url) const;
    std::optional<artefact::Artefact*> findAndGenerate(std::list<std::string> path, ast::Node* node) const;
    std::optional<artefact::Artefact*> generate(std::list<std::string> path, ast::Node* node) const;
    std::optional<artefact::Artefact*> generate(const std::string& artefactName, const std::list<std::string>& path, ast::Node* node) const;

    ast::Project* project{};
    ::metric::Repository* metricRepo{};
    artefact::factory::Factory* factory{};

    typedef std::function<::metric::Repository*()> MetricRepoFactory;
    MetricRepoFactory metricRepoFactory;

    Generator()
    {
      metricRepoFactory = [](){
        return new MinimalRepository();
      };
    }

};


}
