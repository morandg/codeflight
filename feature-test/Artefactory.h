/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ast/DefaultVisitor.h"
#include "artefact/factory/Factory.h"
#include "artefact/Path.h"
#include <string>
#include <list>
#include <optional>
#include <map>
#include <functional>


namespace feature_test
{


class Artefactory :
    public ast::DefaultVisitor
{
  public:
    Artefactory(
        const std::string& name_,
        const std::list<std::string>& path_,
        artefact::factory::Factory* factory_
        );

    void visit(const ast::Class& node) override;
    void visit(const ast::Function& node) override;
    void visit(const ast::Variable& node) override;
    void visit(const ast::Package& node) override;
    void visit(const ast::Project& node) override;

    std::optional<artefact::Artefact*> getArtefact() const;

  private:
    std::string name;
    const std::list<std::string> path;
    artefact::factory::Factory* factory;
    std::optional<artefact::Artefact*> artefact{};

    template<typename T>
    using Generator = std::map<std::string, std::function<artefact::Artefact*(const T*)>>;

    const Generator<ast::Class> classGenerators;
    const Generator<ast::Function> functionGenerators;
    const Generator<ast::Variable> variableGenerators;
    const Generator<ast::Package> packageGenerators;
    const Generator<ast::Project> projectGenerators;

    std::vector<const ast::Node*> getPackages(const std::vector<artefact::Path> paths, const ast::Project* node) const;
    artefact::Artefact* dependenciesBetweenPackages(const ast::Project* node);

    template<typename T>
    std::optional<artefact::Artefact*> generator(const std::string& artefactName, const T* node, const Generator<T>& generator) const
    {
      const auto idx = generator.find(artefactName);
      if (idx == generator.end()) {
        return {};
      } else {
        return idx->second(node);
      }
    }

};


}
