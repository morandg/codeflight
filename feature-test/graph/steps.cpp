/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ast/graph/graph.h"
#include "ast/Package.h"
#include "library/util.h"
#include "ast/graph/cycle/johnson.h"
#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>
#include <sstream>


namespace feature_test::graph
{
namespace
{


class Context
{
  public:
    std::map<std::string, ast::Package*> nodes{};
    ast::graph::Graph graph{};
    std::vector<std::vector<const ast::Node*>> result{};

    void add(const std::string& source, const std::string& destination)
    {
      auto sn = node(source);
      auto dn = node(destination);
      graph.add(sn, dn);
    }

    std::vector<std::string> resultString() const
    {
      std::vector<std::string> string{};

      for (const std::vector<const ast::Node*>& set : result) {
        std::vector<std::string> nodeString{};
        for (const auto& node : set) {
          nodeString.push_back(node->name);
        }
        string.push_back(library::join(nodeString, ", "));
      }

      return string;
    }

  private:
    ast::Package* node(const std::string& name)
    {
      const auto idx = nodes.find(name);
      if (idx != nodes.end()) {
        return idx->second;
      }

      ast::Package* node = new ast::Package();
      node->name = name;
      nodes[name] = node;

      graph.add(node);

      return node;
    }
};


GIVEN("^I have a graph with the edges$")
{
  TABLE_PARAM(edgeTable);

  cucumber::ScenarioScope<Context> context;

  for (const auto& row : edgeTable.hashes()) {
    const auto& source = row.at("source");
    const auto& destination = row.at("destination");
    context->add(source, destination);
  }
}

WHEN("^I run the johnson simple cycle detection algorithm on the graph$")
{
  cucumber::ScenarioScope<Context> context;
  context->result = ast::graph::cycle::johnson(context->graph);
}

THEN("^I expect the resulting nodes$")
{
  TABLE_PARAM(setTable);

  std::vector<std::string> expected{};
  for (const auto& row : setTable.hashes()) {
    const auto& set = row.at("node set");
    expected.push_back(set);
  }

  cucumber::ScenarioScope<Context> context;
  ASSERT_EQ(expected, context->resultString());
}


}
}
