/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MinimalRepository.h"

namespace feature_test
{


const metric::Metrics &MinimalRepository::package() const
{
  return empty;
}

const metric::Metrics &MinimalRepository::clazz() const
{
  return empty;
}

const metric::Metrics &MinimalRepository::function() const
{
  return empty;
}

const metric::Metrics &MinimalRepository::variable() const
{
  return empty;
}

metric::Repository::RealCalculation MinimalRepository::packageInstabilityFunction() const
{
  return [](const ast::Node&) {
    return 0;
  };
}


}
