/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "logger/Logger.h"
#include <vector>
#include <algorithm>


namespace feature_test::logger
{


class MemoryLogger :
    public ::logger::Logger
{
  public:
    void info(const std::string& message) const override;
    void warning(const std::string& message) const override;

    std::string str() const;

  private:
    mutable std::vector<std::string> messages{};

    void log(const std::string& severity, const std::string& message) const;

};


}
