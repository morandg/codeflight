/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefactory.h"

#include "ast/Project.h"
#include "ast/Package.h"
#include "ast/query/find.h"


namespace feature_test
{


Artefactory::Artefactory(const std::string &name_, const std::list<std::string> &path_, artefact::factory::Factory *factory_) :
  name{name_},
  path{path_},
  factory{factory_},
  classGenerators {
    {"intra-class-dependencies", std::bind(&artefact::factory::Factory::intraClassDependencies, factory, std::placeholders::_1)},
    {"neighbors", std::bind(&artefact::factory::Factory::classNeighbors, factory, std::placeholders::_1)},
    {"index", std::bind(&artefact::factory::Factory::clazz, factory, std::placeholders::_1)},
  },
  functionGenerators {
    {"neighbors", std::bind(&artefact::factory::Factory::functionNeighbors, factory, std::placeholders::_1)},
    {"index", std::bind(&artefact::factory::Factory::function, factory, std::placeholders::_1)},
  },
  variableGenerators {
    {"neighbors", std::bind(&artefact::factory::Factory::variableNeighbors, factory, std::placeholders::_1)},
    {"index", std::bind(&artefact::factory::Factory::variable, factory, std::placeholders::_1)},
  },
  packageGenerators {
    {"package-content-dependencies", std::bind(&artefact::factory::Factory::intraPackageDependencies, factory, std::placeholders::_1)},
    {"index", std::bind(&artefact::factory::Factory::package, factory, std::placeholders::_1)},
    {"package-dependencies", std::bind(&artefact::factory::Factory::childPackageDependencies, factory, std::placeholders::_1)},
    {"neighbors", std::bind(&artefact::factory::Factory::packageNeighbors, factory, std::placeholders::_1)},
  },
  projectGenerators {
    {"index", std::bind(&artefact::factory::Factory::overview, factory, std::placeholders::_1)},
    {"dependencies-between-packages", std::bind(&Artefactory::dependenciesBetweenPackages, this, std::placeholders::_1)},
    {"packages-with-cyclic-dependencies", std::bind(&artefact::factory::Factory::packagesWithCyclicDependencies, factory, std::placeholders::_1)},
    {"stable-dependency-principle-violations", std::bind(&artefact::factory::Factory::stableDependencyPrincipleViolations, factory, std::placeholders::_1)},
  }
{
}

void Artefactory::visit(const ast::Class &node)
{
  artefact = generator(name, &node, classGenerators);
}

void Artefactory::visit(const ast::Function &node)
{
  artefact = generator(name, &node, functionGenerators);
}

void Artefactory::visit(const ast::Variable &node)
{
  artefact = generator(name, &node, variableGenerators);
}

void Artefactory::visit(const ast::Package &node)
{
  artefact = generator(name, &node, packageGenerators);
}

void Artefactory::visit(const ast::Project &node)
{
  artefact = generator(name, &node, projectGenerators);
}

std::optional<artefact::Artefact *> Artefactory::getArtefact() const
{
  return artefact;
}

std::vector<const ast::Node *> Artefactory::getPackages(const std::vector<artefact::Path> paths, const ast::Project *node) const
{
  std::vector<const ast::Node*> packages{};

  for (const auto& part : paths) {
    auto partPath = part.raw();
    if ((partPath.size() >= 1) && (partPath.front() == "-")) {
      partPath.front() = "";
    }
    const auto package = ast::query::find(partPath, node);
    if (!package) {
      throw std::invalid_argument("package not found: " + part.display());
    }
    packages.push_back(package);
  }

  return packages;
}

artefact::Artefact *Artefactory::dependenciesBetweenPackages(const ast::Project *node)
{
  if (path.size() != 1) {
    throw std::invalid_argument("expected exactly 1 argument, got " + std::to_string(path.size()));
  }
  const auto paths = artefact::Path::decodeList(path.front());
  const auto packages = getPackages(paths, node);
  return factory->packageDependencies(packages);
}


}
