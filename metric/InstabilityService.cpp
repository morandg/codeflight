/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "InstabilityService.h"


namespace metric
{


InstabilityService::InstabilityService(const PackageInstability& instabilityFunc_) :
  instabilityFunc{instabilityFunc_}
{
}

double InstabilityService::stability(const ast::Node* source, const ast::Node* destination) const
{
  const auto si = getInstability(source);
  const auto di = getInstability(destination);
  const auto stability = si - di;
  return stability;
}

double InstabilityService::getInstability(const ast::Node* node) const
{
  const auto idx = cache.find(node);
  if (idx == cache.end()) {
    const auto instab = instabilityFunc(*node);
    cache[node] = instab;
    return instab;
  } else {
    return idx->second;
  }
}


}
