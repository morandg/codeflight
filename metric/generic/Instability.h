/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <functional>

namespace ast
{

class Node;

}

namespace metric::generic
{


class Instability
{
  public:
    typedef std::function<std::size_t(const ast::Node&)> FanCount;

    Instability(
        const FanCount& fanInCount,
        const FanCount& fanOutCount
        );

    double value(const ast::Node&) const;

  private:
    const FanCount fanInCount;
    const FanCount fanOutCount;

};


}
