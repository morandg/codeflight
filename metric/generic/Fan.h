/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ast/repository/Neighbor.h"
#include <cstdint>

namespace ast
{

class Node;

}

namespace metric::generic
{


class Fan
{
  public:
    typedef  ast::repository::Neighbor::Specification NeigborSpec;

    Fan(const ast::repository::Neighbor&, const NeigborSpec&);

    std::size_t count(const ast::Node&) const;

  private:
    const ast::repository::Neighbor& neighbor;
    const NeigborSpec& spec;
};


}
