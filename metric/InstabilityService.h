/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <functional>
#include <map>

namespace ast
{

class Node;

}

namespace metric
{


class InstabilityService
{
  public:
    typedef std::function<double(const ast::Node&)> PackageInstability;

    InstabilityService(const PackageInstability&);

    double stability(const ast::Node*, const ast::Node*) const;

  private:
    PackageInstability instabilityFunc;

    mutable std::map<const ast::Node*, double> cache{};
    double getInstability(const ast::Node*) const;

};


}
