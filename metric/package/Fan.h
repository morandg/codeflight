/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "metric/generic/Fan.h"

namespace metric::package
{


class FanIn :
    public generic::Fan
{
  public:
    FanIn(const ast::repository::Neighbor&);
};

class FanOut :
    public generic::Fan
{
  public:
    FanOut(const ast::repository::Neighbor&);
};


}
