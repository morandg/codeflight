/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "metric/Metrics.h"


namespace metric
{


class Repository
{
  public:
    typedef std::function<double(const ast::Node&)> RealCalculation;

    virtual ~Repository() = default;

    virtual const Metrics& package() const = 0;
    virtual const Metrics& clazz() const = 0;
    virtual const Metrics& function() const = 0;
    virtual const Metrics& variable() const = 0;

    virtual RealCalculation packageInstabilityFunction() const = 0;
};


}
