/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>
#include <functional>

namespace ast
{

class Node;

}

namespace metric
{


struct Description
{
  typedef std::function<std::string(const ast::Node&)> Calculation;

  std::string name;
  Calculation value;
};

typedef std::vector<Description> Metrics;


}
