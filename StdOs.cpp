/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "StdOs.h"
#include <fstream>


void StdOs::write(const std::filesystem::path& name, const std::string& content)
{
    const auto directory = name.parent_path();
    std::filesystem::create_directories(directory);

    std::ofstream output{name};
    output << content;
}

bool StdOs::fileExists(const std::filesystem::path& name) const
{
  return std::filesystem::exists(name);
}

std::string StdOs::read(const std::filesystem::path& name) const
{
  std::ifstream input{name};

  const std::string content((std::istreambuf_iterator<char>(input)), std::istreambuf_iterator<char>());

  return {content};
}

void StdOs::execute(const std::string &command)
{
    std::system(command.c_str());
}
