/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "driver/Filesystem.h"


class StdFilesystem :
    public driver::Filesystem
{
  public:
    void read(const std::filesystem::path& name, const std::function<void(std::istream&)>& reader) const override;
    std::set<std::filesystem::path> files(const std::filesystem::path&) const override;
    void copy(const std::filesystem::path& source, const std::filesystem::path& destination) override;

};

