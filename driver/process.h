/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/Environment.h"
#include <string>


namespace logger
{

class Logger;

}

namespace driver
{

class Filesystem;


void process(const std::filesystem::path& filename, const std::optional<std::filesystem::path>& resources, const std::filesystem::path& outdir, Filesystem&, artefact::Os&, const logger::Logger&);


}
