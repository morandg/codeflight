/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "resources.h"
#include "feature-test/filesystem/InMemory.h"
#include <filesystem>
#include <gmock/gmock.h>


namespace driver::unit_test
{
namespace
{

using namespace testing;


class resources_Test :
    public Test
{
  public:
    feature_test::filesystem::InMemory fs{};

    html::Resources resources(const std::vector<std::string>& files) const
    {
      html::Resources result{};

      for (const auto& file : files) {
        result.push_back({html::Resource::Type::Stylesheet, {file}});
      }

      return result;
    }
};


TEST_F(resources_Test, return_empty_list_when_no_files_are_available)
{
  const html::Resources expected{};

  const auto actual = getResources({}, fs);

  ASSERT_EQ(expected, actual);
}

TEST_F(resources_Test, return_available_stylesheets)
{
  const auto expected{resources({"style1.css", "style2.css", "xyz.css"})};
  fs.write("style1.css", {});
  fs.write("style2.css", {});
  fs.write("xyz.css", {});

  const auto actual = getResources({}, fs);

  ASSERT_EQ(expected, actual);
}

TEST_F(resources_Test, does_not_return_other_files)
{
  const auto expected{resources({"style.css"})};
  fs.write("text.txt", {});
  fs.write("style.css", {});

  const auto actual = getResources({}, fs);

  ASSERT_EQ(expected, actual);
}

TEST_F(resources_Test, returns_file_from_specific_path)
{
  const auto expected{resources({"style2.css"})};
  fs.write("style1.css", {});
  fs.write("test/style2.css", {});
  fs.write("other/style3.css", {});

  const auto actual = getResources("test", fs);

  ASSERT_EQ(expected, actual);
}

TEST_F(resources_Test, can_specify_directory_with_or_without_trailing_slash)
{
  const auto expected{resources({"style.css"})};
  fs.write("test/style.css", {});

  ASSERT_EQ(expected, getResources("test", fs));
  ASSERT_EQ(expected, getResources("test/", fs));
}

TEST_F(resources_Test, does_not_return_files_from_subdirectories)
{
  const html::Resources expected{};
  fs.write("sub1/style.css", {});
  fs.write("sub1/sub2/style.css", {});

  const auto actual = getResources({}, fs);

  ASSERT_EQ(expected, actual);
}


}
}
