/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <functional>
#include <istream>
#include <filesystem>
#include <set>


namespace driver
{


class Filesystem
{
public:
  virtual ~Filesystem() = default;

  virtual void read(const std::filesystem::path& name, const std::function<void(std::istream&)>& reader) const = 0;
  virtual std::set<std::filesystem::path> files(const std::filesystem::path&) const = 0;
  virtual void copy(const std::filesystem::path& source, const std::filesystem::path& destination) = 0;

};


}
