/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "process.h"
#include "Filesystem.h"
#include "artefact/factory/Factory.h"
#include "graph/parse.h"
#include "graph/Queue.h"
#include "graph/Namer.h"
#include "html/Url.h"
#include "ast/Project.h"
#include "metric/AllRepository.h"
#include "ast/repository/Parent.h"
#include "ast/repository/Neighbor.h"
#include "resources.h"


namespace driver
{


void process(const std::filesystem::path& filename, const std::optional<std::filesystem::path>& resources, const std::filesystem::path& outdir, Filesystem& fs, artefact::Os& os, const logger::Logger& logger)
{
  const auto res = resources.has_value() ? getResources(resources.value(), fs) : html::Resources{};

  if (resources.has_value()) {
    for (const auto& file : res) {
      const auto filename = file.url.filename();
      fs.copy(resources.value() / filename, outdir / filename);
    }
  }

  ast::Project *project{};
  fs.read(filename, [&project, &logger](std::istream& stream){
    project = graph::parse(stream, logger);
  });

  graph::Namer namer{};
  namer.name(project);

  ast::repository::Parent parents{};
  parents.addFrom(project);
  ast::repository::Neighbor neighbors{parents};
  neighbors.build(project);

  metric::AllRepository metricRepo{neighbors};
  artefact::factory::Factory factory{parents, neighbors, metricRepo, res};

  graph::Queue work{project, outdir, os, logger};
  work.push(factory.overview(project));
  work.processAll();
}


}
