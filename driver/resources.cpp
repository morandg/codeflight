/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "resources.h"
#include "Filesystem.h"
#include <map>


namespace driver
{
namespace
{


const std::map<std::string, html::Resource::Type> ResourceExtension {
  {".css", html::Resource::Type::Stylesheet},
  {".js", html::Resource::Type::JavaScript},
};


}


html::Resources getResources(const std::filesystem::path& resources, const Filesystem& fs)
{
  html::Resources result{};

  const auto files = fs.files(resources);
  for (const auto& file : files) {
    const auto idx = ResourceExtension.find(file.extension());
    if (idx != ResourceExtension.end()) {
      const html::Url url{file};
      result.push_back({idx->second, url});
    }
  }

  return result;
}


}
