/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/Path.h"
#include <vector>
#include <string>
#include <map>


namespace ast
{

class Project;
class Node;

}

namespace graph
{


class PathGenerator
{
  public:
    void generateFor(const ast::Project&);
    artefact::Path pathFor(const ast::Node&) const;

  private:
    artefact::Path path{};
    std::map<const ast::Node*, artefact::Path> paths{};

    void generate(const ast::Node&);

};


}
