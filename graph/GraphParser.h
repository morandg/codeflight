/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <tinyxml.h>
#include <map>
#include <vector>
#include <functional>


namespace ast
{

class Project;
class Node;
class Field;
class Method;
class Class;
class Package;
class Function;
class Variable;

}

namespace logger
{

class Logger;

}

namespace graph
{


class GraphParser
{
  public:
    GraphParser(const logger::Logger&);

    ast::Project* parse(const TiXmlDocument&);

  private:
    const logger::Logger& logger;

    const std::map<std::string, std::function<ast::Node*(const TiXmlElement*)>> Parser;

    std::map<std::string, ast::Node*> idToElement{};
    std::map<ast::Node*, std::vector<std::string>> edges{};

    void storeReference(ast::Node *pe, const TiXmlElement *element);
    void storeId(ast::Node *pe, const TiXmlElement *element);
    void handleUnknown(const TiXmlElement*);
    std::vector<ast::Node *> parseChildren(const TiXmlElement *element);
    ast::Field* parseField(const TiXmlElement*);
    ast::Method* parseMethod(const TiXmlElement*);
    void parseMemberContent(ast::Node*, const TiXmlElement*);
    ast::Class *parseClass(const TiXmlElement *element);
    ast::Package *parsePackage(const TiXmlElement *element);
    ast::Function *parseFunction(const TiXmlElement *element);
    ast::Variable *parseVariable(const TiXmlElement *element);
    void parseNodeWithNoChildren(ast::Node *node, const TiXmlElement *element);
};


}
