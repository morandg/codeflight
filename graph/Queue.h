/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "artefact/Environment.h"
#include "PathGenerator.h"
#include "library/Set.h"
#include <queue>
#include <string>
#include <set>

namespace ast
{

class Project;

}

namespace artefact
{

class Artefact;
class Os;

}

namespace html
{

class Url;

}

namespace logger
{

class Logger;

}

namespace graph
{


class Queue
{
  public:
    Queue(
        const ast::Project*,
        const std::filesystem::path& outdir,
        artefact::Os&,
        const logger::Logger&
        );

    void push(const artefact::Artefact*);
    void processAll();
    void processNext();

  private:
    PathGenerator pg{};
    const artefact::Environment env;
    const logger::Logger& logger;

    std::queue<const artefact::Artefact*> queue{};
    library::Set<html::Url> generated{};

    bool hasWork() const;
    bool isGenerated(const html::Url&) const;
    void addGenerated(const html::Url&);
};


}
