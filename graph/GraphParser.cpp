/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "GraphParser.h"
#include "ast/Project.h"
#include "ast/Method.h"
#include "ast/Field.h"
#include "ast/Class.h"
#include "ast/Package.h"
#include "ast/Function.h"
#include "ast/Variable.h"
#include "logger/Logger.h"


namespace graph
{


GraphParser::GraphParser(const logger::Logger& logger_) :
  logger{logger_},
  Parser
  {
    {"class", std::bind(&GraphParser::parseClass, this, std::placeholders::_1)},
    {"package", std::bind(&GraphParser::parsePackage, this, std::placeholders::_1)},
    {"function", std::bind(&GraphParser::parseFunction, this, std::placeholders::_1)},
    {"variable", std::bind(&GraphParser::parseVariable, this, std::placeholders::_1)},
  }
{
}

ast::Project* GraphParser::parse(const TiXmlDocument &document)
{
  const auto child = document.FirstChildElement();
  if (!child) {
    return {};
  }

  const auto cname = child->Attribute("name");
  const std::string name = cname ? cname : "";

  ast::Project* root = new ast::Project();
  root->name = name;

  const auto topPackageNode = child->FirstChildElement("package");
  if (!topPackageNode) {
    logger.warning("missing top level package");
    return root;
  }
  const auto lastTopPackageNode = child->LastChild("package");
  if (topPackageNode != lastTopPackageNode) {
    logger.warning("multiple top level packages");
  }

  const auto topPackage = parsePackage(topPackageNode);
  root->children.push_back(topPackage);

  for (const auto& edge : edges) {
    const auto source = edge.first;
    for (const auto& dst : edge.second) {
      const auto destination = idToElement[dst];
      if (destination) {
        source->references.push_back(destination);
      } else {
        logger.warning("reference target not found: " + dst);
      }
    }
  }

  return root;
}


void GraphParser::storeId(ast::Node* pe, const TiXmlElement* element)
{
  const auto id = element->Attribute("id");
  if (id) {
    idToElement[id] = pe;
  }
}

void GraphParser::storeReference(ast::Node* pe, const TiXmlElement* element)
{
  const std::string target = element->Attribute("target");
  edges[pe].push_back(target);
}

void GraphParser::handleUnknown(const TiXmlElement* element)
{
  const std::string type = element->Value();
  logger.warning("unknown element found: " + type + " (" + std::to_string(element->Row()) + ":" + std::to_string(element->Column()) + ")");
}

ast::Field *GraphParser::parseField(const TiXmlElement* element)
{
  auto* member = new ast::Field();
  parseMemberContent(member, element);
  return member;
}

ast::Method *GraphParser::parseMethod(const TiXmlElement* element)
{
  auto* member = new ast::Method();
  parseMemberContent(member, element);
  return member;
}

void GraphParser::parseMemberContent(ast::Node* member, const TiXmlElement* element)
{
  member->name = element->Attribute("name");

  const std::string childName = element->Attribute("name");
  for (const TiXmlElement *child = element->FirstChildElement(); child != nullptr; child = child->NextSiblingElement()) {
    const std::string type = child->Value();
    if (type == "reference") {
      storeReference(member, child);
    } else {
      handleUnknown(child);
    }
  }

  storeId(member, element);
}


ast::Class* GraphParser::parseClass(const TiXmlElement* element)
{
  auto* clazz = new ast::Class();
  clazz->name = element->Attribute("name");

  storeId(clazz, element);

  for (const TiXmlElement *child = element->FirstChildElement(); child != nullptr; child = child->NextSiblingElement()) {
    const std::string type = child->Value();
    if (type == "field") {
      ast::Node* member = parseField(child);
      clazz->children.push_back(member);
    } else if (type == "method") {
      ast::Node* member = parseMethod(child);
      clazz->children.push_back(member);
    } else if (type == "class") {
      ast::Node* klass = parseClass(child);
      clazz->children.push_back(klass);
    } else if (type == "reference") {
      storeReference(clazz, child);
    } else {
      handleUnknown(child);
    }
  }

  return clazz;
}

ast::Package* GraphParser::parsePackage(const TiXmlElement *element)
{
  auto* package = new ast::Package();
  package->name = element->Attribute("name");

  package->children = parseChildren(element);

  return package;
}

void GraphParser::parseNodeWithNoChildren(ast::Node* node, const TiXmlElement *element)
{
  node->name = element->Attribute("name");

  storeId(node, element);

  for (const TiXmlElement *child = element->FirstChildElement(); child != nullptr; child = child->NextSiblingElement()) {
    const std::string type = child->Value();
    if (type == "reference") {
      storeReference(node, child);
    } else {
      handleUnknown(child);
    }
  }
}

ast::Function* GraphParser::parseFunction(const TiXmlElement *element)
{
  const auto node = new ast::Function();
  parseNodeWithNoChildren(node, element);
  return node;
}

ast::Variable *GraphParser::parseVariable(const TiXmlElement *element)
{
  const auto node = new ast::Variable();
  parseNodeWithNoChildren(node, element);
  return node;
}

std::vector<ast::Node*> GraphParser::parseChildren(const TiXmlElement *element)
{
  std::vector<ast::Node*> children{};

  for (const TiXmlElement *child = element->FirstChildElement(); child != nullptr; child = child->NextSiblingElement()) {
    const std::string type = child->Value();

    const auto idx = Parser.find(type);
    if (idx == Parser.end()) {
      handleUnknown(child);
    } else {
      const auto node = idx->second(child);
      children.push_back(node);
    }
  }

  return children;
}


}
