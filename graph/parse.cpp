/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "parse.h"
#include "GraphParser.h"
#include <tinyxml.h>


namespace graph
{


ast::Project* parse(std::istream& inputFile, const logger::Logger& logger)
{
  TiXmlDocument xml_doc;

  inputFile >> xml_doc;
  GraphParser gp{logger};
  const auto project = gp.parse(xml_doc);
  return project;
}


}
