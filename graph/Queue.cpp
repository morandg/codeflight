/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Queue.h"
#include "artefact/Artefact.h"
#include "logger/Logger.h"
#include "ast/Project.h"
#include <functional>


namespace graph
{
namespace
{


artefact::Environment createEnv(PathGenerator& pg, const ast::Project* root, const std::filesystem::path& outdir, artefact::Os& os, std::queue<const artefact::Artefact*>& queue)
{
  pg.generateFor(*root);
  const auto pathFor = std::bind(&PathGenerator::pathFor, &pg, std::placeholders::_1);
  const artefact::QueueAdder aq = [&queue](const artefact::Artefact* item){
    queue.push(item);
  };
  artefact::Environment env{pathFor, aq, outdir, os};
  return env;
}


}


Queue::Queue(
    const ast::Project* root,
    const std::filesystem::path& outdir,
    artefact::Os& os,
    const logger::Logger& logger_
    ) :
  env{createEnv(pg, root, outdir, os, queue)},
  logger{logger_}
{
}

void Queue::push(const artefact::Artefact* value)
{
  queue.push(value);
}

void Queue::processAll()
{
  while (hasWork()) {
    processNext();
  }
}

void Queue::processNext()
{
  const auto artefact = queue.front();
  const auto name = artefact->filename(env.pathFor);

  if (!isGenerated(name)) {
    logger.info("generate: " + name.filename().string());
    artefact->generate(env);
    addGenerated(name);
  }

  queue.pop();
}

bool Queue::hasWork() const
{
  return !queue.empty();
}

void Queue::addGenerated(const html::Url& artefact)
{
  generated.add(artefact);
}

bool Queue::isGenerated(const html::Url& artefact) const
{
  return generated.contains(artefact);
}


}
