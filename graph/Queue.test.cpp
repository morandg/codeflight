/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Queue.h"
#include "ast/Project.h"
#include "feature-test/filesystem/InMemory.h"
#include "feature-test/logger/MemoryLogger.h"
#include "artefact/unit-test/SimpleArtefact.h"
#include <gmock/gmock.h>


namespace graph::unit_test
{
namespace
{

using namespace testing;
using artefact::unit_test::SimpleArtefact;


class Queue_Test :
    public Test
{
  public:
    ast::Project root{};
    feature_test::filesystem::InMemory os{};
    feature_test::logger::MemoryLogger logger{};
    Queue queue{&root, {}, os, logger};

};


TEST_F(Queue_Test, generates_artefact_in_queue)
{
  SimpleArtefact artefact{{}};
  queue.push(&artefact);

  queue.processAll();

  EXPECT_EQ(1, artefact.generatedTimes());
}

TEST_F(Queue_Test, generates_all_artefact_in_queue)
{
  SimpleArtefact artefact1{"1"};
  queue.push(&artefact1);
  SimpleArtefact artefact2{"2"};
  queue.push(&artefact2);
  SimpleArtefact artefact3{"3"};
  queue.push(&artefact3);

  queue.processAll();

  EXPECT_EQ(1, artefact1.generatedTimes());
  EXPECT_EQ(1, artefact2.generatedTimes());
  EXPECT_EQ(1, artefact3.generatedTimes());
}

TEST_F(Queue_Test, does_not_generate_identical_artefact_twice)
{
  SimpleArtefact artefact{{}};
  queue.push(&artefact);
  queue.push(&artefact);

  queue.processAll();

  EXPECT_EQ(1, artefact.generatedTimes());
}

TEST_F(Queue_Test, does_not_generate_equal_artefact_twice)
{
  SimpleArtefact artefact1{"equal_name"};
  queue.push(&artefact1);
  SimpleArtefact artefact2{"equal_name"};
  queue.push(&artefact2);

  queue.processAll();

  EXPECT_EQ(1, artefact1.generatedTimes());
  EXPECT_EQ(0, artefact2.generatedTimes());
}


}
}
