/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "PathGenerator.h"
#include "ast/Project.h"
#include "ast/traverse/traverse.h"


namespace graph
{


void PathGenerator::generateFor(const ast::Project& root)
{
  path = {};
  paths.clear();
  ast::traverse::children(root, std::bind(&PathGenerator::generate, this, std::placeholders::_1));
}

void PathGenerator::generate(const ast::Node& node)
{
  path = path.add(node.name);
  paths[&node] = path;
  ast::traverse::children(node, std::bind(&PathGenerator::generate, this, std::placeholders::_1));
  path = path.remove();
}

artefact::Path PathGenerator::pathFor(const ast::Node& node) const
{
  const auto idx = paths.find(&node);
  if (idx == paths.end()) {
    throw std::invalid_argument("no path for node " + node.name);
  }
  return idx->second;
}


}
