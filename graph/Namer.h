/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once


namespace ast
{

class Project;
class Node;

}

namespace graph
{


class Namer
{
public:
  void name(ast::Project*);

private:
  unsigned unnamedNumber{0};

  void recursive(ast::Node*);
  void nameChildren(ast::Node*);
};


}
