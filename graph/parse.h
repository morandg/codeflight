/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <istream>
#include <ostream>

namespace ast
{

class Project;

}

namespace logger
{

class Logger;

}

namespace graph
{


ast::Project* parse(std::istream& inputFile, const logger::Logger&);


}
