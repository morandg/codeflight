/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "graph.h"
#include "ast/Node.h"
#include "ast/traverse/type.h"
#include "ast/graph/graph.h"
#include "ast/parents/parents.h"
#include "library/Set.h"


namespace ast::graph
{
namespace
{


void addAllReachable(const Node* node, library::Set<const Node*>& visited, const std::map<const Node*, std::set<const Node*>>& graph)
{
  if (!visited.contains(node)) {
    visited.add(node);
    const auto idx = graph.find(node);
    if (idx == graph.end()) {
      throw std::invalid_argument("node not in graph");
    }
    for (const auto dest : idx->second) {
      addAllReachable(dest, visited, graph);
    }
  }
}

void forAllTargetAnchors(const Node& node, const parents::Parents& parents, const Node* root, const std::function<void(const Node*)>& visitor)
{
  for (const auto target : node.references) {
    const Node* p = parents::topAnchestorBelow(target, parents, [root](const Node* node){
      return node == root;
    });

    if (p) {
      visitor(p);
    }
  }
}


}


Graph buildDirectChildrenGraph(const Node& root, const NodeSpecification& spec)
{
  Graph graph{};

  parents::Parents parents{};
  parents::buildParents(&root, parents);

  traverse::children(root, [&graph, &spec](const Node& child){
    if (spec(&child)) {
      graph.add(&child);
    }
  });

  traverse::children(root, [&graph, &parents, &root](const Node& clazz){
    if (graph.nodeExists(&clazz)) {
      traverse::recursive(clazz, [&graph, &clazz, &parents, &root](const Node& node){
        forAllTargetAnchors(node, parents, &root, [&graph, &clazz](const Node* p){
          if (graph.nodeExists(p)) {
            graph.add(&clazz, p);
          }
        });
      });
    }
  });

  return graph;
}

//TODO rename / merge
Graph buildGraph(const Node& root, const NodeSpecification& specification)
{
  Graph graph{};

  parents::Parents parents{};
  parents::buildParents(&root, parents);

  traverse::recursive(root, [&specification, &graph](const ast::Node& node){
    if (specification(&node)) {
      graph.add(&node);
    }
  });

  const auto getParentPackage = [&graph, &parents](const ast::Node* node){
    const Node* package = parents::lowestAnchestor(node, parents, [&graph](const Node* node){
      return graph.nodeExists(node);
    });
    return package;
  };

  traverse::recursive(root, [&graph, &getParentPackage](const Node& node){
    const Node* source = getParentPackage(&node);
    if (source) {
      for (const auto target : node.references) {
        const Node* dest = getParentPackage(target);
        if (graph.nodeExists(dest)) {
          // we may have not started at the root node
          graph.add(source, dest);
        }
      }
    }
  });

  return graph;
}

void Graph::removeDuplicatedEdges()
{
  for (auto& clazz : nodes) {
    auto& dests = clazz.second;

    library::Set<const Node*> seen;
    const auto newEnd = std::remove_if(dests.begin(), dests.end(), [&seen](const Node* value){
      const auto isDuplicate = seen.contains(value);
      seen.add(value);
      return isDuplicate;
    });

    dests.erase(newEnd, dests.end());
  }
}

void Graph::removeEdgeToItself()
{
  for (auto& clazz : nodes) {
    auto& dests = clazz.second;

    const auto newEnd = std::remove_if(dests.begin(), dests.end(), [source = clazz.first](const ast::Node* dest){
      return source == dest;
    });

    dests.erase(newEnd, dests.end());
  }
}

std::size_t Graph::countConnectedComponents() const
{
  std::map<const Node*, std::set<const Node*>> undirected;

  foreach([&undirected](const Node* node){
    undirected[node];
  });
  foreach([&undirected](const Node* source, const Node* destination){
    undirected[source].insert(destination);
    undirected[destination].insert(source);
  });

  std::size_t count = 0;
  library::Set<const Node*> visited{};
  for (const auto& pair : undirected) {
    const auto isVisited = visited.contains(pair.first);
    if (!isVisited) {
      count++;
      addAllReachable(pair.first, visited, undirected);
    }
  }
  return count;
}

void Graph::assertThat(bool predicate) const
{
  if (!predicate) {
    throw std::runtime_error("assertion failed in " + std::string(__FILE__) + ":" + std::to_string(__LINE__));
  }
}

bool Graph::nodeExists(const Node* value) const
{
  const auto idx = std::find_if(nodes.begin(), nodes.end(), [value](const NodeEntry& entry){
    return value == entry.first;
  });
  return idx != nodes.end();
}

bool Graph::edgeExists(const Node* source, const Node* destination) const
{
  const auto srcIdx = std::find_if(nodes.begin(), nodes.end(), [source](const NodeEntry& entry){
    return source == entry.first;
  });
  if (srcIdx == nodes.end()) {
    return false;
  }
  const auto dstIdx = std::find_if(srcIdx->second.begin(), srcIdx->second.end(), [destination](const Node* entry){
    return destination == entry;
  });
  return dstIdx != srcIdx->second.end();
}

std::size_t Graph::numberOfNodes() const
{
  return nodes.size();
}

std::optional<Graph::NodeEntry*> Graph::find(const Node* value)
{
  const auto idx = std::find_if(nodes.begin(), nodes.end(), [value](const NodeEntry& entry){
    return value == entry.first;
  });
  if (idx == nodes.end()) {
    return {};
  } else {
    return {&*idx};
  }
}

Graph::Graph(const Graph& other) :
  nodes{other.nodes}
{
}

void Graph::add(const Node* node)
{
  assertThat(!nodeExists(node));

  nodes.push_back({node, {}});
}

void Graph::add(const Node* source, const Node* destination)
{
  assertThat(nodeExists(source));
  assertThat(nodeExists(destination));

  auto res = find(source);
  res.value()->second.push_back(destination);
}

void Graph::foreach(const std::function<void(const Node*)>& visitor) const
{
  for (const auto& node : nodes) {
    visitor(node.first);
  }
}

void Graph::foreach(const Node* source, const std::function<void(const Node*)>& visitor) const
{
  const auto idx = std::find_if(nodes.begin(), nodes.end(), [source](const NodeEntry& entry){
    return source == entry.first;
  });
  if (idx != nodes.end()){
    for (const auto& destination : idx->second) {
      visitor(destination);
    }
  }
}

void Graph::foreach(const std::function<void(const Node*, const Node*)>& visitor) const
{
  for (const auto& node : nodes) {
    for (const auto& dest : node.second) {
      visitor(node.first, dest);
    }
  }
}


}
