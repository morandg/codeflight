/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ast/NodeSpecification.h"
#include <map>
#include <vector>


namespace ast::graph
{


class Graph
{
  public:
    Graph() = default;
    Graph(const Graph&);

    void add(const Node*);
    void add(const Node*, const Node* node);

    void removeDuplicatedEdges();
    void removeEdgeToItself();

    std::size_t countConnectedComponents() const;

    void foreach(const std::function<void(const Node*)>&) const;
    void foreach(const Node*, const std::function<void(const Node*)>&) const;
    void foreach(const std::function<void(const Node*, const Node*)>&) const;

    bool nodeExists(const Node*) const;
    bool edgeExists(const Node*, const Node*) const;
    std::size_t numberOfNodes() const;

  private:
    typedef std::pair<const Node*, std::vector<const Node*>> NodeEntry;
    std::vector<NodeEntry> nodes{};

    std::optional<NodeEntry*> find(const Node*);

    void assertThat(bool) const;

};


Graph buildDirectChildrenGraph(const Node&, const NodeSpecification&);
Graph buildGraph(const Node&, const NodeSpecification&);


}
