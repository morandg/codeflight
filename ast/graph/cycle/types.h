/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <vector>

namespace ast
{

class Node;

}

namespace ast::graph::cycle
{


typedef std::vector<const Node*> NodeList;
typedef std::vector<NodeList> Cycles;


}
