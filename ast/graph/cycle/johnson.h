/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "types.h"

namespace ast::graph
{

class Graph;

}

namespace ast::graph::cycle
{


Cycles johnson(const Graph&);


}
