/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <functional>


namespace ast
{

class Node;

typedef std::function<bool(const ast::Node*)> NodeSpecification;


}
