/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AllNodes.h"


namespace ast::specification
{


NodeSpecification AllNodes = [](const Node*){
  return true;
};


}
