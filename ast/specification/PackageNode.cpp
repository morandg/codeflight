/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "PackageNode.h"
#include "ast/query/isOfType.h"


namespace ast::specification
{


const NodeSpecification PackageNode = [](const Node* node){
  return query::isOfType<Package>(node);
};


}
