/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "PackageElement.h"
#include "ast/query/isOfType.h"


namespace ast::specification
{


NodeSpecification PackageElement = [](const Node* node){
  return query::isOfType<Class>(node) || query::isOfType<Function>(node) || query::isOfType<Variable>(node);
};


}
