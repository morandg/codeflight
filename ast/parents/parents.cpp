/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "parents.h"
#include "ast/Node.h"


namespace ast::parents
{


const Node* lowestAnchestor(const Node* successor, const Parents& parents, const NodeSpecification& stopAt)
{
  while (true) {
    if (stopAt(successor)) {
      return successor;
    }

    const auto idx = parents.find(successor);
    if (idx == parents.end()) {
      return nullptr;
    }

    successor = idx->second;
  }
}

const Node* topAnchestorBelow(const Node* successor, const Parents& parents, const NodeSpecification& stopBelow)
{
  while (true) {
    const auto idx = parents.find(successor);
    if (idx == parents.end()) {
      return nullptr;
    }

    if (stopBelow(idx->second)) {
      return successor;
    }

    successor = idx->second;
  }
}

void buildParents(const Node* node, Parents& parents)
{
  for (const auto child : node->children) {
    parents[child] = node;
    buildParents(child, parents);
  }
}


}
