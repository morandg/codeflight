/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ast/NodeSpecification.h"
#include <map>


namespace ast::parents
{


typedef std::map<const Node*, const Node*> Parents;

const Node* lowestAnchestor(const Node* successor, const Parents&, const NodeSpecification& stopAt);
const Node* topAnchestorBelow(const Node* successor, const Parents&, const NodeSpecification& stopBelow);
void buildParents(const Node*, Parents&);


}
