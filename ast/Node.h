/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>


namespace ast
{


class Visitor;


class Node
{
public:
  virtual ~Node() = default;

  virtual void accept(Visitor&) const = 0;

  std::string name{};
  std::vector<Node*> children{};
  std::vector<Node*> references{};

};


}
