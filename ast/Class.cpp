/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Class.h"
#include "Visitor.h"


namespace ast
{


void Class::accept(Visitor &visitor) const
{
  visitor.visit(*this);
}


}
