/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Variable.h"
#include "Visitor.h"


namespace ast
{


void Variable::accept(Visitor &visitor) const
{
  visitor.visit(*this);
}


}
