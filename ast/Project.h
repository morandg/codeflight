/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Node.h"


namespace ast
{


class Project :
    public Node
{
public:
  void accept(Visitor& visitor) const override;
};


}
