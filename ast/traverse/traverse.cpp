/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "traverse.h"
#include "ast/Node.h"
#include "ast/specification/AllNodes.h"


namespace ast::traverse
{


void recursive(const Node& node, const ast::traverse::Visitor& visitor)
{
  recursiveFor(node, specification::AllNodes, visitor);
}

void recursiveFor(const Node& node, const NodeSpecification& spec, const Visitor& visitor)
{
  visitor(node);

  for (const auto child : node.children) {
    if (spec(child)) {
      recursive(*child, visitor);
    }
  }
}

void recursive(const Node& node, ast::Visitor& visitor)
{
  recursive(node, [&visitor](const ast::Node& node){
    node.accept(visitor);
  });
}

void children(const Node& node, ast::Visitor& visitor)
{
  children(node, [&visitor](const ast::Node& node){
    node.accept(visitor);
  });
}

void children(const Node& node, const ast::traverse::Visitor& visitor)
{
  for (const auto& child : node.children) {
    visitor(*child);
  }
}


}
