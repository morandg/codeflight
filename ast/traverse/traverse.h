/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ast/NodeSpecification.h"
#include <functional>


namespace ast
{

class Node;
class Visitor;


namespace traverse
{


typedef std::function<void(const Node&)> Visitor;

void recursive(const Node&, const ast::traverse::Visitor&);
void recursiveFor(const Node&, const NodeSpecification&, const Visitor&);
void recursive(const Node&, ast::Visitor&);

void children(const Node&, ast::Visitor&);
void children(const Node&, const ast::traverse::Visitor&);


}
}

