/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DefaultVisitor.h"


namespace ast
{


void DefaultVisitor::visit(const Project &)
{
}

void DefaultVisitor::visit(const Class &)
{
}

void DefaultVisitor::visit(const Package &)
{
}

void DefaultVisitor::visit(const Variable &)
{
}

void DefaultVisitor::visit(const Function &)
{
}

void DefaultVisitor::visit(const Field &)
{
}

void DefaultVisitor::visit(const Method &)
{
}


}
