/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>

namespace ast
{

class Node;

}

namespace ast::query
{


const Node* find(const std::string&, const Node*);
const Node* find(const std::vector<std::string>&, const Node*);


}
