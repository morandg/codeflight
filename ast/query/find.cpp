/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "find.h"
#include "ast/Node.h"

namespace ast::query
{


const Node* find(const std::string &name, const Node* node)
{
  if (!node) {
    return {};
  }

  for (const auto child : node->children) {
    if (child->name == name) {
      return child;
    }
  }

  return {};
}

const Node* find(const std::vector<std::string>& path, const Node* node)
{
  const Node* result = node;

  for (const auto& part : path) {
    result = find(part, result);
  }

  return result;
}


}
