/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Neighbor.h"
#include "ast/Node.h"
#include "ast/repository/Parent.h"
#include "ast/specification/PackageElement.h"
#include "ast/specification/PackageNode.h"
#include <cassert>


namespace ast::repository
{


Neighbor::Neighbor(const Parent &parent_) :
  outgoingPackageElement{outgoing, specification::PackageElement},
  incomingPackageElement{incoming, specification::PackageElement},
  outgoingPackage{outgoing, specification::PackageNode},
  incomingPackage{incoming, specification::PackageNode},
  parent{parent_}
{
}

void Neighbor::build(const Node *node)
{
  incoming[node];
  outgoing[node];

  for (const auto dest : node->references) {
    incoming[dest].push_back(node);
    outgoing[node].push_back(dest);
  }

  for (const auto child : node->children) {
    build(child);
  }
}

void Neighbor::foreachOutgoingPackageElement(const Node *source, const Visitor &visitor) const
{
  foreach(outgoingPackageElement, source, visitor);
}

void Neighbor::foreachOutgoingPackage(const Node *source, const Visitor &visitor) const
{
  foreach(outgoingPackage, source, visitor);
}

void Neighbor::foreachIncomingPackageElement(const Node *destination, const Visitor &visitor) const
{
  foreach(incomingPackageElement, destination, visitor);
}

void Neighbor::foreachIncomingPackage(const Node *destination, const Visitor &visitor) const
{
  foreach(incomingPackage, destination, visitor);
}

void Neighbor::foreach(const Specification& spec, const Node *destination, const Visitor &visitor) const
{
  Edges visited{};
  foreach(spec, destination, visitor, visited);
}

void Neighbor::foreach(const Specification& spec, const Node* node, const Visitor &visitor, Edges &visited) const
{
  const auto idx = spec.vertices.find(node);
  assert(idx != spec.vertices.end());

  const auto nodeAnchestor = parent.findAnchestor(node, specification::PackageElement);
  if (nodeAnchestor.has_value()) {
    const auto anchestorNode = nodeAnchestor.value();

    for (const Node* successor : idx->second) {
      const auto element = parent.findAnchestor(successor, specification::PackageElement);
      if (element.has_value()) {
        const auto elementNode = element.value();
        if (!visited[anchestorNode].contains(elementNode)) {
          visited[anchestorNode].add(elementNode);
          const auto anchestor = parent.findAnchestor(elementNode, spec.nodes);
          assert(anchestor.has_value());
          visitor(anchestor.value());
        }
      }
    }

  }

  for (const auto child : node->children) {
    if (!spec.nodes(child)) {
      foreach(spec, child, visitor, visited);
    }
  }
}


}
