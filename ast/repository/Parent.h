/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ast/NodeSpecification.h"
#include <map>

namespace ast::repository
{


class Parent
{
  public:
    void addFrom(const Node*);
    std::optional<const Node*> findAnchestor(const Node*, const NodeSpecification&) const;
    std::optional<const Node*> findParent(const Node*) const;

  private:
    std::map<const Node*, const Node*> map{};
};


}
