/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "library/Set.h"
#include "ast/NodeSpecification.h"
#include <functional>
#include <map>


namespace ast
{

class Node;

}

namespace ast::repository
{

class Parent;


class Neighbor
{
  public:
    typedef std::function<void(const Node*)> Visitor;

    typedef std::map<const Node*, std::vector<const Node*>> Vertices;
    struct Specification
    {
        const Vertices& vertices;
        const NodeSpecification& nodes;
    };

    Neighbor(const Parent& parent_);

    void build(const Node* node);

    void foreachOutgoingPackageElement(const Node* source, const Visitor& visitor) const;
    void foreachIncomingPackageElement(const Node* destination, const Visitor& visitor) const;

    void foreachOutgoingPackage(const Node* source, const Visitor& visitor) const;
    void foreachIncomingPackage(const Node* destination, const Visitor& visitor) const;

    void foreach(const Specification&, const Node* destination, const Visitor& visitor) const;

    const Specification outgoingPackageElement;
    const Specification incomingPackageElement;
    const Specification outgoingPackage;
    const Specification incomingPackage;

private:
    const Parent& parent;

    Vertices incoming{};
    Vertices outgoing{};

    typedef std::map<const Node*, library::Set<const Node*>> Edges;
    void foreach(const Specification&, const Node* destination, const Visitor& visitor, Edges& visited) const;

};


}
