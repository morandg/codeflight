/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Parent.h"
#include "ast/Node.h"

namespace ast::repository
{


void Parent::addFrom(const Node* parentNode)
{
  for (const auto child : parentNode->children) {
    map[child] = parentNode;
    addFrom(child);
  }
}

std::optional<const Node*> Parent::findAnchestor(const Node* node, const NodeSpecification& spec) const
{
  auto parentNode = node;
  while (!spec(parentNode)) {
    const auto idx = map.find(parentNode);
    if (idx == map.end()) {
      return {};
    }
    parentNode = idx->second;
  }

  return parentNode;
}

std::optional<const Node*> Parent::findParent(const Node* node) const
{
  const auto idx = map.find(node);
  if (idx == map.end()) {
    return {};
  } else {
    return idx->second;
  }
}


}
