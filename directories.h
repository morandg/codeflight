/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <vector>
#include <filesystem>


std::vector<std::filesystem::path> defaultResourcesDirectories();

