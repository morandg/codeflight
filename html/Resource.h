/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Url.h"
#include <vector>


namespace html
{


struct Resource
{
  enum class Type {
    Stylesheet,
    JavaScript,
  };

  Type type;
  Url url;
};

bool operator==(const Resource& lhs, const Resource& rhs);


typedef std::vector<Resource> Resources;


}
