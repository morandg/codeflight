/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "WriterFactory.h"
#include "Writer.h"


namespace html
{


WriterFactory::WriterFactory(
    const Resources& resources_
    ) :
  resources{resources_}
{
}

std::unique_ptr<Writer> WriterFactory::produce(const Url& url) const
{
  return std::make_unique<Writer>(url, resources);
}


}
