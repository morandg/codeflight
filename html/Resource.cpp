/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Resource.h"


namespace html
{


bool operator==(const Resource& lhs, const Resource& rhs)
{
  return (lhs.type == rhs.type) && (lhs.url == rhs.url);
}


}
