/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Url.h"
#include <gmock/gmock.h>


namespace html::unit_test
{
namespace
{

using namespace testing;


TEST(Url_Test, is_equal_to_itself)
{
  const Url url{{"path", "to", "file"}, ".html"};

  ASSERT_TRUE(url == url);
}

TEST(Url_Test, is_equal_to_other_url_with_same_content)
{
  const Url url1{{"path", "to", "file"}, ".html"};
  const Url url2{{"path", "to", "file"}, ".html"};

  ASSERT_TRUE(url1 == url2);
}

TEST(Url_Test, is_not_equal_when_path_differs_in_length)
{
  const Url url1{{"path", "to", "file"}, ".html"};
  const Url url2{{"path", "to"}, ".html"};

  ASSERT_FALSE(url1 == url2);
}

TEST(Url_Test, is_not_equal_when_path_differs_in_content)
{
  const Url url1{{"path", "to", "file"}, ".html"};
  const Url url2{{"path", "from", "file"}, ".html"};

  ASSERT_FALSE(url1 == url2);
}

TEST(Url_Test, is_not_equal_when_representation_is_different)
{
  const Url url1{{"path", "to", "file"}, ".html"};
  const Url url2{{"path", "to", "file"}, ".svg"};

  ASSERT_FALSE(url1 == url2);
}


}
}
