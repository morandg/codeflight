/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Url.h"
#include "library/util.h"
#include <iomanip>
#include <cassert>
#include <algorithm>


namespace html
{
namespace
{


bool isValidUrlCharacter(char c)
{
  return isalnum(c) || (c == '-') || (c == '_') || (c == '.') || (c == '~') || (c == '!') || (c == ',');
}

std::string to_hex(uint8_t value)
{
  std::stringstream ss;
  ss << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << (int)value;
  return ss.str();
}

const std::string DirectorySeparator = "/";

std::string escape(const std::string& value)
{
  std::string result;

  for (const auto& sym : value) {
    if (isValidUrlCharacter(sym)) {
      result += sym;
    } else {
      result += "%" + to_hex(sym);
    }
  }

  return result;
}

Url::Path escapePath(const Url::Path& value)
{
  Url::Path result{};
  std::transform(value.cbegin(), value.cend(), std::back_inserter(result), escape);
  return result;
}

Url parse(const std::string& value)
{
  auto path = library::split(value, "/");
  if (path.empty()) {
    return {};
  }

  std::string& name = path.back();
  const auto sep = name.find_last_of('.');
  std::string ext{};
  if (sep != std::string::npos) {
    ext = name.substr(sep);
    name.erase(sep);
  }

  return {path, ext};
}


}


Url::Url(const Path& path_, const std::string& representation_) :
  path{path_},
  representation{representation_}
{
}

Url::Url(const std::string& escaped_) :
  Url{parse(escaped_)}
{
}

std::string Url::escaped() const
{
  return library::join(escapePath(path), DirectorySeparator) + representation;
}

std::filesystem::path Url::filename() const
{
  std::filesystem::path result{};
  for (const auto& part : path) {
    result /= part;
  }
  result.replace_extension(representation);
  return result;
}

std::string Url::getRepresentation() const
{
  return representation;
}

Url::Path Url::getPath() const
{
  return path;
}

Url Url::append(const std::string& part) const
{
  Path np = path;
  np.push_back(part);
  return {np, representation};
}

Url Url::asRepresentation(const std::string& representation) const
{
  return {path, representation};
}

Url Url::relativeTo(const Url& root) const
{
  //TODO cleanup
  const auto rootPath = root.filename().parent_path();
  const auto linkFile = filename();
  const auto relativeFile = linkFile.lexically_relative(rootPath).replace_extension("");

  Path path{};
  for (const auto& part : relativeFile) {
    path.push_back(part);
  }

  return {path, representation};
}

bool Url::operator<(const Url& rhs) const
{
  if (path == rhs.path) {
    return representation < rhs.representation;
  } else {
    return path < rhs.path;
  }
}

bool Url::operator==(const Url& rhs) const
{
  return !(*this < rhs) && !(rhs < *this);
}


}
