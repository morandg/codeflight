/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Writer.h"
#include <map>


namespace html
{


Writer::Writer(
    const Url& url_,
    const Resources& resources_
    ) :
  thisUrl{url_},
  resources{resources_}
{
}

void Writer::beginDocument(const std::string &title)
{
  xw.beginNode("html");
  xw.attribute("lang", "en");

  xw.beginNode("head");

  xw.beginNode("meta");
  xw.attribute("charset", "utf-8");
  xw.endNode();

  printResources();

  xw.textNode("title", title);

  xw.endNode();
  xw.beginNode("body");
}

void Writer::printResources()
{
  for (const auto& resource : resources) {
    const auto relativeFilename = resource.url.relativeTo(thisUrl).escaped();

    switch (resource.type) {
      case Resource::Type::Stylesheet:
        xw.beginNode("link");
        xw.attribute("rel", "stylesheet");
        xw.attribute("href", relativeFilename);
        xw.endNode();
        break;

      case Resource::Type::JavaScript:
        xw.beginNode("script");
        xw.attribute("src", relativeFilename);
        xw.text("");
        xw.endNode();
        break;
    }
  }
}

bool Writer::hasResources() const
{
  return !resources.empty();
}

void Writer::endDocument()
{
  xw.endNode();
  xw.endNode();
}

void Writer::beginSection(const std::string& title)
{
  sectionLevel++;

  xw.textNode("h" + std::to_string(sectionLevel), title);
}

void Writer::endSection()
{
  assert(sectionLevel > 0);
  sectionLevel--;
}

void Writer::beginParagraph()
{
  xw.beginNode("p");
}

void Writer::endParagraph()
{
  xw.endNode();
}

void Writer::beginUnnumberedList()
{
  xw.beginNode("ul");
}

void Writer::endUnnumberedList()
{
  xw.endNode();
}

void Writer::beginListItem()
{
  xw.beginNode("li");
}

void Writer::endListItem()
{
  xw.endNode();
}

void Writer::link(const std::string& caption, const Url& url)
{
  const auto relativeFilename = url.relativeTo(thisUrl).escaped();

  xw.beginNode("a");
  xw.attribute("href", relativeFilename);
  xw.text(caption);
  xw.endNode();
}

namespace
{

const std::map<Writer::TableType, std::string> TableTypeNames
{
  {Writer::TableType::StringNumbers, "string-numbers"},
  {Writer::TableType::StringStringNumbers, "string-string-numbers"},
};

std::string tableTypeName(Writer::TableType type)
{
  const auto idx = TableTypeNames.find(type);
  assert(idx != TableTypeNames.end());
  return idx->second;
}

}

void Writer::beginTable(const Row& header, TableType type)
{
  xw.beginNode("table");

  if (hasResources()) {
    xw.attribute("class", tableTypeName(type));
  }

  beginTableRow();
  for (const auto& value : header) {
    xw.textNode("th", value);
  }
  endTableRow();
}

void Writer::endTable()
{
  xw.endNode();
}

void Writer::beginTableRow()
{
  xw.beginNode("tr");
}

void Writer::endTableRow()
{
  xw.endNode();
}

void Writer::tableCell(const std::string& value)
{
  xw.beginNode("td");
  xw.text(value);
  xw.endNode();
}

void Writer::tableCellLink(const std::string& caption, const Url& url)
{
  xw.beginNode("td");
  this->link(caption, url);
  xw.endNode();
}

void Writer::tableCells(const Row& values)
{
  beginTableRow();

  for (const auto& value : values) {
    tableCell(value);
  }

  endTableRow();
}

void Writer::serialize(std::ostream &output) const
{
  xw.serialize(output);
}


}
