/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "XmlWriter.h"
#include "Url.h"
#include "Resource.h"
#include <vector>
#include <set>
#include <functional>
#include <ostream>


namespace html
{


class Writer
{
  public:
    Writer(
        const Url&,
        const Resources&
        );

    void beginDocument(const std::string& title);
    void endDocument();

    void beginSection(const std::string& title);
    void endSection();

    void beginParagraph();
    void endParagraph();

    void beginUnnumberedList();
    void endUnnumberedList();

    void beginListItem();
    void endListItem();

    void link(const std::string&, const Url&);

    enum class TableType {
      StringNumbers,
      StringStringNumbers,
    };
    typedef std::vector<std::string> Row;

    void beginTable(const Row& header, TableType);
    void endTable();

    void beginTableRow();
    void endTableRow();
    void tableCell(const std::string&);
    void tableCellLink(const std::string&, const Url&);

    void tableCells(const Row&);

    void serialize(std::ostream& output) const;

  private:
    XmlWriter xw;

    unsigned sectionLevel{0};

    const Url thisUrl;

    Resources resources;
    void printResources();
    bool hasResources() const;
};


}
