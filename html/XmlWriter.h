/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <tinyxml.h>
#include <ostream>


namespace html
{


class XmlWriter
{
  public:
    void textNode(const std::string& nodeName, const std::string& textValue);

    void text(const std::string& nodeValue);
    void attribute(const std::string& name, const std::string& value);
    void beginNode(const std::string& nodeName);
    void endNode();

    void serialize(std::ostream& output) const;

  private:
    TiXmlDocument doc{};
    TiXmlNode* current{&doc};
};


}
