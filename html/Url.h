/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>
#include <filesystem>


namespace html
{


class Url
{
  public:
    typedef std::vector<std::string> Path;

    Url() = default;
    Url(const Path& name, const std::string& representation);
    Url(const std::string& escaped);

    std::string escaped() const;
    std::filesystem::path filename() const;
    std::string getRepresentation() const;
    Path getPath() const;

    Url append(const std::string&) const;
    Url asRepresentation(const std::string&) const;
    Url relativeTo(const Url&) const;

    bool operator<(const Url&) const;
    bool operator==(const Url&) const;

  private:
    Path path{};
    std::string representation{};

};


}
