/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Writer.h"
#include <memory>


namespace html
{

class Url;


class WriterFactory
{
public:
  WriterFactory(
      const Resources&
      );

  std::unique_ptr<Writer> produce(const Url&) const;

private:
  const Resources resources;

};


}
