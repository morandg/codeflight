/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "StdFilesystem.h"
#include "StdOs.h"
#include "StreamLogger.h"
#include "directories.h"
#include "driver/process.h"
#include <tclap/CmdLine.h>
#include <vector>


namespace
{


std::optional<std::filesystem::path> findResourcesInSystem()
{
  const auto resPaths{defaultResourcesDirectories()};

  for (const auto path : resPaths) {
    const auto fullPath = path / "codeflight" / "resources";
    if (std::filesystem::exists(fullPath) && std::filesystem::is_directory(fullPath)) {
      return fullPath;
    }
  }

  return {};
}


}


int main(int argc, char* argv[])
{
  std::filesystem::path filename{};
  std::filesystem::path outdir{};
  std::optional<std::filesystem::path> resources{};

  try {
    TCLAP::CmdLine cmd{"generating artefacts and metrics"};

    TCLAP::UnlabeledValueArg<std::string> astArg{"ast-file", "file containing the AST", true, {}, "filename"};
    cmd.add(astArg);

    TCLAP::UnlabeledValueArg<std::string> outArg{"output-directory", "where to place the output", true, {}, "output directory"};
    cmd.add(outArg);

    TCLAP::ValueArg<std::string> resArg("r", "resources", "directory for resources to add to the output", false, {}, "directory");
    cmd.add(resArg);

    cmd.parse(argc, argv);

    filename = astArg.getValue();
    outdir = outArg.getValue();
    resources = resArg.isSet() ? resArg.getValue() : resources;
  } catch (TCLAP::ArgException &e) {
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    return -1;
  }

  if (!resources) {
    resources = findResourcesInSystem();
  }

  StdFilesystem fs{};
  StdOs os{};
  StreamLogger logger{std::cout, std::cerr};
  process(filename, resources, outdir, fs, os, logger);

  return 0;
}
