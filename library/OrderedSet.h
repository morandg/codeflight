/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <set>
#include <vector>


namespace library
{


/**
 * A set where the elements are ordered by the first time they are inserted.
 */
template<typename T>
class OrderedSet
{
  public:
    void add(T item)
    {
      if (set.find(item) == set.end()) {
        list.push_back(item);
        set.insert(item);
      }
    }

    const std::vector<T>& data() const
    {
      return list;
    }

  private:
    std::vector<T> list{};
    std::set<T> set{};
};


}
